from tools import readH5,writeH5
from skimage.segmentation import random_walker#,random_walker_with_prior
import matplotlib.pyplot as plt
from matplotlib import colors
import pylab
import numpy as np
import time
import pickle
import vigra
import os
import matplotlib
import h5py
import labeler
import scipy.spatial.distance as scp
np.set_printoptions(threshold=np.nan)


TOL = 0.1

all_labels = []        
UP = 0.5
testrun = 0
imgflag = 0


class RWException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


class RandomWalker(labeler.labeler):
    
    def __init__(self):
        pass
    
    
    
    def setup(self,data,lvol,segmentation, info, cc_start, cc_end, times, positions, K):
        self.data = data
        self.lvol = lvol
        self.segmentation = segmentation
        self.info = info
        self.K = K
        self.times = times
        self.cc_start = cc_start
        self.cc_end = cc_end
        self.positions = positions
        self.appearing_times = []
        self.disappearing_times = []
        for l in cc_start:
            if times[l] not in self.appearing_times:
                self.appearing_times.append(times[l])
        for l in cc_end:
            if times[l] not in self.disappearing_times:
                self.disappearing_times.append(times[l])
        
    def cc_to_larvae(self,output,lvol):
    
    
        # maps components to list of larvae it contains
        cc_ll_connections = {}
        
        # dictionary containing dicionaries for each larva a component contains
        # meaning: component -> larva1 -> positions
        #                    -> larva2 -> positions
        positions_map = {}
        
        for t in range (0,output.shape[-1]):
            components = np.unique(lvol[...,t])[1:]  #we dont want the 0
            labelarr = np.zeros_like(output[...,t])
            labelarr = np.where(output[...,t]!=-1,output[...,t],0)
            for cc in components:
    
                #for the component, find the corresponding larvae and their positions
                ccarr = np.zeros_like(lvol[...,t])
                ccarr = np.where(lvol[...,t]==cc,1,ccarr)    
                cc_ll = ccarr * labelarr
                cc_ll_list = np.unique(cc_ll)[1:]
    
                #this finds the positions of the larvae belonging to the component
                positions={}
                for ll in cc_ll_list:
                    positions[ll] = []
                    pixels = np.where(labelarr==ll)
                    for i in range(len(pixels[0])):
                        positions[ll].append([t+self.info[0],pixels[1][i]+self.info[2],pixels[0][i]+self.info[4]])
    
                positions_map[cc] = positions
    
                cc_ll_connections[cc] = cc_ll_list
    
        return cc_ll_connections, positions_map
    
    
    #write the output to images
    def saveimgs(output_labels):
        lmin = sorted(np.unique(output_labels))[1]
        for t in range (0,output_labels.shape[-1]):
            a=np.copy(np.where(output_labels[...,t]==-1,0,output_labels[...,t]))
            plt.imshow(a,interpolation=None,cmap=None,vmin=lmin-2,vmax=lmin+4)
            plt.savefig(fileoutbase+"_"+str(t).zfill(2)+".png")
    
    #write the output to h5 file
#    def serialize(output):
#        
#        print fileoutbase + "output_test" + str(kk) + ".h5"
#    
#        fh4=h5py.File(fileoutbase + "output_test" + str(kk) + ".h5","a")
#        g=fh4.require_group("volume")
#        g.create_dataset(name="data",data=output)
#        fh4.close()
    
    #finds most common entry other than '-1' in an array
    def find_most_common(self,arr):
        countermap = {}
        for i in range(len(arr)):
            for j in range(len(arr[0])):
                item = arr[i][j]
                if item == 0:
                    continue                
                if item in countermap:
                    countermap[item]+=1
                else:
                    countermap[item]=1
        most_common = sorted(countermap, key=countermap.get, reverse=True)
        
        if most_common[0] == -1:
            return most_common[1]
        else:
            return most_common[0]
    
    #decides whether a threshold is still okay of if it has reached
    #its maximum (in this case [0.5, 0.5, 0.5, ...] )
    def allowed(self,th):
        
        if len(th)==1 and abs(th[0]-0.5) < TOL:
            return 0
    
        maxi = [0.5] * (len(th)-1)
        diff = np.asarray(maxi) - np.asarray(th[0:len(th)-1])
        for k in diff:
            if k<-TOL:
                return 0
        return 1
    
    
    #increases a threshold vector
    def increase(self,th,up):
    
        if len(th) == 1:
            th[0] += up
    
        if len(th) == 2:
            th[0] += up
            th[1] -= up
        else:
            k=len(th)-2
            while k>=0:
                if abs(th[k]-0.5) > TOL:
                    th[k] += up
                    break
                else:
                    th[k] = -0.5
                    th[k-1] += up
                    break
                k-=1
        
            summe = sum(th[0:len(th)-1])
            th[-1] = - summe
    
    #computes the difference between the larvae's sizes
    def compute_energy(self,labels_unique, start_sizes, current_sizes, Nlarvae):
        energy = 0
        for k in labels_unique:
            if k == -1:
                continue   
            a = current_sizes[k] - start_sizes[k]   
            energy += abs(a)
            
            #if 2*current_sizes[k] < start_sizes[k]:
            #    print "PREVENT KILLING"
            #    energy += 300
        
        return energy
    
    def get_biggest(self,seg):
        #Clean out small cc components from a binary segmentation
        lseg=vigra.analysis.labelImageWithBackground(seg.astype(np.float32)).view(np.ndarray)
        sizes=np.bincount(lseg.flatten().astype(np.int32))
        sizes=sizes[1:]
        
        ii=np.argmax(sizes)
        lseg=np.where(lseg==ii+1,1,0)
        
        return lseg, ii+1
    
    def get_average_position(self,positions):
        
        avg_x=0
        avg_y=0
        counter=0
        
        for pos in positions:
            avg_x += pos[1]
            avg_y += pos[2]
            counter += 1
            
        return avg_x/counter, avg_y/counter
    
    
    #assigns an appearing worm its correct label
    def find_new_worm_labels(self,remainder,t):
        #print "START FINDING WORM LABEL", self.cc_start
        print self.info
        
        new_labels = []
        
        for larva in self.cc_start:
            
            #print "compare" , self.times[larva] , "with" , str(t+self.info[0])
            if self.times[larva] == t+self.info[0]:
                #self.cc_start.remove(larva)
                new_labels.append(larva)
            
        return new_labels
        
        
    def label_appearing(self,labels,start_sizes,remainder,new_worm_labels):
        #relabel remainder
        
        remainder = vigra.analysis.labelImageWithBackground(remainder.astype(np.float32),4).view(np.ndarray)
        comps = np.bincount(remainder.flatten())
        comps_copy = np.copy(comps)
        comps[0]=0
        #print "COMPS", comps
        medians_remainder = {}
        medians_data = {}
        
        means_remainder={}
        means_data={}
        
        for k in range(len(new_worm_labels)):
            
            
            label = ii=np.argmax(comps)
            
            #print "FIND LABEL", label
                        
            x,y = np.where(remainder==label)
            
            #print "LENGHT OF X", len(x)
            
            median_x = np.median(x)
            median_y = np.median(y)
            
            mean_x = np.mean(x)
            mean_y = np.mean(y)
            
            medians_remainder[label] = [median_y+self.info[2], median_x+self.info[4]]
            means_remainder[label] = [mean_x+self.info[4], mean_y+self.info[2]]
            
            comps[label]=0
            
        
        for larva in new_worm_labels:
            positions_data = np.vstack(self.positions[larva])
            medians_data[larva] = [np.median(positions_data[:,1]), np.median(positions_data[:,2])]
            means_data[larva] = [np.mean(positions_data[:,1]), np.mean(positions_data[:,2])]
            
        print "MEDIANS REMAINDER:", medians_remainder
        print "MEDIANS DATA:", medians_data
        print "MEANS REMAINDER:", means_remainder
        print "MEANS DATA:", means_data
        
        #find best match
        match = {}
        for larva in medians_remainder.keys():
            mindist = 1000
            minidx = -1
            for larva2 in medians_data.keys():
                dist = scp.norm(np.asarray(medians_remainder[larva])-np.asarray(medians_data[larva2]))
                if dist < mindist:
                    mindist = dist
                    minidx = larva2
                    
            match[larva] = minidx
        
        print match
        
        for a in match.keys():
            #print "RELABEL", a
            labels[remainder == a] = match[a]
            start_sizes[match[a]] = comps_copy[a]
            
            
    def handle_disappearing_and_appearing(self,labels,lvol,t, start_sizes):
        #delete a worm if necessary
    
        labels_unique = np.unique(labels)
    
        lvol_pos = np.where(lvol[...,t]>0,1,0)
        labels_pos = np.where(labels>0, 1, 0)
        remainder = lvol_pos - labels_pos
        remainder_pos = np.where(remainder>0, 1, 0)
        remainder_neg = np.where(remainder<0, 1, 0)
        remainder_pos_size = sum(sum(remainder_pos))
        remainder_neg_size = sum(sum(remainder_neg))
    
        #REMARK: The try-except is necessary, because it could happen due to the threshold, that worms
        #are deleted.
        #find if worm disappeared
        print "remainder_neg_size:", remainder_neg_size
        #if remainder_neg_size>LARVA_SIZE_THRESH:
        if t+self.info[0]-1 in self.disappearing_times:
            #find which worm has disappeared
            remainder = abs(remainder) * labels
            
            print "NEED TO KILL A WORM!!"
            
            for larva in self.cc_end:
                
                print "kill larva", larva, "?"
                print "compare", self.times[larva],"and",t+self.info[0]-1
                if self.times[larva] == t+self.info[0]-1:
                    id_worm_gone = self.find_most_common(np.asarray(remainder))
                    labels = np.where(labels==id_worm_gone,-1,labels)
                    remainder = np.where(remainder==id_worm_gone,-1,remainder)
                    start_sizes[id_worm_gone] = 0
                    print "KILLED WORM" , id_worm_gone
    
        #REMARK: The try-except is necessary, because it could happen due to the threshold, that worms
        #are deleted.
        #a worm has appeared
        #if remainder_pos_size>LARVA_SIZE_THRESH:
        if t+self.info[0] in self.appearing_times:
            print "NEED TO GENERATE WORM!"
            #print "labels unique", labels_unique
            
            new_worm_labels = self.find_new_worm_labels(remainder,t)
            if len(new_worm_labels)>0:
                
                try:
                
                    if len(new_worm_labels)>1:
                    
                        print "mehr kommen dazu, zeit", t, "new worms", new_worm_labels
                        self.label_appearing(labels,start_sizes,remainder,new_worm_labels)
                        print "start_sizes:", start_sizes
                            
                    else:
                        
                        print "nur einer kommt dazu, zeit", t, "new worm", new_worm_labels
                        start_sizes[new_worm_labels[0]] = remainder_pos_size
                        #ll is unused on purpose
                        labelsnew,ll = self.get_biggest(np.where(remainder_pos==1,1,0))
                        labels[labelsnew==1]=new_worm_labels[0]
                except:
                    print "error in appearing worms"
        
        #print "UNIQUE LABELS AFTER APPEARING",np.unique(labels)
        
        return labels
    
    #assign new labels
    def create_labels_from_rwresult(self,res,labels_unique,th,t):
        
        segmentation = self.segmentation
        current = np.zeros_like(segmentation[...,t])
        numrows=len(current)
        numcols=len(current[0])
        
        for i in range(numrows):
            for j in range(numcols):
                #at each place, assign current the maximum!
                maxprob = -10
                maxind = -1
                larva = -2
                for k in labels_unique:
                    larva += 1
                    if k == -1:
                        continue
                    #even if the larvae are labeled 2 and 4, they must be addressed as 0 and 1
                    if res[larva,i,j,1] + th[larva] > maxprob:
                        maxprob = res[larva,i,j,1]
                        maxind = k-1
                current[i,j] = maxind + 1
                
        return np.where(segmentation[...,t]==0,0,current)
        
    #main function
    def label_sequence(self):    
    
        try:
            lvol = self.lvol
            segmentation = self.segmentation
            data = self.data
            
            #labels at first time step
            labels0=lvol[...,0]
            labels0=np.where(labels0==0,-1,labels0)
            labels=labels0
            labels_start = np.unique(labels)
        
            output_labels = np.zeros_like(lvol)
            start_sizes = {}
            
            #save sizes of components from first time step
            for l in labels_start:
                if l < 1:
                    continue   
                start_sizes[l] = sum(sum(k==l for k in labels))
                
            output_labels[...,0] = labels
        
            for t in range(1,lvol.shape[-1]):
                
                
                #take care of worms that have appeared or disappeared
                labels = self.handle_disappearing_and_appearing(labels,lvol,t, start_sizes)
                
                labels_unique = np.unique(labels)
                
                lvol_unique = np.unique(lvol[...,t])
                Nlarvae = len(labels_unique) - 1
                th = [-0.5]*Nlarvae
                th[len(th)-1] = 0.5*(Nlarvae-1)
        
                print int(t*100.0/lvol.shape[-1]) , "%, larvae:", labels_unique, "start sizes", start_sizes
        
                #set labels of next time step to 0
                nextlabels=np.where(segmentation[...,t]==0,-1,0).astype(np.int32)
                labels3d=np.dstack([labels,nextlabels])
                img3d=data[...,t-1:t+1].astype(np.float32) 
                    
                    
                min_energy = 10000
                counter = 0                
                #go through all the thresholds
                while self.allowed(th):
                    
                    if counter > 1000:
                        raise RWException("problem in random walker! counter too high")
                    
                    counter+=1
                    #get the result of the random walker
                    res=random_walker(img3d, labels3d,500,mode="bf",return_full_prob=True)
                    #use the result and the threshold to assign labels for the next time step
                    current = self.create_labels_from_rwresult(res,labels_unique,th,t)
                    curr_labels = np.unique(current)
                    
                    #check if a worm was killed due to wrong threshold
                    #<= because curr_labels also includes the -1
                    if len(curr_labels) <= Nlarvae:
                        print "WORM KILLED DUE TO THRESHOLD"
                        self.increase(th, UP)
                        continue
        
                    #get the sizes of the worms from the new labeling
                    current_sizes = {}  
                    for l in start_sizes:   
                        current_sizes[l] = sum(sum(k==l for k in current))             
                    
                    #calculate the difference between the worms' size and their size from the beginning
                    energy = self.compute_energy(labels_unique, start_sizes, current_sizes, Nlarvae)
        
                    if energy >= min_energy:
                        self.increase(th,UP)
                        continue
                    else:
                        min_energy = energy
                        labelsnew = {}
                
                        try:        
                            for l in curr_labels:
                                if l == 0:
                                    continue
                                labelsnew[l] = np.where(current==l,1,0)
                                #ll is unused
                                labelsnew[l],ll = self.get_biggest(labelsnew[l])
                        except:
                            print "SCHEISSE"
                            print current
                
                    
                        for k in curr_labels:
                            if k == 0:
                                    continue
                            labelsnew[k],ll = self.get_biggest(np.where(current==k,1,0))
                
            
                        #put the mask for the two larvae together again
                        current2 = np.zeros_like(segmentation[...,t])
                        for k in curr_labels:
                            if k == 0:
                                    continue
                            a,b = self.get_biggest(np.where(current==k,1,0))
                            current2 += a*k
                        labels=np.where(segmentation[...,t]==0,-1,current2)
                
                        #remove little fragments
                        for k in curr_labels:
                            if k == 0:
                                continue
                            labelsnew[k],ll = self.get_biggest(np.where(labels==k,1,0))
                           
                
                        #reset the labels array which will be worked on in the next iteration
                        labels=-np.ones_like(labels)
                        for k in curr_labels:
                            if k == 0:
                                continue
                            labels[labelsnew[k]==1]=k    
            
            
                        #update threshold
                        self.increase(th,UP)
                
                output_labels[...,t] = labels
                output_map, positions_map = self.cc_to_larvae(output_labels, lvol)
            
            return output_map, positions_map
        
            print "done"
        
        
        
        except Exception,e:
            print "EXCEPTION"
            print e
            pass 
        
        
if __name__ == "__main__":

    #filename="testdata/encounters/%d/encounter%d.h5"%(kk,kk)
    #filename="/home/kgregor/Desktop/new_cutouts/3/test1.h5"
    #filename="randomwalker_encounters/test%d.h5"%kk    
    filename = "home/kgregor/Desktop/results/encounters/5encounter/test1.h5"
    graphfile = "home/kgregor/Desktop/results/encounters/5encounter/test1.graph_fast"

        
    from cythonfiles.connectivity import getPositionsOfComponent, deserializeGraphFast2    
    data=readH5(filename).squeeze().swapaxes(0,-1).astype(np.float32)/255
    segmentation=readH5(filename,"volume/segmentation").squeeze().swapaxes(0,-1).astype(np.int32)
    lvol=readH5(filename,"volume/labeledvol").squeeze().swapaxes(0,-1).astype(np.int32)
    segmentation=segmentation*(lvol>0)
    connections,positions,sizes,times=deserializeGraphFast2(graphfile)
    
    IF=InfoProducer(connections,positions,times,sizes,true_sizes)
    
    import region_of_interest
     
    RW = RandomWalker(data, lvol, segmentation, info, cc_start, times, positions, K)
    output_labels, output_map, positions_map = RW.label_sequence()
    
    print output_labels.shape

      

