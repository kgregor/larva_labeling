from tools import readH5,showCC, writeH5
import h5py
import randomwalk_more_larvae2
import numpy as np

def test_rw():

	filename="randomwalker_encounters/cut_and_label/encounter_with_3/test1.h5"
	import randomwalk_more_larvae2
	print "start test"

	output = randomwalk_more_larvae2.test_run(filename)
	
	print "LENGTH OF OUTPUT" , len(output),",",len(output[0]),",",len(output[0][0])

	desired_output_file = "tests/combined3_output_test1.h5"
	desired_output=np.asarray(readH5(desired_output_file,"volume/data",onlyhandle=True))
	
	
	print "DIFFERENCE:" , sum(sum(sum(output-desired_output)))

	assert sum(sum(sum(output-desired_output))) < 100
	

