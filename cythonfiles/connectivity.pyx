#cython: boundscheck=False
#cython: wraparound=False
cimport cython
import numpy as np

cimport numpy as np

import sys
from libc.math cimport log

from libcpp.vector cimport vector
from libcpp.map cimport map




def get_dict_times(np.ndarray[np.uint32_t,ndim=1] times):
    #return the dict which indicates at which time (key) are the ccp indexes
    cdef int cc_id,l,t
    cdef int maxtime=-1
    cdef map[int,vector[int]] tdict
    
    l=times.shape[0]
    with nogil:
        for cc_id in range(1,l):
            t=times[cc_id]
            if maxtime<t:
                maxtime=t
            tdict[t].push_back(cc_id)
    
    return tdict,maxtime
    
    
def testfunc():
    cdef map[int,vector[int]] D2
    cdef vector[int] test
    test.push_back(1)
    D2[0]=test
    return D2


def serializeGraphFast2(ofilename,D,Positions,sizes,times,label="entire"):
    import h5py
    fh=h5py.File(ofilename,"a")
    
    g=fh.require_group("graph_fast").require_group(str(label))
    
    print "test"
    
    print times
    
    g.create_dataset(name="times",data=times)
    g.create_dataset(name="sizes",data=sizes)
   
    
    #pos_group=g.require_group("positions")
        
    tmp=[]
    pos=[]
    for id in sorted(D.iterkeys()):
        tmp+=[id]
        tmp+=D[id]
        tmp+=[-1]
        #if id==1: print pos," 1"
        pos+=[(id,id,id)]
        #if id==1: print pos," 2"
        pos+=[Positions[id]]
        #if id==1: print pos," 3"
        pos+=[(-1,-1,-1)]
    
    pos=np.vstack(pos)
    #print pos
        
        
    cdef np.ndarray[np.int32_t,ndim=1] connectivity=np.asarray(tmp, dtype=np.int32)
    g.create_dataset(name="connectivity",data=connectivity)
    
    g.create_dataset(name="positions",data=pos)
                
    fh.close()

    
def getPositionsOfComponent(cc, filename, t_min, left, bottom, t_start_search=0):
    import h5py
    fh=h5py.File(filename,"r")
    
    g=fh["graph_fast"]["entire"]
    
    sizes=g["sizes"].value
    times=g["times"].value
    
    cdef np.ndarray[np.int32_t,ndim=2] pos=g["positions"].value.astype(np.int32)
    
    fh.close()
    
    positions = []
    
    # each component has at least 80 pixels, so start around there:
    i=t_start_search;
    while pos[i,0] != cc:
        i+=1
        
    start = i+1
    #print "COMPONENT ", cc, "STARTET  BEI ", i
    
    while pos[i,0] != -1:
        i+=1
    ende = i
    
    for k in range(start,ende):
        temp = [pos[k][0]-t_min, pos[k][1]-left, pos[k][2]-bottom]
        positions.append(temp)
        
    #print "COMPONENT ", cc , "ENDET BEI" , i
    
    return positions, i
    
    
def deserializeGraphFast2(filename,label="entire"):
    import h5py
    fh=h5py.File(filename,"r")
    
    g=fh["graph_fast"][str(label)]
    
    sizes=g["sizes"].value
    times=g["times"].value
    
    #pos_group=g.require_group("positions")
    
    cdef np.ndarray[np.int32_t,ndim=1] connectivity=g["connectivity"].value.astype(np.int32)
    cdef np.ndarray[np.int32_t,ndim=2] pos=g["positions"].value.astype(np.int32)
    cdef map[int,vector[int]] D
    #cdef map[int,vector[float]] Positions

    Positions={}

    cdef vector[int] aux
    cdef vector[float] aux2
    cdef vector[int] aux3 

    fh.close()
    cdef int i,j,val
    i=0
    j=0


    cdef int nvals=connectivity.shape[0]
    
    while i!=nvals:
        D[connectivity[i]]=aux
        
        j=i+1
        while connectivity[j]!=-1:
            D[connectivity[i]].push_back(connectivity[j])
            j+=1
        i=j+1
    
    cdef int nvals2=pos.shape[0]
    cdef float count=0
    cdef float xc
    cdef float yc
    cdef float tc
    i=0
    j=0
    while i!= nvals2:
        j=i+1
        count=0
        xc=0
        yc=0
        tc=0
        
        Positions[pos[i,0]]=[]
        while pos[j,0]!=-1:
            count+=1
            Positions[pos[i,0]]+=[(pos[j,0],pos[j,1],pos[j,2])]
            
#            tc+=pos[j,0]
#            xc+=pos[j,1]
#            yc+=pos[j,2]
            j+=1
#        xc/=count
#        tc/=count
#        yc/=count
        
        #Positions[pos[i,0]].push_back(tc)
        #Positions[pos[i,0]].push_back(xc)
        #Positions[pos[i,0]].push_back(yc)
        

        i=j+1

    #print "SIZE OF POSISIONT: " , Positions[1]
        
    return D,Positions,sizes,times






def deserializeGraphFast(filename,label="entire"):
    import h5py
    fh=h5py.File(filename,"r")
    
    g=fh["graph_fast"][str(label)]
    
    sizes=g["sizes"].value
    times=g["times"].value
    
    #pos_group=g.require_group("positions")
    
    cdef np.ndarray[np.int32_t,ndim=1] connectivity=g["connectivity"].value.astype(np.int32)
    cdef np.ndarray[np.int32_t,ndim=2] pos=g["positions"].value.astype(np.int32)
    
    fh.close()
    cdef int i,j,val
    i=0
    j=0
    D={}
    Positions={}
    #print pos
    #print pos.shape[0],pos.shape[1]
    #print "ajdshjgdhasgdhjgahsgdhgh"
    
    while i!=connectivity.shape[0]:
        D[connectivity[i]]=[]
        
        j=i+1
        while connectivity[j]!=-1:
            D[connectivity[i]]+=[connectivity[j]]
            j+=1
        i=j+1
    
    i=0
    j=0
    while i!=pos.shape[0]:
        Positions[pos[i,0]]=[]
        j=i+1
        while pos[j,0]!=-1:
            Positions[pos[i,0]]+=[(pos[j,0],pos[j,1],pos[j,2])]
            j+=1
        i=j+1
    
        
    return D,Positions,sizes,times




def serializeGraphFast2(ofilename,D,Positions,sizes,times,label="entire"):
    import h5py
    fh=h5py.File(ofilename,"w")
    
    g=fh.require_group("graph_fast").require_group(str(label))
    
    print "test"
    
    print times
    
    g.create_dataset(name="times",data=times)
    g.create_dataset(name="sizes",data=sizes)
   
    
    #pos_group=g.require_group("positions")
        
    tmp=[]
    pos=[]
    for id in sorted(D.iterkeys()):
        tmp+=[id]
        tmp+=D[id]
        tmp+=[-1]
    
    for i in range(1,len(Positions)+1):
        
         #if id==1: print pos," 1"
        pos+=[(i,i,i)]
        #if id==1: print pos," 2"
        pos+=[Positions[i]]
        #if id==1: print pos," 3"
        pos+=[(-1,-1,-1)]
        
        
    
    
    pos=np.vstack(pos)
    #print pos
        
        
    cdef np.ndarray[np.int32_t,ndim=1] connectivity=np.asarray(tmp, dtype=np.int32)
    g.create_dataset(name="connectivity",data=connectivity)
    
    g.create_dataset(name="positions",data=pos)
                
    fh.close()




def serializeGraphFast(ofilename,D,Positions,sizes,times,label="entire"):
    import h5py
    fh=h5py.File(ofilename,"a")
    
    g=fh.require_group("graph_fast").require_group(str(label))
    
    print "test"
    
    print times
    
    g.create_dataset(name="times",data=times)
    g.create_dataset(name="sizes",data=sizes)
   
    
    #pos_group=g.require_group("positions")
    
    
    tmp=[]
    pos=[]
    for id,others in D.items():
        tmp+=[id]
        tmp+=others
        tmp+=[-1]
        #if id==1: print pos," 1"
        pos+=[(id,id,id)]
        #if id==1: print pos," 2"
        pos+=[Positions[id]]
        #if id==1: print pos," 3"
        pos+=[(-1,-1,-1)]
    
    pos=np.vstack(pos)
    #print pos
        
        
    cdef np.ndarray[np.int32_t,ndim=1] connectivity=np.asarray(tmp, dtype=np.int32)
    g.create_dataset(name="connectivity",data=connectivity)
    
    g.create_dataset(name="positions",data=pos)
                
    fh.close()



from cython.operator cimport dereference as deref, preincrement as inc

def storesafe(group,path,shape,dtype,data,compression=None):
    if path not in group:
        if compression==None:
            dg=group.create_dataset(name=path,shape=shape,dtype=dtype,data=data)
        else:
            dg=group.create_dataset(name=path,shape=shape,dtype=dtype,data=data,compression=compression)
    else:
        dg=group[path]
        dg[...]=data
    
    return dg


#===============================================================================
#  New versions
#===============================================================================

def serializeGraphFastNew(ofilename,D,Positions,sizes,times,label="entire"):
    import h5py
    fh=h5py.File(ofilename,"a")
    
    g=fh.require_group("graph_fast").require_group(str(label))
    storesafe(g,"sizes",sizes.shape,sizes.dtype,sizes)
    storesafe(g,"times",times.shape,times.dtype,times)
    
    #pos_group=g.require_group("positions")
    
    cdef map[int,vector[int]] graph=D
    cdef map[int,vector[vector[int]]] positions=Positions
    
    cdef map[int,vector[int]].iterator git #iterator
    
    
    git=graph.begin()
    pit=positions.begin()
    
    
    cdef vector[int] others
    cdef int id,k,j
    
    cdef vector[int] tmp
    cdef vector[int] pos
    
    
    cdef vector[int] posmakerid
    posmakerid.resize(3)
    cdef vector[int] posmakerstop
    posmakerstop.resize(3)
    posmakerstop[0]=-1
    posmakerstop[1]=-1
    posmakerstop[2]=-1
    while git!=graph.end():
        
        id=deref(git).first
        tmp.push_back(id)
        
        for k in range(deref(git).second.size()):
            tmp.push_back(deref(git).second[k])
        tmp.push_back(-1)
        
        
        #positon
        for j in range(3):
            pos.push_back(id)
         
        for k in range(positions[id].size()):
            for j in range(3):
                pos.push_back(positions[id][k][j])
        
        for j in range(3):
            pos.push_back(-1)
        
        inc(git)
    
    
    pos_np=np.asarray(pos).reshape(-1,3)
    connectivity=np.asarray(tmp, dtype=np.int32)    
    storesafe(g,"connectivity",connectivity.shape,connectivity.dtype,connectivity,compression=None)
    storesafe(g,"positions",pos_np.shape,pos_np.dtype,pos_np,compression=None)
    
        
        
    fh.close()





@cython.boundscheck(False)
@cython.wraparound(False) 
def adjacency_matrix(np.ndarray[np.uint32_t, ndim=3] vol, long maxlabel):
    
    cdef vector[vector[int]] graph
    cdef vector[vector[int]] positions #Contains the postions of the connected componet
    graph.resize(maxlabel+1)
    positions.resize(maxlabel+1)
    cdef np.ndarray[np.uint32_t, ndim=1] sizes=np.zeros(maxlabel+1,np.uint32) #Contains the size of the component
    cdef np.ndarray[np.uint32_t, ndim=1] times=np.zeros(maxlabel+1,np.uint32) #Contain the time at whcih the component was created
    
    cdef int m,n,d 
    cdef int tt,jj,ii,w,label,nextlabel,auxlabel
   
    cdef bint found = False
    cdef int steps=vol.shape[0]
    m=vol.shape[1]
    n=vol.shape[2]
    for tt in xrange(0,steps-1):
        for ii in xrange(0,m):
            for jj in xrange(0,n):
                label=vol[tt,ii,jj]
                nextlabel=vol[tt+1,ii,jj]
                
                #print "here", label,nextlabel
                sizes[label]+=1
                times[label]=tt
                
                if label!=0:
                    positions[label].push_back(ii)
                    positions[label].push_back(jj)
                    
                
                
                
                if label!=0 and nextlabel !=0:
                    found=False
                    for w in range(0,graph[label].size()):
                        
                        auxlabel=graph[label][w]
                        if auxlabel==nextlabel:
                            found=True
                        if found:
                            break
                    if not found:
                        graph[label].push_back(nextlabel)
                            
    #get the sixze of the stuff in the last frame 
    for jj in xrange(0,n):
        for ii in xrange(0,m):
            label=vol[steps-1,ii,jj]
            if label!=0:
                sizes[label]+=1
                times[label]=steps-1
                positions[label].push_back(ii)
                positions[label].push_back(jj)
      
    
    
    cdef int sizecc
    D={}
    Positions={}
    for label in range(1,maxlabel+1):
        tmp=[]
        for ii in range(0,graph[label].size()):
            nextlabel=graph[label][ii]
            tmp.append(nextlabel)
        D[label]=tmp
        
        #Store positions
        tmp=[]
        sizecc=positions[label].size()
        ii=0
        while ii < sizecc:
            tmp.append((times[label],positions[label][ii],positions[label][ii+1])) 
            ii+=2
        Positions[label]=tmp
        
    return D,Positions,times,sizes

@cython.boundscheck(False)
@cython.wraparound(False) 
def adjacency_matrix_new(np.ndarray[np.uint32_t, ndim=3] vol, long maxlabel, int margin=5):
    #Margin is the margin of overlap between connected components
    cdef vector[vector[int]] graph
    cdef map[int,map[int,int]] overlap
    
    cdef vector[vector[int]] positions #Contains the postions of the connected componet
    graph.resize(maxlabel+1)
    positions.resize(maxlabel+1)
    cdef np.ndarray[np.uint32_t, ndim=1] sizes=np.zeros(maxlabel+1,np.uint32) #Contains the size of the component
    cdef np.ndarray[np.uint32_t, ndim=1] times=np.zeros(maxlabel+1,np.uint32) #Contain the time at whcih the component was created
    
    cdef int m,n,d 
    cdef int tt,jj,ii,w,label,nextlabel,auxlabel
   
    cdef bint found = False
    cdef int steps=vol.shape[0]
    m=vol.shape[1]
    n=vol.shape[2]
    for tt in xrange(0,steps-1):
        for ii in xrange(0,m):
            for jj in xrange(0,n):
                label=vol[tt,ii,jj]
                nextlabel=vol[tt+1,ii,jj]
                
                #print "here", label,nextlabel
                sizes[label]+=1
                times[label]=tt
                
                if label!=0:
                    positions[label].push_back(ii)
                    positions[label].push_back(jj)
                    
                
                
                
                if label!=0 and nextlabel !=0:
                    found=False
                    for w in range(0,graph[label].size()):
                        
                        auxlabel=graph[label][w]
                        if auxlabel==nextlabel:
                            found=True
                        if found:
                            break
                    overlap[label][nextlabel]+=1
                    if not found:
                        graph[label].push_back(nextlabel)
                        overlap[label][nextlabel]=1
                        
                        
    cdef vector[vector[int]] newgraph
    newgraph.resize(maxlabel+1)
    for label in range(1,maxlabel+1):
        for ii in range(0,graph[label].size()):
            newlabel=graph[label][ii]
            if overlap[label][newlabel]>=margin:
                newgraph[label].push_back(newlabel)
#            else:
#                print label, "-> ",newlabel," ",overlap[label][newlabel]
#                print sizes[label],sizes[newlabel]
#                
#                if overlap[label][newlabel]==0: print "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"
                
    graph=newgraph
    
    #get the sixze of the stuff in the last frame 
    for jj in xrange(0,n):
        for ii in xrange(0,m):
            label=vol[steps-1,ii,jj]
            if label!=0:
                sizes[label]+=1
                times[label]=steps-1
                positions[label].push_back(ii)
                positions[label].push_back(jj)
      
    
    
    cdef int sizecc
    D={}
    Positions={}
    for label in range(1,maxlabel+1):
        tmp=[]
        for ii in range(0,graph[label].size()):
            nextlabel=graph[label][ii]
            tmp.append(nextlabel)
        D[label]=tmp
        
        #Store positions
        tmp=[]
        sizecc=positions[label].size()
        ii=0
        while ii < sizecc:
            tmp.append((times[label],positions[label][ii],positions[label][ii+1])) 
            ii+=2
        Positions[label]=tmp

    return D,Positions,times,sizes

#===============================================================================
# New version much faster but return result is sligthly different moight couse problems
#===============================================================================

@cython.boundscheck(False)
@cython.wraparound(False) 
def adjacency_matrix_new2(np.ndarray[np.uint32_t, ndim=3] vol, long maxlabel, int margin=5):
    
    cdef map[int,vector[int]] graph
    cdef map[int,map[int,int]] overlap
    
    cdef map[int,vector[vector[int]]] positions #Contains the postions of the connected componet
    
    cdef np.ndarray[np.uint32_t, ndim=1] sizes=np.zeros(maxlabel+1,np.uint32) #Contains the size of the component
    cdef np.ndarray[np.uint32_t, ndim=1] times=np.zeros(maxlabel+1,np.uint32) #Contain the time at whcih the component was created
    
    cdef int m,n,d 
    cdef int tt,jj,ii,w,label,nextlabel,auxlabel
   
    cdef bint found = False
    cdef int steps=vol.shape[0]
    
    
    cdef vector[int] aux #auxiliari vector of integers to rapresent a point
    aux.resize(3)
    m=vol.shape[1]
    n=vol.shape[2]
    for tt in xrange(0,steps-1):
        for ii in xrange(0,m):
            for jj in xrange(0,n):
                label=vol[tt,ii,jj]
                nextlabel=vol[tt+1,ii,jj]
                

                sizes[label]+=1
                times[label]=tt
                
                if label!=0:
                    aux[0]=tt
                    aux[1]=ii
                    aux[2]=jj
                    positions[label].push_back(aux)
                    
                
                
                
                if label!=0 and nextlabel !=0:
                    found=False
                    for w in range(0,graph[label].size()):
                        
                        auxlabel=graph[label][w]
                        if auxlabel==nextlabel:
                            found=True
                        if found:
                            break
                        
                    if not found:
                        graph[label].push_back(nextlabel)
                        overlap[label][nextlabel]=1
                    else:
                        overlap[label][nextlabel]+=1
                        
    #Remove stuff wich overlaps too little
    cdef map[int,vector[int]] newgraph
    for label in range(1,maxlabel+1):
        for ii in range(0,graph[label].size()):
            newlabel=graph[label][ii]
            if overlap[label][newlabel]>=margin:
                newgraph[label].push_back(newlabel)
            else:
                newgraph[label]=(new vector[int]())[0]
                
    #FIXME:
    #we want that the list of the elements which are not included to be 0 for backward compatilbility

 
#            else:
#                print label, "-> ",newlabel," ",overlap[label][newlabel]
#                print sizes[label],sizes[newlabel]
#                
#                if overlap[label][newlabel]==0: print "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH"
                

    
    #get the sixze of the stuff in the last frame 
    for jj in xrange(0,n):
        for ii in xrange(0,m):
            label=vol[steps-1,ii,jj]
            if label!=0:
                sizes[label]+=1
                times[label]=steps-1
                aux[0]=tt
                aux[1]=ii
                aux[2]=jj
                positions[label].push_back(aux)

    

    
    return newgraph,positions,times,sizes
 