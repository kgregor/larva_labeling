#cython: nonecheck=False
#cython: boundscheck=False
#cython: wraparound=False

cimport cython
import numpy as np

cimport numpy as np
from libc.math cimport log

from libcpp.vector cimport vector
from libcpp.map cimport map
from cython.parallel import parallel, prange


cdef class Labeller:
    cdef np.uint8_t[:,:] palette
    cdef np.uint8_t[:,:,:] current_image_color
    cdef int current_time
    cdef int sx
    cdef int sy
    cdef int st
    cdef int ncc
    cdef int[:] sizes
    
    
    cdef map[int,vector[int]]* time_map
    cdef map[int,vector[vector[int]]]* positions_map #contains the positions for the timestep
    
    def __cinit__(self, np.uint8_t[:,:] palette, int sx,int sy,int st):
        
        self.current_image_color=np.zeros((sx,sy,3),np.uint8)
        self.palette=palette
        self.current_time=0
        
        self.st=st
        self.sx=sx
        self.sy=sy
    
    
    cdef void reset_image(self):
        cdef int i,j,alpha
        alpha=60
        self.current_image_color=np.zeros((self.sx,self.sy,4),np.uint8)
        for i in range(self.sx):
            for j in range(self.sy):
                self.current_image_color[i,j,3]=alpha
        
        
        #self.current_image_colo=cython.view.array(shape=(self.sx,self.sy,3), itemsize=sizeof(int))
    
    cpdef deserialize_positions(self,filename,label="entire"):
        import h5py
        fh=h5py.File(filename,"r")
    
        g=fh["graph_fast"][str(label)]
        cdef int[:] times=g["times"].value.astype(np.int32)
        cdef int[:,:] pos=g["positions"].value.astype(np.int32)
        
        
        fh.close()
        
        self.ncc=times.shape[0]-1 #includes the background
        
        #loop to get the times  out 
        cdef int cc,t
        cdef vector[int] tmpvec
        cdef map[int,vector[int]]* time_map =new map[int,vector[int]]() #hakinsh should prevent cython from deleteting the pointed object
        
        cdef int maxtime=0
        for cc in range(1,times.shape[0]):
            t=times[cc]
            if t>maxtime: maxtime=t
            time_map[0][t].push_back(cc)
        assert maxtime==self.st,"The graph has bigger time than the volume %d"%maxtime
        self.time_map=time_map
        
        
        #loop to get the positions out
        

        #loop to get the positions out
        
        cdef map[int,vector[vector[int]]]* positions_map= new map[int,vector[vector[int]]]() #hakinsh should prevent cython from deleteting the pointed object
        cdef int i=0
        cdef int j=0
        while i!=pos.shape[0]:
            positions_map[0][pos[i,0]].resize(3)
            j=i+1
            while pos[j,0]!=-1:
                positions_map[0][pos[i,0]][0].push_back(pos[j,0])
                positions_map[0][pos[i,0]][1].push_back(pos[j,1])
                positions_map[0][pos[i,0]][2].push_back(pos[j,2])
                j+=1
            i=j+1
        
        self.positions_map=positions_map
        
        assert self.positions_map[0].size()==self.ncc
#        
#        cdef vector[int] tmp
#        for i in range(self.st):
#            print "here2---",time_map[i].size()
#            for j in range(time_map[i].size()):
#                print time_map[i][j], 
#                print " "
#            print
#        
    ###
    def get_img(self):
        return self.current_image_color
    
    
    def set_sizes(self,int[:] true_sizes):
        self.sizes=true_sizes
    
    def get_sizes(self):
        return np.asarray(self.sizes)
    
    
    cpdef set_image_at_time(self, int timestep):
        self.reset_image()
        cdef int i,cc,x,y,npixels,j,sizecc,r,g,b
        if timestep>=0 and timestep<=self.st:
            for i in range(self.time_map[0][timestep].size()):
                cc=self.time_map[0][timestep][i]
                #print "cc = ",cc
                self.paint_obj(cc)
                    

    cdef void paint_obj(self,int cc_index) nogil:
        cdef int x,y,npixels,j,sizecc,r,g,b
        #print "sizecc = ",np.asarray(self.sizes)
        npixels=self.positions_map[0][cc_index][0].size()
        #print "npixels = ",npixels
        sizecc=self.sizes[cc_index]
        
        r=self.palette[sizecc,0]
        g=self.palette[sizecc,1]
        b=self.palette[sizecc,2]

        
        for j in range(npixels):
            x=self.positions_map[0][cc_index][1][j]
            y=self.positions_map[0][cc_index][2][j]
    
    
            self.current_image_color[x,y,0]=r
            self.current_image_color[x,y,1]=g
            self.current_image_color[x,y,2]=b
    
        
    
    
    def change_obj_size(self,int cc_index,int newsize):
        self.sizes[cc_index]=newsize
        self.paint_obj(cc_index)
    
    def get_size(self,int cc_index):
        return self.sizes[cc_index]
    
                    
                
                
        
    
    
    ### Debug    
    cpdef printTimeMap(self):
        cdef vector[int] tmp
        for i in range(self.st):
            print "here2---",self.time_map[0][i].size()
            for j in range(self.time_map[0][i].size()):
                print self.time_map[0][i][j], 
                print " "
            print
        
    
    cpdef printCCMap(self):
        print "hdhashdjadhs"
        for i in range(1,self.ncc+1):
            print "here2---",self.positions_map[0][i][0].size()
            for j in range(self.positions_map[0][i][0].size()):
                print self.positions_map[0][i][0][j], 
                print " "
            print
    
    
    
    
    
