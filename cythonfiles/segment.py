from b_segment import _cleanBorders, clean_small
import vigra
import numpy as np

def clean_small_segments(seg,min_size,max_size):
    seg=np.require(seg,np.uint32)
    shape=seg.shape
    seg=clean_small(seg.flatten(),min_size,max_size)
    seg=seg.reshape(shape)
    if seg.ndim==2:
        seg=vigra.analysis.labelImageWithBackground(seg.astype(np.float32),8).view(np.ndarray)
    else:
        seg=vigra.analysis.labelVolumeWithBackground(seg.astype(np.float32),26).view(np.ndarray)
    return seg
def cleanBorders(image,margin):
    res=_cleanBorders(image,margin)
    return vigra.analysis.labelImageWithBackground(res.astype(np.float32),4)

