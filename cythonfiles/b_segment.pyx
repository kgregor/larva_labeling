#cython: boundscheck=False
#cython: wraparound=False
cimport cython
import numpy as np

cimport numpy as np

import sys
from libc.math cimport log

def get_sizes(np.ndarray[np.uint32_t, ndim=1,mode="c"] seg):
    cdef int k
    cdef n=seg.shape[0]
    cdef maxlabel=np.max(seg)
    cdef int label
    cdef np.ndarray[np.uint32_t, ndim=1,mode="c"] h =np.zeros(maxlabel+1,np.uint32)
    
    for k in range(n):
        label=seg[k]
        h[label]+=1
    
    return h


def clean_small(np.ndarray[np.uint32_t, ndim=1,mode="c"] seg, int min_size,int max_size):
    cdef np.ndarray[np.uint32_t, ndim=1,mode="c"] sizes=get_sizes(seg)
    
    cdef int i
    for i in range(seg.shape[0]):
        if sizes[seg[i]]<=min_size or sizes[seg[i]]>max_size and seg[i]!=0:
            seg[i]=0
    
    return seg


def _cleanBorders(np.ndarray[np.uint32_t,ndim=2] image,int margin=50):
    # check if an object si entered in the margin then delete it from the segmentation
    up=image[:margin,:].flatten()
    down=image[-margin:,:].flatten()
    left=image[:,:margin].flatten()
    right=image[:,-margin:].flatten()
    
    cdef np.ndarray[np.uint32_t,ndim=1] interior=np.unique(image[margin:-margin,margin:-margin].flatten())
    
    cdef np.ndarray[np.uint32_t,ndim=2] res=image.copy()
    
    cdef np.ndarray[np.uint32_t,ndim=1] s
    s=np.concatenate([up,down,left,right])
    s=np.unique(s)
    cdef int k,el,el2
    cdef int i,j
    cdef int h,w
    h=image.shape[0]
    w=image.shape[1]
    cdef int k2
    cdef int found=0
    for k in range(1,s.shape[0]):
        el=s[k]
        found=0
        
        for k2 in range(1,interior.shape[0]):
            el2=interior[k2]
            if el2==el:
                found=1
                break
        
        if found==0:

            for i in range(h):
                for j in range(w):
                    if image[i,j]==el:
                        res[i,j]=0
                        
    return res 