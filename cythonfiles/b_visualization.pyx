#cython: boundscheck=False
#cython: wraparound=False
cimport cython
import numpy as np

cimport numpy as np

import sys
from libc.math cimport log


def color_image(np.uint8_t[:,:] image,int [:,:] labeledimg,int [:] true_sizes,int [:,:] palette):
    #Overlay the palette
    cdef int w,h
    cdef int x,y
    cdef float alpha=0.5
    w=labeledimg.shape[0]
    h=labeledimg.shape[1]
    
    cdef int [:] color
    cdef int id_cc,tsize
    
    cdef np.uint8_t[:,:,:] resimg=np.dstack([image,image,image])
    
    with nogil:
        for x in range(w):
            for y in range(h):
                id_cc=labeledimg[x,y]
                if id_cc!=0:
                    tsize=true_sizes[id_cc]
                    color=palette[tsize,:]
                    resimg[x,y,0]=<np.uint8_t>(alpha*resimg[x,y,0]+(1-alpha)*color[0])
                    resimg[x,y,1]=<np.uint8_t>(alpha*resimg[x,y,1]+(1-alpha)*color[1])
                    resimg[x,y,2]=<np.uint8_t>(alpha*resimg[x,y,2]+<np.uint8_t>(1-alpha)*color[2])
                    
            
    return resimg