cimport cython
import numpy as np

cimport numpy as np

import sys







def getPerimeterFromPos(np.ndarray[np.int32_t,ndim=2] pos):
    
    cdef np.ndarray[np.int32_t,ndim=2] perimeter
    cdef np.ndarray[np.int32_t,ndim=1] perimeter_marked=np.zeros(pos.shape[0],np.int32)
    cdef np.ndarray[np.int32_t, ndim=2] neigh8=np.asarray([[1,0],[0,1],[1,1],[-1,-1],[-1,0],[0,-1],[1,-1],[-1,1]]).astype(np.int32)
    
    
    cdef int x,y,shiftx,shifty,k,x2,y2
    cdef int isPerimeter=1
    cdef int neighfoun=0
    
    for k in range(pos.shape[0]):
        x=pos[k,0]
        y=pos[k,1]
        
        neighfound=0
        isPerimeter=1
        for kk in range(pos.shape[0]):
            x2=pos[kk,0]
            y2=pos[kk,1]
        
            for l in range(8):
                shiftx=neigh8[l,0]
                shifty=neigh8[l,1]
                if x2==shiftx+x and y2==shifty+y:
                    neighfound+=1
            
            if neighfound==8:
                isPerimeter=0
                break
        
        if isPerimeter==1:
            perimeter_marked[k]=1
        
    
    index=np.where(perimeter_marked==1)[0]
    perimeter=pos[index,:]
    
    
    
    return perimeter,np.sum(perimeter_marked)





def getHuMoments(np.ndarray[np.int32_t,ndim=2] pos,np.ndarray[np.float32_t,ndim=1] intensity):

    cdef np.ndarray[np.float32_t,ndim=2] Mu=np.zeros((4,4),np.float32)
    cdef np.ndarray[np.float32_t,ndim=1] I=np.zeros(7,np.float32)
    cdef np.ndarray[np.float32_t,ndim=2] Nu=np.zeros_like(Mu)
    
    cdef int npoints,p,q
    cdef float x,y,xm,ym

    
    npoints=pos.shape[0]
    xm=np.mean(np.float32(pos[:,0]))
    ym=np.mean(np.float32(pos[:,1]))
    
    
        
    
    
    x=x-xm
    y=y-ym
    
    for p in range(4):
        for q in range(4):
            Mu[p,q]=np.sum((x**p)*(y**q))
    
    
   
    for p in range(4):
        for q in range(4):
            Nu[p,q]=Mu[p,q]#/(Mu[0,0]**(1+(p+q)*0.5))
            
    I[0]=Nu[2,0]+Nu[0,2]
    I[1]=(Nu[2,0]-Nu[0,2])**2+4*Nu[1,1]**2
    I[2]=(Nu[3,0]-3*Nu[1,2])**2+(3*Nu[2,1]-Nu[0,3])**2
    I[3]=(Nu[3,0]+Nu[1,2])**2+(Nu[2,1]+Nu[0,3])**2
    
    I[4]=(Nu[3,0]-3*Nu[1,2])*(Nu[3,0]+Nu[1,2])*( (Nu[3,0]+Nu[1,2])**2-3*(Nu[2,1]+Nu[0,3])**2 )+(3*Nu[2,1]-Nu[0,3])*(Nu[2,1]+Nu[0,3])*( 3*(Nu[3,0]+Nu[1,2])**2-(Nu[2,1]+Nu[0,3])**2 )
    
    I[5]=(Nu[2,0]-Nu[0,2])*( (Nu[3,0]+Nu[1,2])**2-(Nu[2,1]+Nu[0,3])**2 )+4*Nu[1,1]*(Nu[3,0]+Nu[1,2])*(Nu[2,1]+Nu[0,3])
    I[6]=(3*Nu[2,1]-Nu[0,3])*(Nu[2,1]+Nu[0,3])*( 3*(Nu[3,0]+Nu[1,2])**2-(Nu[2,1]+Nu[0,3])**2 )-(Nu[3,0]-3*Nu[1,2])*(Nu[2,1]+Nu[0,3])*( 3*(Nu[3,0]+Nu[1,2])**2-(Nu[2,1]+Nu[0,3])**2 )
    
    return I
    
        
    
    


    