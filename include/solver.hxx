#ifndef SOLVER_HXX
#define SOLVER_HXX
#include <iostream>
#include <fstream>
#include <lemon/list_graph.h>
#include <lemon/euler.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <tr1/tuple>
#include <graph_utils.hxx>
#include <lemon/dim2.h>
#include <lemon/core.h>
#include <lemon/concepts/graph.h>
#include <ilcplex/ilocplex.h>
#include <ilconcert/iloenv.h>
#include <lemon/graph_to_eps.h>
#include <sstream>
#include <stdexcept>   // for exception, runtime_error, out_of_range

typedef std::map<int, std::vector<Point> > point_map;
typedef std::map<int, std::vector<float> > intensity_map;
typedef std::map<int,std::vector<int> > time_map;
typedef  std::vector<Point> point_vec;
typedef std::map<int,float> size_map;
typedef lemon::ListDigraph GraphType;
typedef GraphType::NodeIt nodeit;
typedef GraphType::OutArcIt outit;
typedef GraphType::Node nodetype;
typedef GraphType::ArcIt arcit;

namespace cplex_solver{
using namespace std;
using namespace std::tr1;
using namespace lemon;
typedef unsigned int uint8;

class Solver
{
public:
	/*
	 * PIXEL GRAPH!
	 */
	const GraphType& graph;

	nodetype red_s;
	nodetype green_s;
	nodetype red_t;
	nodetype green_t;

	const GraphType::ArcMap<double> & cost;
	const GraphType::ArcMap<double> & cap;
	const GraphType::NodeMap<tuple<double,double> > & demands;
	const GraphType::ArcMap<tuple<int,int> > & arcinfo;
	const GraphType::NodeMap< PixelInfo > &info_map;
	const GraphType::NodeMap< vector<GraphType::Node> > &neighbor_map;
	IloEnv env;
	IloModel model;
	IloCplex cplex;
	vector<IloNumVar> f_map; //store the variables with an integer

	map<int,vector<int> > _aux; //map an arc index to two variables

	int narcs;
	int nnodes;
	int nvars;

	 Solver(GraphType& graph_, const GraphType::ArcMap<double>& cost_,const GraphType::ArcMap<double>& cap_,
			 const GraphType::NodeMap<tuple<double,double> >& demands_,const GraphType::ArcMap<tuple<int,int> >& arcinfo_,
			 const GraphType::NodeMap< PixelInfo > &info_map_, const GraphType::NodeMap<vector<GraphType::Node> > &neighrbor_map_, nodetype red_s_,nodetype green_s_,nodetype red_t_,nodetype green_t_):
		graph(graph_), green_s(green_s_),red_s(red_s_),green_t(green_t_),red_t(red_t_),//f_map(graph_),
		cost(cost_),cap(cap_),demands(demands_),arcinfo(arcinfo_),info_map(info_map_), neighbor_map(neighrbor_map_)
		{
		 narcs=countArcs(graph);
		 cerr << "N Arcs here " << narcs << endl;
		 nnodes=countNodes(graph);
		 cerr << "N Nodes here " << nnodes <<endl;
		 nvars=arcIndToVarInd(1,narcs-1)+1;
		 cerr << "N Vars here " << nvars << endl;

		};

	 /*
		  * gets the results and creates a graphical output depending on the global variable CREATE_IMAGE
		  */
	 void getResultsAndSerialize(int X, int Y, int T, string name, int hypo, int threshdist){

		 cplex.out() << "CPLEX RESULT: " << cplex.getCplexStatus() << " Solution value = " << cplex.getObjValue() << endl;

		 cerr << "T: " << T << endl;

		 //write info to text file
		 std::stringstream folder;
		 folder << "output/with_threshold/" << name;


		 struct stat st = {0};
		 if(stat(folder.str().c_str(), &st) == -1) {
			 mkdir(folder.str().c_str(),0700);
		 }

		 std::stringstream filename;
		 filename << folder.str() << "/hypo" << hypo << ".txt";
		 std::string filename_str = filename.str();
		 std::ofstream myfile(filename_str.c_str());


		 cout << "WRITE TO " << filename_str << endl;

		 if (myfile.is_open())
		 {
			 myfile << "Solution Value for Hypothesis " << hypo << ":\n";
			 myfile << cplex.getObjValue() << "\n";
			 myfile << "Elapsed Time: \n";
			 myfile << cplex.getTime() << "\n";
			 myfile << "Threshold used for distance:\n";
			 myfile << threshdist;
			 myfile.close();
		 }
		 else cout << "Unable to open text file";

		 vigra::MultiArrayShape<4>::type shape(3,X,Y,T);
		 vigra::MultiArray<4, float> volume(shape,0.0);

		 int x,y,t;
		 float var_val =0;

		 cout << "ASDASDASDASDAS" << endl;

		 for (int k=0;k<2;k++)
		 {
			 cout << "BLABLA " << k << endl;

			 for (int i=0;i<narcs;i++)
			 {
				 const GraphType::Arc& arc= graph.arcFromId(i);

				 nodetype pixel = graph.target(arc);
				 PixelInfo info = info_map[pixel];

				 t = info.t;
				 x = info.x;
				 y = info.y;

				 if(t>T || x > X || y > Y) cout << "JAP: " << t << " " << T << " " << x << " " << X << " " << y << " " << Y;

				 var_val = cplex.getValue(f_map[arcIndToVarInd(k,i)]);

				 //do not draw source/sink nodes
				 if(t < 0) continue;

				 //get variable
				 volume(k,x,y,t) = 255*var_val;

			 }

		 }

		 cout << "HIER " << volume.shape(3) << endl;

		 // serialize
		 for(int k=0;k<volume.shape(3);k++)
		 {
			 std::stringstream filename_img;
			 filename_img << folder.str() << "/hypo" << hypo << "_" << k;

			 cout << "WRITE IMG TO " << filename_img.str() << endl;

			 vigra::MultiArrayView<3, float ,vigra::StridedArrayTag> image=volume.bindAt(3,k);
			 vigra::MultiArray<3, float> image2=image;
			// image2(0,0,0)=255;
			 float s=0.0;
			 for(int k1=0;k1<image2.shape(1);k1++)
				 for(int k2=0;k2<image2.shape(2);k2++)
					 s+=image2(0,k1,k2);

			 vigra::ImageExportInfo info((filename_img.str()+".png").c_str());
			 vigra::exportImage(vigra::srcImageRange(vigra::makeRGBImageView(image2)),info);
		 }


	 }



	 double solve()
	 {

		 double sol_val = -1;

		 try
		 {
			model = IloModel(env);

			cplex = IloCplex(model);
			cerr << "Setting the objective function " << endl;
			this->setObjective();
			cerr << "Setting the flow cosntrain " << endl;
			this->setFlowConstrain();
			cerr << "Setting the capacity constrain " << endl;
			this->setCapacityConstraint();
			cerr << "Solve the model" << endl;

			cerr << "rows " << cplex.getNrows() << endl;
			cerr << "cols " << cplex.getNcols() << endl;

			//cplex.setParam(IloCplex::RootAlg, IloCplex::Network);

			cplex.solve();

			for(int i=0;i<104;i++){
				if(cplex.getValue(f_map.at(i)) != 0) cerr << f_map.at(i).getName() << ": "<<cplex.getValue(f_map.at(i))
						<< " (cost " << cost[graph.arcFromId(varIndToArcInd(i))] << ")" << endl;
			}

			sol_val = cplex.getObjValue();

			cerr << sol_val << endl;
			cerr << cplex.getCplexStatus() << endl;


		 }
		 catch(Exception &e)
		 {
				std::cerr<<"EXCEPTION"<<std::endl;
		 }

		 return sol_val;
	 }
	private:
	 void setObjective()
	 {

		 IloExpr expr(env);
		 int varcount=0;

		 for (int k=0;k<2;k++)
		 {
			 for (int i=0;i<narcs;i++)
			 {
				 const GraphType::Arc& arc= graph.arcFromId(i);
				 //FIXME exchanged cap[arc] to 1000 just for a test

				 std::stringstream name;
				 name << "f(" << i << "," << k << "| " << get<0>(arcinfo[arc]) << "-->"<< get<1>(arcinfo[arc]) << ")";

				 f_map.push_back(IloNumVar(env,0,1000,IloNumVar::Float, name.str().c_str()));

				 expr+=f_map[varcount]*cost[arc];
				 varcount++;
			 }

		 }
		 cout << "N Arcs " << narcs << " VARCOUNT: " << varcount << endl;


		 //smoothness constraint
		 //IloExpr expr(env);

//		 cout << "SMOOTHNESS START" << endl;
//
//		 //go over all pixels
//		 for(nodeit pix(graph) ; pix!= INVALID; ++pix){
//
//
//			 //go over all colors
//			 for(int k=0 ; k<2 ; k++){
//
//				 int counter = 0;
//				 //the intensity is "stored" in the outgoing arc!
//				 //TODO better way than arc iterator? --> possibly arc properties
//				 for( GraphType::OutArcIt a(graph,pix); a!=INVALID; ++a){
//
//					 //if(counter > 1) break;
//
//					 //we are only interested in arcs between pixel clones
//					 if(cap[a] > 1) continue;
//
//					 IloNumVar & f_pix = this->f_map[arcIndToVarInd(k,graph.id(a))];
//
//					 //get neighbors
//					 vector<GraphType::Node> neighbors = neighbor_map[pix];
//
//					 assert (neighbors.size()<=8);
//
//					 for(int l=0 ; l<neighbors.size() ; l++){
//
//						 GraphType::Node neigh = neighbors.at(l);
//
//						 //for each of these neighbors, sum up the absolute difference between intensity
//						 //of pixel and neighbor
//						 for( GraphType::OutArcIt a2(graph,neigh); a2!=INVALID; ++a2){
//
//							 if(cap[a2] > 1) continue;
//
//							 IloNumVar & f_neigh = this->f_map[arcIndToVarInd(k,graph.id(a2))];
//
//							 //expr += IloAbs(f_pix - f_neigh);
//
//
//							 expr += IloAbs(f_pix-f_neigh);
//
//							 counter++;
//
//						 }
//
//					 }
//				 }
//
//			 }
//
//		 }

		 cerr << "SMOOTHNESS ADDED"<<endl;
		 model.add(IloMinimize(env,expr));
		 cerr << "done objective" << endl;

	 }

	 void setFlowConstrain()
	 {

		 for(nodeit n(graph);n!=INVALID;++n)
		 {


			 for(int k=0;k<2;k++)
			 {

				 IloExpr expr1(env);
				 IloExpr expr2(env);
				 double demand =0;

				// cerr << "VARS: ";
				 std::string color = "red";
				 if(k==1) color = "green";

				 //incoming flow
				 for( GraphType::InArcIt ia(graph,n); ia!=INVALID; ++ia)
				 {
					 int id_arc=graph.id(ia);
					 int var_ind=this->arcIndToVarInd(k,id_arc);

					// cerr << "+ " << color << "(" << get<0>(arcinfo[ia]) << "->" << get<1>(arcinfo[ia]) << ") ";

					 IloNumVar & f = this->f_map[var_ind];
					 expr1+=f;

				 }

				 //outgoing flow
				 for( GraphType::OutArcIt ia(graph,n); ia!=INVALID; ++ia)
				 {
					 int id_arc=graph.id(ia);
					 int var_ind=this->arcIndToVarInd(k,id_arc);
				//	 cerr << "- " << color << "(" << get<0>(arcinfo[ia]) << "->" << get<1>(arcinfo[ia]) << ") ";

					 IloNumVar & f = this->f_map[var_ind];
					 expr2+=f; //outgoing flow needs to be subtracted
				 }

				 if(k==0) demand = get<0>(demands[n]);
				 if(k==1) demand = get<1>(demands[n]);

				// cerr << " = " << demand <<  endl;


				// if(demand != 0) cerr << demand << endl;

				 model.add((expr1-expr2)==demand); //incoming = outgoing flow - demand

			 }

		 }

	 }

	 /*
	  * the colors of neighboring pixels should be more or less alike
	  */
	 IloExpr setSmoothnessConstraint(){

		 cout << "ADD SMOOTHNESS CONSTRAINT" << endl;

		 IloExpr expr(env);

		 //go over all pixels
		 for(nodeit pix(graph) ; pix!= INVALID; ++pix){


			 //go over all colors
			 for(int k=0 ; k<2 ; k++){


				 //the intensity is "stored" in the outgoing arc!
				 //TODO better way than arc iterator? --> possibly arc properties
				 for( GraphType::OutArcIt a(graph,pix); a!=INVALID; ++a){

					 //we are only interested in arcs between pixel clones
					 if(cap[a] > 1) break;

					 IloNumVar & f_pix = this->f_map[arcIndToVarInd(k,graph.id(a))];

					 //get neighbors
					 vector<GraphType::Node> neighbors = neighbor_map[pix];

					 assert (neighbors.size()<=8);

					 for(int l=0 ; l<neighbors.size() ; l++){

						 GraphType::Node neigh = neighbors.at(l);

						 //for each of these neighbors, sum up the absolute difference between intensity
						 //of pixel and neighbor
						 for( GraphType::OutArcIt a2(graph,neigh); a2!=INVALID; ++a2){

							 if(cap[a2] > 1) break;

							 IloNumVar & f_neigh = this->f_map[arcIndToVarInd(k,graph.id(a2))];

							 expr += IloAbs(f_pix - f_neigh);

						 }

					 }
				 }

			 }

		 }

		 cout << "in smoothness done" << endl;
		 return expr;

	 }


	 /*
	  * each arc can hold the flow of both colors
	  * but in one timestep, this is limited by the pixel's intensity
	  */
	 void setCapacityConstraint(){

		 for(arcit a(graph);a!=INVALID;++a)
		 {
			 IloExpr expr(env);
			 for(int k=0;k<2;k++)
			 {
				 int id_arc=graph.id(a);
				 int var_ind=this->arcIndToVarInd(k,id_arc);
				 expr += f_map[var_ind];
			 }
			 if(cap[a] != 1e+11) {
				 model.add(expr == cap[a]);
			 }
			 else {
				 model.add(expr <= cap[a]);
			 }

		 }

		 cerr<< "Number of constraints: " << cplex.getNrows() <<  endl;

	 }

	 int varIndToArcInd(int var_id){
		 if(var_id < narcs) return var_id;
		 return var_id-narcs;
	 }

	 int arcIndToVarInd(int k, int arc_id)
	 {
		 return arc_id +k*narcs;
	 }

};
} //end namespace cplex_solver
#endif
