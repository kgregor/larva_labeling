from pool import ThreadPool
import glob
import os
import sys
pool=ThreadPool(5)

def run(file_in_base,filename, file_out_base):
    
    try:
        file_in_base = file_in_base + filename + "/"
        sss = "python region_of_interest2.py 1 %s %s %s"%(file_in_base, filename, file_out_base)
        print sss
        os.system(sss)
    except Exception, e:
        print "ERROR"
        print e
        f=open("log_parallel.txt","w")
        f.write(filename+"\n")
        f.write(str(e)+"\n")
        f.close()

    
file_in_base="/home/kgregor/data/interesting_movies/"
file_out_base="/home/kgregor/data/rw_output/"
if not os.path.exists(file_out_base):
    os.mkdir(file_out_base)


files=sorted(glob.glob(file_in_base+"t*"))
# skiplist1=sorted(glob.glob(file_out_base+"t*"))
# skiplist=[]
# for sf in skiplist1:
#     skiplist+=[os.path.basename(sf)]
tmp=[]
for f in files:
    file = os.path.basename(f)
    #if file not in skiplist:
    tmp+=[file]
files=tmp
for f in files:
    print f
print "#############"


for filename in files:
    
    print file_in_base
    print filename
    print "-----------------"
    
    pool.add_task(run,file_in_base,filename,file_out_base)

pool.wait_completion()