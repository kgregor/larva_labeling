# -*- coding: utf-8 -*-
import h5py
import numpy as np

vol=15 * np.ones((3,1,5,5,1))

print vol

fh1=h5py.File("tiny.h5",'w')
g1=fh1.create_group("volume")
g1.create_dataset("data",data=vol)
fh1.close()
