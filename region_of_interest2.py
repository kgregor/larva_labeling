import numpy as np
import labeler
import randomwalker
import h5py
import sys
import cPickle
import color_tracking_result
import os
import logging
from copy import copy



logging.basicConfig(level=logging.DEBUG)


DEBUG=0
NMAX_ENCOUNTER=10
LIMIT = 10000000
MAX_RECURSION_DEPTH=1000
MAX_TIME_STEP = 10


class GraphRegion(object):
    
    def __init__(self,graph,positions,info,cc_subregion,cc_end,cc_start,K):
        self.graph=graph
        self.info=info
        self.cc_subregion=cc_subregion
        self.cc_end=cc_end
        self.cc_start=cc_start
        self.positions=positions
        self.new_positions=None #positions in the new coordinate system
        self.K = K
        
        self.startt=info[0]
        self.stopt=info[1]
        self.startx=info[2]
        self.stopx=info[3]
        self.starty=info[4]
        self.stopy=info[5]
        
        print "INFO: start time", self.startt, "end time" , self.stopt, "len x:", self.stopx-self.startx+1, "len y:", self.stopy-self.starty+1
        
        self.shape=(info[1]-info[0]+1,info[3]-info[2]+1,info[5]-info[4]+1) #spatial region shape out of the volume
        
        self.slicing=(slice(info[0],info[1]+1,None),slice(info[2],info[3]+1,None),slice(info[4],info[5]+1,None))
        
        self._get_new_coordinates()
        

    def _get_new_coordinates(self):
        self.new_positions={}
        info =self.info
        for cc_ind in self.cc_subregion:
            
            pos_new=np.array(self.positions[cc_ind])
            pos_new[:,0]-=info[0]
            pos_new[:,1]-=info[2]
            pos_new[:,2]-=info[4]
            
            self.new_positions[cc_ind]=pos_new
    
    def __str__(self):
        
        out="This is %s \n"%self.info
        out+="starting stuff %s"%self.cc_start
        out+="ending stuff %s"%self.cc_end
        
        return out
    
        
            

def visualize_region(data,region):
    
    print "data.shape",data.shape
    print "region", region.shape
    vol=np.zeros((region.shape),np.uint8)
    print vol.shape,"here",region.slicing
    print region.info
    vol[:,:,:]=data[region.slicing]
    vol.shape=vol.shape+(1,)
    vol=np.concatenate([vol,vol,vol],axis=-1)
    print "there"
    
    alpha = 0.6
    
    for cc in region.cc_subregion:
        p=region.new_positions[cc]
        
        print "PPPP", p
        
        color=[0,0,255]
        if cc in region.cc_start:
            color=[255,0,0]
        else:
            if cc in region.cc_end:
                color=[0,255,0]
            
        vol[p[:,0],p[:,1],p[:,2],0]=alpha*vol[p[:,0],p[:,1],p[:,2],0] + (1-alpha)*color[0]
        vol[p[:,0],p[:,1],p[:,2],1]=alpha*vol[p[:,0],p[:,1],p[:,2],1] + (1-alpha)*color[1]
        vol[p[:,0],p[:,1],p[:,2],2]=alpha*vol[p[:,0],p[:,1],p[:,2],2] + (1-alpha)*color[2]
        
    
    return vol
    

def get_info_to_serialize(total_series,positions):
    ##FIXME: rewrite clearly
    
    x_max =0
    y_max =0
    x_min =1000000
    y_min =1000000
    
#    print total_series
    print "LEN TOTAL SERIES", len(total_series)
    
    for i in range(len(total_series)):
        pixel = positions[total_series[i]]
        pixel=np.array(pixel)
        cmax=np.max(pixel,axis=0)
        cmin=np.min(pixel,axis=0)
        
        if cmax[1] > x_max:
            x_max = cmax[1]
        if cmin[1] < x_min:
            x_min = cmin[1]
        if cmax[2] > y_max:
            y_max = cmax[2]
        if cmin[2] < y_min:
            y_min = cmin[2]
    if DEBUG: print "MIN_X " , x_min , ", MAX_X " , x_max , ", MIN_Y " , y_min , " MAX_Y " , y_max
    
    #now get time steps!
    t_max=positions[total_series[-1]][0][0]
    t_min=positions[total_series[0]][0][0]
    
    if DEBUG: 
        print "MIN TIME STEP " , t_min
        print "MAX TIME STEP " , t_max
    
    assert x_min>=0 and y_min>=0

        
    left = x_min-5
    right = x_max+5    
    bottom = y_min-5
    top = y_max+5
    
    return [int(t_min), int(t_max), int(left), int(right), int(bottom), int(top)]


def write_dict_to_file(complete_positions):
    
    
    f = open("/home/kgregor/Desktop/dictionary2.txt","w")
    
    for cc in complete_positions:
        #print complete_positions[cc]
        f.write(str(cc)+"\n")
        for larva in complete_positions[cc]:
            #print "larva", larva
            f.write(str(larva)+":"+str(complete_positions[cc][larva])+"\n")
        f.write("==============================================\n")
   
    f.close()
    


def find_reconnections(inlarvae, outlarvae, connections, true_sizes, series):
    
    print "in", inlarvae
    print "out", outlarvae
    
    maxcc = np.max(outlarvae)
    maxrecursion=100000
    
    identities=[]
    paths=[]
    for cc in outlarvae:
        if connections[cc] > maxcc:
            break
        found=-1
        
        ccinner=copy(cc)
        
        counter=0
        inbetween=[]
        while(True):
            counter+=1
            if counter>maxrecursion: 
                break
            
            if len(connections[ccinner])!=1 or true_sizes[connections[ccinner][0]]!=1:
                break
            else:
                inbetween+=[ccinner]
                ccinner=connections[ccinner][0]
                if ccinner in inlarvae:
                    found=ccinner
                    print "3, found=", found
                    break
            
        if found!=-1:
            identities+=[(cc,found)]
            paths.append(inbetween+[found])
    
    print identities
    print paths
    for path in paths:
        for larva in path:
            if larva not in series:
                series.append(larva)



def is_relevant(cc, series,true_sizes,connections,K):
    #checks if a node is a candidate node start for an encounter
    
    
    if cc > LIMIT and DEBUG:
        return 0
    
    #if K-component is connected to another relevant K-component
    if true_sizes[cc] == K and len(connections[cc]) == 1 :
        if is_relevant(connections[cc][0], series,true_sizes,connections,K):
            series.append(cc)
            return 1
        
    #it should not merge with other components in the next time step
    if true_sizes[cc] == K and len(connections[cc]) > 1:
        sum_neigh = 0
        for neigh in connections[cc]:
            sum_neigh += true_sizes[neigh]
            #if true_sizes[connections[cc][0]]==1 and true_sizes[connections[cc][1]]==1 :
        if sum_neigh <= K:
            series.append(cc)
            return 1
    else:
        return 0

#a node is considered a start node, if it has size K, comes from components with smaller size
def is_start_node(cc,true_sizes,back_connections,used_K_nodes,K):
    if true_sizes[cc]!=K:
        return 0
    
    if cc in used_K_nodes:
        return 0
    
    if len(back_connections[cc])==1:
        return 0
    
    sum_back_neigh = 0
    for back_neigh in back_connections[cc]:
        sum_back_neigh += true_sizes[back_neigh]

    if sum_back_neigh > K:
        return 0
    
    return 1


#def find_heads(cc, series, depth,true_sizes,K):  
#    #we don't want to find clusters containing components bigger than our parameter K
#    if depth==0 or true_sizes[cc] > K:
#        return 0
#    
#    #if we find an isolated larva, stop there, and store information that it is a child
#    #because then we don't want to print its neighbors later on!
#    if true_sizes[cc] == 1:
#        if not cc in done:
#            series.append(cc)
#            done[cc]=1
#        return 1
    
        
def find_connections(cc, series, depth, from_top, done,true_sizes,looked_at,connections,back_connections,K,starting_cc,ending_cc):       
    #we don't want to find clusters containing components bigger than our parameter K
    #looked at check that things are not looked twice
    if depth==0 or true_sizes[cc] > K:
        return 0
    
    looked_at[cc] = 1
    series.append(cc)
    
    #if we find an isolated larva, stop there, and store information that it is a child
    #because then we don't want to print its neighbors later on!
    if true_sizes[cc] == 1:
        if not cc in looked_at:
            series.append(cc)
            done[cc]=1
#         if from_top ==1 :
#             #TODO is_child[cc] = 1
#             ending_cc.append(cc) #stores the ccs at the end of the subgraph
#         if from_top ==0:
#             starting_cc.append(cc) #stored the ccs at the start of the subgraph
        return 1
    
    #check for children
    if len(connections[cc])==0:
        return 0
    for child in connections[cc]:
        if child not in looked_at:
            if not find_connections(child, series, depth-1, 1,done,true_sizes,looked_at,connections,back_connections,K,starting_cc,ending_cc):
                return 0
    
    #print "PARENTS: " , back_connections[cc]
    if len(back_connections[cc])==0:
        return 0
    for parent in back_connections[cc]:
        if parent not in looked_at:
            if not find_connections(parent, series, depth-1, 0,done,true_sizes,looked_at,connections,back_connections,K,starting_cc,ending_cc):
                return 0
    
    if not cc in done:
        done[cc] = 1

    return 1

def find_starting_and_ending(series, starting_cc, ending_cc, connections, back_connections, true_sizes):
    for cc in series:
        if true_sizes[cc]==1:
            if len(connections[cc])==0:
                ending_cc.append(cc)
                continue
            if len(back_connections[cc])==0:
                starting_cc.append(cc)
                continue
            if connections[cc][0] not in series:
                ending_cc.append(cc)
                continue
            if back_connections[cc][0] not in series:
                starting_cc.append(cc)
                continue
            
            #necessary for the special case when larva disconnects and immediately reconnects
            if len(connections[cc]) == 1 and connections[cc][0] in series and true_sizes[connections[cc][0]]==1:
                ending_cc.append(cc)
            if len(back_connections[cc]) == 1 and back_connections[cc][0] in series and true_sizes[back_connections[cc][0]]==1:
                starting_cc.append(cc)

def is_start_of_single_trail(cc,true_sizes,connections,back_connections):
    
    if true_sizes[cc] != 1:
        return 0
    
    if len(back_connections[cc])==1:
        if true_sizes[back_connections[cc][0]]==1:
            return 0
#         if len(connections[cc]) > 1:
#             
#             childsum=0
#             for child in connections[cc]:
#                 childsum += true_sizes[child]
#             
#             if childsum != 1:
#                 return 0
    
    if len(back_connections[cc])>1:
        
        parentsum=0
        for parent in back_connections[cc]:
            parentsum += true_sizes[parent]
        if parentsum==1:
            return 0
        if len(connections[cc]) > 1:
            return 0
    
    return 1
      
  
def get_single_trail(cc, series,connections,true_sizes):
    while True:
        
        #print cc
        
#         if true_sizes[cc] != 1:
#             return
#         series.append(cc)
#         if len(connections[cc]) > 1 or len(connections[cc]) < 1 or connections[cc][0] == -1:
#             return
#         cc = connections[cc][0]
        
        
        if true_sizes[cc] != 1:
            return
        series.append(cc)
        if len(connections[cc]) < 1 or connections[cc][0] == -1:
            return
        if len(connections[cc]) >= 1:
            childrensum = 0
            for child in connections[cc]:
                childrensum += true_sizes[child]
                
            if childrensum != 1:
                return
        
        for child in connections[cc]:
            if true_sizes[child]==1:
                cc = child
        
        


class InfoProducer(object):
    def __init__(self,connections,Positions,times,pixel_sizes,true_sizes):
        
        self.cc_graph=connections
        self.cc_positions=Positions
        self.cc_pixel_sizes=pixel_sizes
        self.cc_times=times
        self.cc_true_sizes=true_sizes
        
        ########
        self.back_connections=None
        self.K=None
        
        self._create_back_map_cc()
        
    def __iter__(self):
        return self
    
    """
    creates the connection from each larva to its parents
    """
    def _create_back_map_cc(self):
        #Creates a cc component graph from child to parent
        #dictionary whether a node was already checked completely

        
        #create list of lists: each component gets a list with its "backneighbors"
        back_connections = [[] for i in range(LIMIT+400)] #make up for components in T+1
        for cc,neighbors in self.cc_graph.items():
            back_connections[cc]=[]
        
        #fill in backneighbors
        for cc,neighbors in self.cc_graph.items():
            for neigh_cc in neighbors:
                back_connections[neigh_cc].append(cc)
        
        self.back_connections=back_connections
        
    
    
    """
    extracts tracks of isolated larvae
    """
    def extract_K1(self, component_dictionary, complete_positions):
        #final_segmentation = np.zeros((1000,1400,1400),np.uint16)
        positions=self.cc_positions
        true_sizes=self.cc_true_sizes
        back_connections=self.back_connections
        connections=self.cc_graph
        times = self.cc_times
        
        
        counter=0
        
        all_infos=[]
        for cc, neighbors in connections.items():
            
            if counter > NMAX_ENCOUNTER and DEBUG>1:
                print "FOUDN ENOUGH"
                break
            series = [] ##Will contain the current trail
            if not is_start_of_single_trail(cc,true_sizes,connections,back_connections):
                continue
            
            if DEBUG>0: print "start of single trail:" , cc, "at time", times[cc]
            
            get_single_trail(cc, series,connections, true_sizes)
            
            if len(series) <= 1: ##if it is an isolated larva for one timestep between two bigger connected components
                continue
        
            info = get_info_to_serialize(series,positions)
            
            starting_cc=[series[0]]
            ending_cc=[series[-1]]
            
            #yield info,series,starting_cc,ending_cc
            
            #yield GraphRegion(connections,positions,info,series,ending_cc,starting_cc,1)
            
            print 
            
            #print "found single trail:", series
#            fh2=readH5(file_in_base+".h5","volume/labeledvol",onlyhandle=True)
              
        
            for cc in series:
                component_dictionary[cc]=starting_cc
                complete_positions[cc] = {}
                
                #TODO hier evtl starting cc beim zweiten?
                
                complete_positions[cc][cc] = positions[cc]
                
            print "SINGLE TRAIL LABELED"
            counter+=1
            
            all_infos.append(info)
        
        if DEBUG: print len(all_infos), "single trails have been labeled"
        #return all_infos
        
        
    def disable_K_nodes(self,series,used_K_nodes,K):
        for cc in series:
            if self.cc_true_sizes[cc] == K:
                used_K_nodes.append(cc)
    
    def _extract_K(self):
        #final_segmentation = np.zeros((1000,1400,1400),np.uint16)
        positions=self.cc_positions
        true_sizes=self.cc_true_sizes
        back_connections=self.back_connections
        connections=self.cc_graph
        K=self.K
        
        
        print "KKKKKKKKKKKKKK", K
        
        done = {}
        #save all the nodes containing K larvae to prevent them from being selected as start node later
        #if we didn't do that, the same series could be cropped out multiple times
        used_K_nodes = [] 
        looked_at = {}
        
        counter=0
    
        larva_map = {}
        #extract relevant sequence
        for cc,neighbors in connections.items():
            looked_at = {} #tells if in the current series the componets are already visited
            tails= []
            heads= []
            series = []
            D = {} #connections
            P = {} #positions
            
            starting_cc=[] #connections at the top of the subgraph
            ending_cc=[] #connections at the end of the subgraph
            
            comp_map = {}
            done = {}
            
            if counter>NMAX_ENCOUNTER and DEBUG : 
              print "FOUND ENOUGH"
              break
            
            
            if is_start_node(cc,true_sizes,back_connections,used_K_nodes,K):
                
                if DEBUG>0: print 'found start node' , cc, "back_conn:", back_connections[cc]
                
                if find_connections(cc, series, MAX_RECURSION_DEPTH,False,done,true_sizes,looked_at,connections,back_connections,K,starting_cc,ending_cc):              
                    series.sort(cmp=None, key=None, reverse=False)
                    find_starting_and_ending(series, starting_cc, ending_cc,connections,back_connections,true_sizes)
                    starting_cc.sort(reverse=False)
                    ending_cc.sort(reverse=False)
                    
                    if not LABEL_ALGO:
                        find_reconnections(starting_cc, ending_cc, connections, true_sizes, series)
                    
                    
                    info = get_info_to_serialize(series,positions)
                    
                    self.disable_K_nodes(series,used_K_nodes,K)
                 
                    assert len(starting_cc) == len(ending_cc) and len(starting_cc)>0
                 
                    yield GraphRegion(self.cc_graph,positions,info,series,ending_cc,starting_cc,K)
                    
                    
                    counter = counter + 1
                    
                    if DEBUG>1: print "TOTAL SERIES: " , series
                else:
                    if DEBUG>1: print "not a K =%d larvae  encounter"%K
                    continue

          
          
    
    def extract(self,K):
        self.K=K
        return self._extract_K()
            
    """"puts the result from the random walker into the dictionary and relabels
    the sequence as well as components connected to it, if that is necessary
    
    comp_map and pos_map are the results from the labeling algorithm
    
    component_dictionary is the dictionary mapping components to larvae. this dict is
        filled here with the results from the labeler(comp_map, pos_map)
        
    complete_positions is the BIG dictionary that contains all information and will be
        dumped to a pickle file in the end.
    
    """
    def relabel_rw_result(self,component_dictionary,comp_map,pos_map,el, complete_positions):
        #relabel region if start components of region have already been considered before!
        relabel = False
        #aux are the indices in the start of a region that have been relabeled before.
        #thus in the whole region those larvae need to be relabeled as well
        aux = {}
        relabel_end = False
        #this is the same as aux only for the indices in the end of a region
        #the two maps are needed to create the real mapping later on (which is called mapping)
        aux_end = {}
        aux_old = {}
    
        for cc in el.cc_start:
            #we need to relabel the fresh result!
            if cc in component_dictionary:
                #TODO WORKAROUND
                if len(component_dictionary[cc]) == 0:
                    component_dictionary[cc].append(cc)
                relabel = True
                aux[cc] = component_dictionary[cc][0]
                
                
                print "cc in cc start! cc=", cc, "aux[cc]=", aux[cc]

        for cc in el.cc_end:
            if cc in component_dictionary:
                if len(component_dictionary[cc]) == 0:
                    component_dictionary[cc].append(cc)
                relabel_end = True
                aux_end[cc] = comp_map[cc]
                aux_old[cc] = component_dictionary[cc][0]
                
                
                print "cc in cc end! cc=", cc, "aux_end[cc]=", aux_end[cc],"aux_old[cc]=", aux_old[cc]
                
        #assert len(complete_positions[9725]) > 0  
                
        for cc in comp_map:
            #print "cc", cc, "with larvae", comp_map[cc]
            if relabel==True:
                
                if cc in el.cc_start:
                    #the components are in cc_start, and relabel==True,
                    #that means, that these components have been relabeled and their
                    #entry in component_dictionary stays the same.
                    #we need to take care of the rest of the sequence now though. They need to be relabeled
                    #with the label that the start components have!
                    print "cc",cc,"already labeled!"
                else:
                    if not cc in component_dictionary:
                        component_dictionary[cc] = []  #TODO might not be necessary
                        print "cc", cc, "not in component_dictionary!!"
                        
                    #for all the larvae in the region, assign it the new label, if necessary. it need not be necessary
                    #for all larvae, because not all starting indices have been labeled before
                    for larva in comp_map[cc]:
                        
                        #only if a larva was in an already relabeled starting_cc, it needs to be relabeled.
                        #otherwise, it is just copied into the dictionary
                        if larva in aux:
                            if not aux[larva] in component_dictionary[cc]:
                                component_dictionary[cc].append(aux[larva])
                        else:
                            if not larva in component_dictionary[cc]:
                                component_dictionary[cc].append(larva)
                        
                    #cc was in the end of the sequence and was relabeled
                    if cc in el.cc_end:
                        aux_end[cc]=component_dictionary[cc]
            
            #just normal copying            
            else:
                component_dictionary[cc] = []
                for larva in comp_map[cc]:
                    if not larva in component_dictionary[cc]:
                        component_dictionary[cc].append(larva)
        

        #assert len(complete_positions[9725]) > 0  


        #create new mapping from aux_end and aux_old
        #this will be the mapping for when the last components of a region overlap with already labeled components
        #those already labeled components need to be relabeled then.
        mapping = {}
        for cc in aux_old:
            mapping[aux_old[cc]] = aux_end[cc]
            
        print "mapping done"
            
        if relabel_end == True:
            for cc,ll in component_dictionary.items():
                for oldkey, newkey in mapping.items():
                    if oldkey in component_dictionary[cc]:
                        component_dictionary[cc].remove(oldkey)
                        
                        #TODO HIER WAR EIN FEHLER!!!
                        
                        if len(newkey)==0:
                            print "newkey len ist null"
                            component_dictionary[cc].append(oldkey)
                        else:
                            if not newkey[0] in component_dictionary[cc]:
                                component_dictionary[cc].append(newkey[0])
                        #print "check for", oldkey, "to replace with", newkey[0]
                        #print component_dictionary[cc]
                        #print "-------"
        
        
        #assert len(complete_positions[9725]) > 0 
        
        
        #fill positions map
        for cc in pos_map:
            
            #these are the isolated larva at the beginning and end
            #their positions are already stored.
            if cc in complete_positions:
                continue
            
            aux_pos_map = {}
            for larva in pos_map[cc]:
                
                pos = pos_map[cc][larva]
                
                #int "POSMAP CC",pos_map[cc]
                #print "POS MAP CC LARVA", pos_map[cc][larva]
                if larva in component_dictionary:
                    #print "COMPONENTN DOICT LARVA", component_dictionary[larva]
                    aux_pos_map[component_dictionary[larva][0]]=pos
            complete_positions[cc] = aux_pos_map
            #print complete_positions[cc]
            
            #assert len(complete_positions[9725]) > 0 
            
            
        print "-----"
            
            
        print "===================================================================="   


"""
The main class, that gets a file and a labeling algorithm and uses that to produce a
dictionary of type 'component -> larva -> positions' and dumps it in a pickle file
"""
class PutTogether(object):
    
    """
    labeler is an object of a class that inherits from the labeler class,
    thus an algorithm that takes an encounter and returns
    """
    def __init__(self,file_in_base,filename,file_out_base,LABEL_ALGO):
        self.file_in_base = file_in_base
        self.file_out_base = file_out_base
        self.LABEL_ALGO = LABEL_ALGO

        if self.LABEL_ALGO:
            self.labeler = randomwalker.RandomWalker()
            print "labeler created"
        
        
    """
    main function which reads in a file, crops out the encounters
    and solves them given a labeling algorithm
    """
    def do(self):
    
        import pickle
        import mailme
        from tools import readH5
        import glob
        import os
        
        try:
        
            print "read data", filename
            from cythonfiles.connectivity import getPositionsOfComponent, deserializeGraphFast2
            connections,positions,sizes,times=deserializeGraphFast2(file_in_base+filename+".graph_fast")
            data=readH5(file_in_base+filename+".h5").astype(np.uint8).squeeze()
            true_sizes=readH5(file_in_base + filename+ ".res","graph_fast/entire/true_sizes")
            
            import pylab
            print "start"
            IF=InfoProducer(connections,positions,times,sizes,true_sizes)
            print "created infoproducer"
            
            for i in range(1,len(positions)):
                for j in range(len(positions[i])):
                    assert positions[i][j][0]>=0 and positions[i][j][1]>=0 and positions[i][j][2]>=0
            
            
            #this maps each component to the larvae it contains
            component_dictionary = {}
            complete_positions = {}
            
            if self.LABEL_ALGO:
                #labels the trails of single larvae
                IF.extract_K1(component_dictionary, complete_positions)
            
            
            #labels encounters with K larvae
            for K in range(2,10):
                
                counter= 0
                
                for el in IF.extract(K):
                    counter+=1
                    
                    if DEBUG>0 and counter > NMAX_ENCOUNTER:
                        break
                    
                    print "CCSTART",el.cc_start
                    print "CCEND", el.cc_end
                    
                    print "Crop out Region!"
                    data_cropped=data[el.slicing].astype(np.int32).swapaxes(0,-1)
                    data_cropped = data_cropped/255.0
                    print "data cropped"
                    lvol_cropped=readH5(file_in_base+filename+".h5","volume/labeledvol",onlyhandle=True)[el.slicing[0],0,el.slicing[1],el.slicing[2],0].swapaxes(0,-1).astype(np.int32)
                    segmentation=readH5(file_in_base+filename+".h5","volume/segmentation",onlyhandle=True)[el.slicing[0],0,el.slicing[1],el.slicing[2],0].astype(np.int32).swapaxes(0,-1)
                    segmentation_cropped=segmentation*(lvol_cropped>0)
                    
                    
                    lvol2_cropped=np.zeros_like(lvol_cropped)
                    for comp in el.cc_subregion:
                        lvol2_cropped += np.where(lvol_cropped == comp, comp, 0)
                    segmentation_cropped=segmentation*(lvol2_cropped>0)
                
                    print "start algorithm with K=",K
                    
                    if self.LABEL_ALGO:
                        self.labeler.setup(data_cropped, lvol2_cropped, segmentation_cropped, el.info,el.cc_start,el.cc_end,IF.cc_times,IF.cc_positions, K)
                        try:
                            comp_map, pos_map = self.labeler.label_sequence()
                        except Exception, ex:
                            logging.exception("Exception in Region of Interest!")
                            f=open("log_rw.txt","a")
                            f.write(filename+"\n")
                            f.write(str(ex)+"\n")
                            a,b,c = sys.exc_info()
                            f.write("in line"+str(c.tb_lineno)+"\n")
                            f.write("This is an exception caused by the labeling algorithm\n")
                            f.write("---------------------\n")
                            f.close()
                            
                        print "random walker done"
                        try:
                            IF.relabel_rw_result(component_dictionary,comp_map,pos_map,el,complete_positions) 
                        except Exception, exc:
                            print "didnt work"
                            logging.exception("Exception in Region of Interest!")
                            f=open("log_rw.txt","a")
                            f.write(filename+"\n")
                            f.write(str(exc)+"\n")
                            a,b,c = sys.exc_info()
                            f.write("in line"+str(c.tb_lineno)+"\n")
                            f.write("This is an exception caused by the relabeling_rw_result function \n")
                            f.write("---------------------\n")
                            f.close()
                    else:
                        if not os.path.exists(file_out_base+filename):
                            os.mkdir(file_out_base+filename)
                    
                        fh=h5py.File(file_out_base+filename+"/encounter_"+str(K)+"_"+str(counter).zfill(2)+".h5","w")
                        g=fh.require_group("volume")
                        tmpshape = data_cropped.shape
                        data_out = data_cropped[None,None,:,:,:].swapaxes(0,-1).swapaxes(2,3)
                        lvol_out = lvol2_cropped[None,None,:,:,:].swapaxes(0,-1).swapaxes(2,3)
                        seg_out = segmentation_cropped[None,None,:,:,:].swapaxes(0,-1).swapaxes(2,3)
                        g.create_dataset(name="data",data=data_out, compression=1)
                        g.create_dataset(name="labeledvol", data=lvol_out,  compression=1)
                        g.create_dataset(name="segmentation", data=seg_out, compression=1)
                        fh.close()
                        del el.positions
                        del el.graph
                        cPickle.dump(el, open(file_out_base+filename+"/encounter_"+str(K)+"_"+str(counter).zfill(2)+".pkl","wb"))
             
             
            if self.LABEL_ALGO:           
                #general new mapping
                for cc in complete_positions:
                    
                    aux_dict = complete_positions[cc]
                    complete_positions[cc]={}
                    for larva in aux_dict:
                        if larva not in component_dictionary:
                            complete_positions[cc][larva] = aux_dict[larva]
                            continue
                        if len(component_dictionary[larva]) == 1:
                            complete_positions[cc][component_dictionary[larva][0]] = aux_dict[larva]
                        
                        
                        
                print "write to pickle file"
                cPickle.dump(complete_positions, open(file_out_base+filename+"_comp_dict.pkl","wb"))
                print "dumped pickle file"
                
        except Exception, e:
            print "didnt work"
            logging.exception("Exception in Region of Interest!")
            f=open("log_rw.txt","a")
            f.write(filename+"\n")
            f.write(str(e)+"\n")
            a,b,c = sys.exc_info()
            f.write("in line"+str(c.tb_lineno)+"\n")
            f.write("This is an exception caused by the region_of_interest file\n")
            f.write("---------------------\n")
            f.close()
            
            
"""
Usage: python region_of_interest2.py [flag 1 for labeling or 0 for only cropping] [file_in_base] [filename] [file_out_base]
"""            
if __name__=="__main__":
    
    args = sys.argv
    LABEL_ALGO = int(str(args[1]))
    file_in_base=str(args[2])
    filename=str(args[3])
    file_out_base=str(args[4])
    
    if not os.path.exists(file_out_base):
        os.mkdir(file_out_base)
    
    PT = PutTogether(file_in_base, filename, file_out_base, LABEL_ALGO)
    PT.do()
    
    
    blabablabl