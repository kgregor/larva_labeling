"""
This file just takes all pickled dictionaries and applies the converter to movies,
which is the file "color_tracking_result.py"
"""

import cPickle
from tools import readH5
import numpy as np
import color_tracking_result
import os
import glob

if __name__ == "__main__":
    
    
    args = sys.argv
    file_in_base_dict=str(args[1])
    file_in_base=str(args[2])
    filename=str(args[3])
    file_out_base=str(args[4])
    
    if not os.path.exists(file_out_base):
        os.mkdir(file_out_base)
    
    #file_in_base_dict="/home/kgregor/Desktop/bigset/"
    #file_in_base="/home/kgregor/Desktop/1000frames_newseg/"
    #file_out_base="/home/kgregor/Desktop/bigset/"
    
    # files=sorted(glob.glob(file_in_base+"t*"))
    # tmp = []
    # for f in files:
    #     file = os.path.basename(f)
    #     tmp+=[file]
    # files=tmp
    # for f in files:
    #     print f
    # print "#############"
    
    files = [filename]
    
    
    
    for filename in files:
        if not os.path.exists(file_out_base+filename):
            os.mkdir(file_out_base+filename)
    
        #file_in_base += filename + "/"
    
        complete_positions = cPickle.load(open(file_in_base_dict+filename+"_comp_dict.pkl","r"))
        data=readH5(file_in_base+filename+".h5").astype(np.uint8).squeeze()
        cres = color_tracking_result.ColorResult(data,file_out_base+filename,complete_positions)
        cres.serialize()
            