
#include <iostream>
#include <lemon/list_graph.h>
#include <lemon/euler.h>
#include <math.h>


#include <graph_utils.hxx>
#include <lemon/dim2.h>
#include <lemon/core.h>
#include <lemon/concepts/graph.h>
#include <ilcplex/ilocplex.h>
#include <ilconcert/iloenv.h>
#include <lemon/graph_to_eps.h>
#include <sstream>
#include <stdexcept>   // for exception, runtime_error, out_of_range
#include <solver.hxx>
#include <tr1/tuple>
#include <vigra/multi_array.hxx>

using namespace std;
using namespace std::tr1;
using namespace lemon;

int HYPO = -1;
int thresh_dist=-1;
//TODO get from data
int X = -1;
int Y = -1;
string name = "";


void check(bool goodThingHappened,string message="")
{
	try
	{
		if (goodThingHappened==0)
		{    throw std::runtime_error(message);
		}
	}
	catch(std::exception const& e)
	{
		std::cerr << "Exception: " << e.what() << "value " << goodThingHappened << "\n";
		throw e;
	}
}



//bool arePixelNeighbors(Point a, Point b){
//
//	return (!p.equals(pp) && abs(pp.x-p.x)<=1 && abs(pp.y-p.y)<=1);
//
//}

template < class P>
bool close_enough(P a,P b)
{
	//cerr << (a.t-b.t)*(a.t-b.t) << "KKK ";
	check( int((a.t-b.t)*(a.t-b.t))==1,"time is wrong what ??");
	//cerr << (int((a.t-b.t)*(a.t-b.t))==1) << " #### "<< endl;
	double d=max(abs(a.x-b.x),abs(a.y-b.y));
	return d < thresh_dist;
}

template < class P>
double compute_cost(P a,P b)//, vigra::MultiArray<4, float>& opt_flow)
{
	double d=sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
	double cost = d*d;

	if(d>5){
		cost = 100;
	}

	//float of1 = opt_flow(a.x,a.y,0,a.t);
	//float of2 = opt_flow(a.x,a.y,1,a.t);

	std::stringstream ss;

	ss << "shape3 " << a.t;

	//check(a.x<opt_flow.shape(0),"shape0");
	//check(a.y<opt_flow.shape(1),"shape1");
	//check(a.t<opt_flow.shape(3),ss.str());


	//cerr << "pixel (" << a.x << "," << a.y << ") with opt flow " << of1 << ", " << of2 << endl;

	float dir1 = b.x - a.x;
	float dir2 = b.y - a.y;

	//float res = dir1 * of1 + dir2 * of2;
	//if(res <0) res =0;

	//res*=10;

	//res += 1;

	return cost;
	//return  cost/res;
}



double compute_capacity(PixelInfo P)
{
	double cap=P.norm_intensity;
	return  cap;
}


typedef std::map<int, std::vector<Point> > point_map;
typedef std::map<int, std::vector<float> > intensity_map;
typedef std::map<int,std::vector<int> > time_map;
typedef std::vector<Point> point_vec;
typedef std::map<int,float> size_map;
typedef lemon::ListDigraph GraphType;
typedef GraphType::NodeIt nodeit;
typedef GraphType::OutArcIt outit;
typedef GraphType::Node nodetype;


template < class G >
void print_graph(const G& g)
{
	for (typename G::NodeIt n(g); n != INVALID; ++n)
	{
		cout << "Connection: " << g.id(n) << " -> ";
		for (typename G::OutArcIt e(g, n); e != INVALID; ++e)
		{
			cout << g.id(g.target(e)) << " , ";
		}
		cout << endl;
	}

}


void print_point_map(point_map& pos_map)
{
	for(point_map::iterator it=pos_map.begin(); it!=pos_map.end();it++)
	{
		cout <<" cc_id = " << it->first << " has " << (*it).second.size() << " pixels ";
		cout <<endl;

		point_vec & vec=(*it).second;
		for (int i=0;i<vec.size();i++)
		{
			cout << "(" << vec[i].t << "," << vec[i].x << "," << vec[i].y << ") ";
		}
		cout << endl << "##############" << endl;
	}
}



void print_intensity(intensity_map& int_map)
{
	for(intensity_map::iterator it=int_map.begin(); it!=int_map.end();it++)
	{
		cout <<" cc_id = " << it->first << " has " << (*it).second.size() << " pixels ";
		cout <<endl;

		vector<float> & vec=(*it).second;
		for (int i=0;i<vec.size();i++)
		{
			cout << vec[i] << " , ";
		}
		cout << endl << "##############" << endl;
	}
}


//template < class T >
//void downsample_graph(T & original_g  , point_map & original_posmap, intensity_map & original_intmap,size_map & original_true_size,
//		GraphType & g  , point_map & posmap, intensity_map & intmap,size_map & true_size, int Tstart, int Tstop)
//{
//	map<int,int> relabellig;
//	int count=0;
//	for(nodeit n(g); n != INVALID; ++n)
//	{
//		int idx=g.id(n);
//		if (original_posmap[idx][0].t>=Tstart && original_posmap[idx][0].t<Tstop)
//		{
//			g.addNode();
//		}
//
//
//
//	}
//}
SubDigraph< ListDigraph >
downsample_graph(MyGraph& g, point_map& posmap, int TStart,int TStop)
{
	ListDigraph::NodeMap<bool> node_filter(g,false);
	ListDigraph::ArcMap<bool> edge_filter(g,true);
	for (nodeit n(g); n != INVALID; ++n)
	{
		int idx= g.id(n);
		if (posmap[idx][0].t>=TStart && posmap[idx][0].t<TStop)
		{
			node_filter[n]=true;
		}
	}

	SubDigraph< ListDigraph > sg(g, node_filter, edge_filter);
	return sg;
}

void printPixelGraph(GraphType& pixel_graph, GraphType::NodeMap< PixelInfo >& info_map){

	cerr << "#######################################################" << endl;

	int old_cc =0;

	for (nodeit n(pixel_graph); n != INVALID; ++n)
	{

		int curr_cc = info_map[n].cc_ind;

		if(curr_cc != old_cc) cerr << endl;

		int idx= pixel_graph.id(n);
		cerr << "Node #" << idx << " (component " << info_map[n].cc_ind << "("<< info_map[n].true_size_cc << "," << info_map[n].pixel_size_cc << ") , position ("<< info_map[n].x << "," <<info_map[n].y << "), intensity " << info_map[n].intesity << ") connected to: ";

		for(GraphType::OutArcIt a(pixel_graph,n); a != INVALID; ++a){
			cerr << pixel_graph.id(pixel_graph.target(a)) << " (" << info_map[pixel_graph.target(a)].cc_ind <<") ,";
		}

		cerr << endl;

		old_cc = curr_cc;

	}
	cerr << "#######################################################" << endl;


}



int main(int argc, char *argv[]) {

	if(argc < 3){
		cout << "USAGE: ./test Filename MaxDistanceOfMovement" << endl;
		cout << "EXAMPLE: ./test test1 10" << endl;
		cout << "MaxDistanceOfMovement specifies the maximal Manhattan distance flow can travel from one time step to another." << endl;
		cout << "When you get infeasibilities, try using a higher distance" << endl;

		return -1;
	}

	thresh_dist = strtol(argv[2],NULL,10);
	name = argv[1];
	//Problem specification
	//string base="/home/lfiaschi/workspace/wormscpp/";
	string base="/";
	//string name = "tiny";
	//string name = "test1";

	string folder="home/kgregor/Desktop/1000frames_newseg/final_encounters/";
	string graphfile=base+folder+name+".graph_fast";
	string true_size_file=base+folder+name+".true_sizes";
	string datafile=base+folder+name+".h5";
	//string of_file = base+"encounters_OF/"+name+"_OF.h5";

	cout << "TRY FILE: " << graphfile << endl;

	int hyp = 0;

	//MAKE LOOP FOR HYPOTHESIS!!!
	for(hyp=0;hyp<2;hyp++){

		///////////////////////////////////////////////////////
		MyGraph g; //graph of the connected component indexes
		point_map pos_map; //contains positions of all pixels
		intensity_map int_map;
		size_map true_sizes;
		size_map pixel_size;
		time_map t_map; //contains at each entry i a vector of components living at time step i
		//std::map<int, vector< tuple<float, float> > > of_map;

		deserializeGraphFast(graphfile, "entire", g,-1);
		cerr << "graph deserialized" << endl;
		deserializePositions(graphfile, "entire", pos_map,-1);
		cerr << "positions deserialized" << endl;
		//function returns a point with the size of the images
		Point XY = deserializeIntensities(datafile, pos_map, int_map);
		X = XY.x+2;
		Y = XY.y+2;
		cerr << "intensities deserialized" << endl;
		deserializeTrueSizes(true_size_file,"entire",true_sizes);
		cerr << "true sizes deserialized" << endl;
		//vigra::MultiArray<4,float> opt_f= deserializeOF(of_file);
		//cerr << "optical flow deserialized" << endl;

		//cerr << int_map


		int count=0;
		for(nodeit n(g) ; n!=INVALID ; ++n){
			count++;
		}

		cerr << "NUMBER OF NODES IN g: " << count << ", maxnodeid: " << g.maxNodeId() << endl;

		for(int i=0;i<true_sizes.size();i++){
			cerr << "TRUE SIZE AT " << i << " : " << true_sizes[i] << endl;
		}

		//Construct time and size map
		int max_t = -1;

		for (nodeit n(g); n != INVALID; ++n)
		{
			int comp_idx= g.id(n)+1;
			pixel_size[comp_idx]=pos_map[comp_idx].size();
			int time_step=pos_map[comp_idx][0].t;
			if (t_map.count(time_step)==0)
			{	t_map[time_step]= vector<int>(); }

			t_map[time_step].push_back(comp_idx);
			if(time_step > max_t){
				max_t = time_step;
			}
		}

		int TStart=0;  //including
		int TStop=max_t+1; //excluding

		cerr << "MAX T: " << max_t << endl;

		for(int l=0;l<t_map.size();l++){

			cerr << l << ": ";

			for(int ll=0; ll<t_map[l].size(); ll++){
				cerr << t_map[l].at(ll) << " ";
			}
			cerr << endl;
		}

		check(t_map[TStart].size()==2,"too many components for start");
		cerr << "here " << t_map[TStop-1].size() << endl;
		check(t_map[TStop-1].size()==2,"too many components for stop");




		/// Subset the original graph
		ListDigraph::NodeMap<bool> node_filter(g,false);
		ListDigraph::ArcMap<bool> edge_filter(g,true);
		for (nodeit n(g); n != INVALID; ++n)
		{
			int idx= g.id(n)+1;
			if ((pos_map[idx][0].t>=TStart && pos_map[idx][0].t<TStop) || TStop==-1)
			{
				node_filter[n]=true;
			}
		}


		SubDigraph< ListDigraph > subgraph(g, node_filter, edge_filter);



		///////////////////////
		double maxcap=100000000000;
		GraphType pixel_graph;
		GraphType::ArcMap<double> cap(pixel_graph,maxcap);
		GraphType::ArcMap<double> cost(pixel_graph,0);
		GraphType::ArcMap<tuple<int, int> > arcinfo(pixel_graph,tuple<int, int> (0,0));
		GraphType::NodeMap< PixelInfo > info_map(pixel_graph);
		GraphType::NodeMap<vector<GraphType::Node> > neighbor_map(pixel_graph);

		//Append pixels nodes
		int counter=0;
		map <int,vector<nodetype> > map_cc_pixel_nodes;


		int min_n=1000000000;
		int max_n=-1;
		for (SubDigraph< ListDigraph > ::NodeIt n(subgraph); n != INVALID; ++n)
		{
			int idx=subgraph.id(n);
			if (min_n>=idx) min_n=idx;
			if (max_n<=idx) max_n=idx;
		}

		cerr<< "minmax" <<min_n<<", " <<max_n << endl;

		//go through components
		for (int idx=min_n+1;idx<=max_n+1;idx++)
		{   nodetype n=subgraph.nodeFromId(idx-1);
			//int idx=subgraph.id(n);
			int npixels=pos_map[idx].size();

			double cc_total_int =0;

			//get component's total intensity (sum of intensities)
			for(int k=0; k<npixels; k++){
				cc_total_int += int_map[idx][k];
			}

			//go through all pixels in the current component idx
			for(int k=0;k<npixels;k++)
			{

				nodetype v=pixel_graph.addNode();
				Point & p=pos_map[idx][k];

//				//for each pixel, find its direct pixelneighbors around it
//				vector<Point> neighborpixels = vector<Point>();
//				for(int l=0; l<npixels; l++){
//					Point & pp = pos_map[idx][l];
//					if(!p.equals(pp) && abs(pp.x-p.x)<=1 && abs(pp.y-p.y)<=1){
//						neighborpixels.push_back(pp);
//					}
//				}


				//TODO true_sizes[idx+1]
				PixelInfo info(p,idx,true_sizes[idx],int_map[idx][k],cc_total_int,counter);//, neighborpixels);
				info_map[v]=info;
				counter++;
				map_cc_pixel_nodes[idx].push_back(v);

			}
		}


		///////////////////////////////////////////////////////////////////////////////
		///					GIVE EACH PIXEL NODE ITS NEIGHBORS						///
		///////////////////////////////////////////////////////////////////////////////

		//TODO: the way we do it now, comparisons are done multiple times!
		//e.g. A is neighbor of B => compare A with B, but later:
		//we find B is neighbor of A => compare B with A etc...

		for (nodeit pix(pixel_graph);pix!=INVALID;++pix){

			neighbor_map[pix] = vector<GraphType::Node>();
			PixelInfo pix_info = info_map[pix];


			for (nodeit other_pix(pixel_graph);other_pix!=INVALID;++other_pix){

				PixelInfo neigh_info = info_map[other_pix];

				if(abs(pix_info.x-neigh_info.x)<=1 && abs(pix_info.y-neigh_info.y)<=1)
					neighbor_map[pix].push_back(other_pix);


			}


		}





		int subgraph_maxid = subgraph.maxNodeId();

		///////////////////////////////////////////////////////////////////////
		///			CREATE ARCS BETWEEN COMPONENTS							///
		///////////////////////////////////////////////////////////////////////

		//go through all pixels
		for(int i=0; i <= subgraph.maxNodeId(); i++){

			//find corresponding component
			nodetype comp = subgraph.nodeFromId(i);
			//cerr << "corresponding component " << subgraph.id(comp) << endl;

			//find neighboring components
			for (SubDigraph< ListDigraph >::OutArcIt a(subgraph, comp); a != INVALID; ++a)
			{

				nodetype t_comp= subgraph.target(a);
				int id_targetcomp=subgraph.id(t_comp)+1;
				int id_comp=subgraph.id(comp)+1;
				int npixel_node=map_cc_pixel_nodes[id_comp].size();
				int npixel_target=map_cc_pixel_nodes[id_targetcomp].size();

				//go through all pixels of this neighbor component
				for (int k=0;k<npixel_node;k++)
					for(int j=0;j<npixel_target;j++)
					{
						nodetype& u= map_cc_pixel_nodes[id_comp][k];
						nodetype& v= map_cc_pixel_nodes[id_targetcomp][j];
						PixelInfo& info_u= info_map[u];
						PixelInfo& info_v= info_map[v];

						//add potential arcs
						if(close_enough(info_u,info_v))
						{
							ListDigraph::Arc arc= pixel_graph.addArc(u,v);
							get<0>(arcinfo[arc]) = pixel_graph.id(u);
							get<1>(arcinfo[arc]) = pixel_graph.id(v);

							cost[arc]=compute_cost(info_u,info_v);//, opt_f);
							cap[arc]=maxcap;
						}
					}
			}
		}

		///////////////////////////////////////////////////////////////////////////////
		//			CREATE THE CAPACITY ARCS FOR EACH PIXEL							///
		///////////////////////////////////////////////////////////////////////////////

		int mm = countNodes(pixel_graph);
		//for all pixels
		for (int i = 0; i < mm; i++) {

			//split each pixel and create an arc (that will conserve the intensity)
			nodetype n = pixel_graph.nodeFromId(i);
			nodetype m = pixel_graph.split(n, false);
			ListDigraph::Arc arc = pixel_graph.addArc(n, m);
			get<0> (arcinfo[arc]) = pixel_graph.id(n);
			get<1> (arcinfo[arc]) = pixel_graph.id(m);
			info_map[m] = info_map[n];

			//reassign arclabels
			for (GraphType::OutArcIt a(pixel_graph, m); a != INVALID; ++a) {
				get<0> (arcinfo[a]) = pixel_graph.id(m);
			}

			int tmp = 0; //check
			//set capacity to intensity of the pixel
			for (GraphType::InArcIt ai(pixel_graph, m); ai != INVALID; ++ai) {
				tmp += 1; //check
				cap[ai] = compute_capacity(info_map[n]);
				cost[ai] = 0;
			}
			check(tmp == 1, "more than one node");
		}

		///////////////////////////////////////////////////////////////////////////////////
		///					APPEND SOURCE AND SINK										///
		///////////////////////////////////////////////////////////////////////////////////

		//Tuple: First entry is for demand of red, second for demand of green
		GraphType::NodeMap<tuple<double,double> > demands(pixel_graph,tuple<double,double> (0,0));

		nodetype red_s= pixel_graph.addNode();
		nodetype green_s= pixel_graph.addNode();
		nodetype red_t= pixel_graph.addNode();
		nodetype green_t =pixel_graph.addNode();
		get<0>(demands[red_s]) = -1;
		get<0>(demands[red_t]) = 1;
		get<1>(demands[green_s]) = -1;
		get<1>(demands[green_t]) = 1;


		int ccr_start=t_map[TStart][0];
		int ccg_start=t_map[TStart][1];
		int ccr_end, ccg_end;



		if(hyp == 0){

			ccr_end=t_map[TStop-1][1];
			ccg_end=t_map[TStop-1][0];

		}

		else {
			ccr_end=t_map[TStop-1][0];
			ccg_end=t_map[TStop-1][1];
		}

		for (nodeit n(pixel_graph);n!=INVALID;++n)
		{
			PixelInfo& info =info_map[n];

			if (ccr_start==info.cc_ind){
				//check for incoming arcs
				int num_in = 0;
				for(GraphType::InArcIt ai(pixel_graph,n);ai!=INVALID;++ai)	num_in++;

				if(num_in == 0) {
					ListDigraph::Arc arc= pixel_graph.addArc(red_s,n);
					get<0>(arcinfo[arc]) = pixel_graph.id(red_s);
					get<1>(arcinfo[arc]) = pixel_graph.id(n);
					cost[arc]=0;
					cap[arc]=1e11;
				}
			}

			if (ccg_start==info.cc_ind){
				//check for incoming arcs
				int num_in = 0;
				for(GraphType::InArcIt ai(pixel_graph,n);ai!=INVALID;++ai)	num_in++;

				if(num_in == 0) {
					ListDigraph::Arc arc= pixel_graph.addArc(green_s,n);
					get<0>(arcinfo[arc]) = pixel_graph.id(green_s);
					get<1>(arcinfo[arc]) = pixel_graph.id(n);
					cost[arc]=0;
					cap[arc]=1e11;
				}
			}

			if (ccg_end==info.cc_ind) {
				//check for outgoing arcs
				int num_out = 0;
				for(GraphType::OutArcIt ai(pixel_graph,n);ai!=INVALID;++ai)	num_out++;

				if(num_out == 0){
					ListDigraph::Arc arc= pixel_graph.addArc(n,green_t);
					get<0>(arcinfo[arc]) = pixel_graph.id(n);
					get<1>(arcinfo[arc]) = pixel_graph.id(green_t);
					cost[arc]=0;
					cap[arc]=1e11;
				}
			}


			if (ccr_end==info.cc_ind){
				//check for outgoing arcs
				int num_out = 0;
				for(GraphType::OutArcIt ai(pixel_graph,n);ai!=INVALID;++ai)	num_out++;

				if(num_out == 0) {
					ListDigraph::Arc arc= pixel_graph.addArc(n,red_t);
					get<0>(arcinfo[arc]) = pixel_graph.id(n);
					get<1>(arcinfo[arc]) = pixel_graph.id(red_t);
					cost[arc]=0;
					cap[arc]=1e11;
				}
			}

		}

		////
		//Print space for the integer linear program formulation
		cout<< " Starting the linear Program " << endl;
		cout << " the program will have " << countArcs(pixel_graph) << " arcs " << endl;
		cout << " the program will have " << countNodes(pixel_graph) << " nodes " << endl;


		cplex_solver::Solver solv(pixel_graph,cost,cap,demands,arcinfo, info_map, neighbor_map,red_s,green_s,red_t,green_t);

		double sol_val = solv.solve();

		cout << "SERIALIZE!!" << endl;

		solv.getResultsAndSerialize(X,Y,max_t+1,name,hyp, thresh_dist);

	}

	return 0;
}

