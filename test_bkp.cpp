
#include <iostream>
#include <lemon/list_graph.h>
#include <lemon/euler.h>
#include <math.h>


#include <graph_utils.hxx>
#include <lemon/dim2.h>
#include <lemon/core.h>
#include <lemon/concepts/graph.h>
#include <ilcplex/ilocplex.h>
#include <ilconcert/iloenv.h>
#include <lemon/graph_to_eps.h>
#include <sstream>
ILOSTLBEGIN

using namespace lemon;
using namespace std;

const int DISTANCE =8;
int HYPO;
//FIXME this needs to be read from data!!!
int AVG_SIZE;
const bool CREATE_IMAGE=true;
const int X=100;
const int Y=100;
const int T=48;
int mode; //0: from file, 1: tiny example



typedef lemon::ListDigraph::NodeMap<bool> bool_n_map;
typedef lemon::ListDigraph::NodeMap<int> int_n_map;
typedef lemon::ListDigraph::NodeMap<Point> point_n_map;
typedef lemon::ListDigraph::NodeMap<float> float_n_map;
typedef lemon::ListDigraph::ArcMap<float> float_a_map;
typedef lemon::ListDigraph::ArcMap<IloNumVar> var_a_map;
typedef lemon::ListDigraph::NodeMap<IloNumVar> var_n_map;
typedef lemon::ListDigraph::ArcMap<bool>  bool_a_map;



class Solver{
public:

	MyGraph g;
	std::map<int, std::vector<Point> > pos_map;
	std::map<int, std::vector<float> > int_map;


	IloEnv env;
	IloModel model;
	ListDigraph graph_pixel;
	bool_n_map cc_map;
	bool_n_map meta_map;
	int_n_map color_map;//0=not a color, 1=red, 2=blue
	bool_n_map is_color_map; //for testing purposes
	point_n_map points_map;
	float_n_map intensity_map;
	float_a_map cost_map;
	var_a_map f_map;
	var_n_map m_map;
	bool_a_map is_f_var;
	float_n_map true_sizes_map;
	float_n_map comp_total_intensity_map;
	lemon::ListDigraph::NodeMap<lemon::dim2::Point<int> > point2d_map;

	Solver(): cc_map(graph_pixel),meta_map(graph_pixel), color_map(graph_pixel),is_color_map(graph_pixel),
			points_map(graph_pixel), intensity_map(graph_pixel),cost_map(graph_pixel),f_map(graph_pixel),m_map(graph_pixel),
			is_f_var(graph_pixel),true_sizes_map(graph_pixel),comp_total_intensity_map(graph_pixel), point2d_map(graph_pixel){};


	float cost(Point &a, Point &b){
		float cost = (a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y);
		return cost*cost;
	}

	/*
	 *	tells if two pixel lie in a small enough neighborhood
	 */
	bool closeEnough(Point a, Point b) {
		return (distance(a,b)<DISTANCE);
	}


	float distance(Point &a, Point &b){
		return max(abs(a.x-b.x),abs(a.y-b.y));
	}

	/*
	 * for printing eps of graph
	 */
	lemon::dim2::Point<int> getPoint2D(Point p){
		return lemon::dim2::Point<int>(p.x,p.y);
	}

	/*
	 * calls the functions that construct the lp and solve it
	 */
	void execute(std::string file)
	{

		parseGraph(file);
		constructGraphPixel();

		//GET COMPONENT SUBGRAPH
		FilterNodes<ListDigraph> comp_graph(graph_pixel, cc_map);
		FilterNodes<ListDigraph> meta_graph(graph_pixel, meta_map);
		FilterNodes<ListDigraph> color_graph(graph_pixel, is_color_map);
		FilterArcs<ListDigraph> var_graph(graph_pixel, is_f_var);

		constructConnections(comp_graph,meta_graph, color_graph);

		cerr<<"GRAPH IS CORRECT: "<<checkGraph(comp_graph,meta_graph,color_graph)<<endl;

		//printGraphToEPS();
		//outputGraph(comp_graph,meta_graph, color_graph);

		try{

			model = IloModel(env);
			cerr<<"MODEL SET UP"<<endl;
			setObjective(var_graph, color_graph);
			cerr<<"OBJECTIVE SET"<<endl;
			setConstraints(comp_graph, meta_graph, color_graph);
			cerr<<"CONSTRAINTS SET"<<endl;
			setHypothesis(comp_graph, meta_graph, color_graph);
			cerr<<"HYPOTHESIS SET, NUMBER: "<<HYPO<<endl;
			IloCplex cplex(model);

			//cplex.setParam(IloCplex::BarEpComp, 1e-2);
			//cplex.setParam(IloCplex::EpOpt, 1e-2);
			//cplex.setParam(IloCplex::RootAlg, IloCplex::Primal);
			//cplex.setParam(IloCplex::ObjLLim, 1330);
			//cplex.setParam(IloCplex::ItLim, 67000);
			//cplex.setParam(IloCplex::DualObj)

			cplex.solve();
			getResultsAndSerialize(cplex, comp_graph, meta_graph, color_graph);
			cerr<<"PROGRAM DONE. RESULTS SERIALIZED"<<endl;

		}catch(Exception &e){
			std::cerr<<"EXCEPTION"<<std::endl;
		}

	}

	/*
	 * Parses the Graph depending on the global variable "mode"
	 * mode = 0 --> Graph from file
	 * mode = 1 --> Tiny example
	 */
	void parseGraph(std::string filename){

		if(mode==0){
			std::string graphfile = filename;
			graphfile+=".graph_fast";

			std::string datafile = filename;
			datafile+=".h5";

			cout<<"FILE TOTAL NAME: "<<graphfile<<endl;

			deserializeGraphFast(graphfile, "entire", g,T);

			deserializePositions(graphfile, "entire", pos_map,T);

			deserializeIntensities(datafile, pos_map, int_map);
		}
		if(mode==1){
			/*
			 *  FOR TESTING PURPOSES
			 */
			for(int j=0; j<14; j++){
				g.addNode();
			}
			for(int j=0;j<6;j++){
				g.sizes[g.nodeFromId(j)]=120;
				g.times[g.nodeFromId(j)]=j/2;
			}
			for(int j=6;j<12;j++){
				g.sizes[g.nodeFromId(j)]=240;
				g.times[g.nodeFromId(j)]=j-3;
			}
			g.sizes[g.nodeFromId(12)]=120;
			g.sizes[g.nodeFromId(13)]=120;
			g.times[g.nodeFromId(12)]=9;
			g.times[g.nodeFromId(13)]=9;

			for(int j=0; j<5; j++){
				g.addArc(g.nodeFromId(j),g.nodeFromId(j+2));
			}
			for(int j=5;j<12;j++){
				g.addArc(g.nodeFromId(j),g.nodeFromId(j+1));
			}
			for(int j=11; j<12; j++){
				g.addArc(g.nodeFromId(j),g.nodeFromId(j+2));
			}


			//COMPONENT 1
			for(int j=0; j<4; j+=2){
				for(int k=0;k<5;k++){
					pos_map[j].push_back(Point(j/2,10+j/2,14+k-j));
					pos_map[j].push_back(Point(j/2,11+j/2,14+k-j));
					int_map[j].push_back(13);
					int_map[j].push_back(13);
				}
			}
			for(int k=0;k<4;k++){
				pos_map[4].push_back(Point(2,11,11+k));
				pos_map[4].push_back(Point(2,12,11+k));
				int_map[4].push_back(17);
				int_map[4].push_back(12);
			}
			pos_map[4].push_back(Point(2,13,11));
			pos_map[4].push_back(Point(2,13,12));
			int_map[4].push_back(16);
			int_map[4].push_back(12);

			std::cerr<<"COMPONENT 0"<<std::endl;


			for(int k=0;k<pos_map[0].size();k++){
				std::cerr<< pos_map[0].at(k).toString() << "; ";
			}
			std::cerr<<std::endl;


			/*
			 * OVERLAP PART
			 */

			//t=3
			for(int k=0;k<3;k++){
				pos_map[6].push_back(Point(3,11,11+k));
				pos_map[6].push_back(Point(3,12,11+k));
				int_map[6].push_back(14);
				int_map[6].push_back(14);
			}
			pos_map[6].push_back(Point(3,13,11));
			int_map[6].push_back(14);
			pos_map[6].push_back(Point(3,13,12));
			int_map[6].push_back(14);
			pos_map[6].push_back(Point(3,14,11)); //overlap
			int_map[6].push_back(28);
			pos_map[6].push_back(Point(3,14,12));
			int_map[6].push_back(14);

			pos_map[6].push_back(Point(3,14,10));
			int_map[6].push_back(14);
			for(int k=0;k<4;k++){
				pos_map[6].push_back(Point(3,15+k,10));
				int_map[6].push_back(14);
				pos_map[6].push_back(Point(3,15+k,11));
				int_map[6].push_back(14);
			}

			//t=4
			for(int k=0;k<5;k++){
				pos_map[7].push_back(Point(4,11+k,10));
				int_map[7].push_back(14);

				pos_map[7].push_back(Point(4,11+k,12));
				int_map[7].push_back(14);
			}

			pos_map[7].push_back(Point(4,11,11)); int_map[7].push_back(14);
			pos_map[7].push_back(Point(4,12,11)); int_map[7].push_back(14);
			pos_map[7].push_back(Point(4,13,11)); int_map[7].push_back(28);
			pos_map[7].push_back(Point(4,14,11)); int_map[7].push_back(28);
			pos_map[7].push_back(Point(4,15,11)); int_map[7].push_back(28);
			pos_map[7].push_back(Point(4,16,11)); int_map[7].push_back(14);
			pos_map[7].push_back(Point(4,17,11)); int_map[7].push_back(14);

			//t=5
			for(int k=0;k<4;k++){
				pos_map[8].push_back(Point(5,11+k,12));
				int_map[8].push_back(14);
			}

			pos_map[8].push_back(Point(5,11,11)); int_map[8].push_back(14);
			pos_map[8].push_back(Point(5,12,11)); int_map[8].push_back(28);
			pos_map[8].push_back(Point(5,13,11)); int_map[8].push_back(28);
			pos_map[8].push_back(Point(5,14,11)); int_map[8].push_back(28);
			pos_map[8].push_back(Point(5,15,11)); int_map[8].push_back(14);
			pos_map[8].push_back(Point(5,16,11)); int_map[8].push_back(14);

			pos_map[8].push_back(Point(5,12,10)); int_map[8].push_back(14);
			pos_map[8].push_back(Point(5,13,10)); int_map[8].push_back(28);
			pos_map[8].push_back(Point(5,14,10)); int_map[8].push_back(28);
			pos_map[8].push_back(Point(5,15,10)); int_map[8].push_back(14);
			pos_map[8].push_back(Point(5,16,10)); int_map[8].push_back(14);

			//t=6
			pos_map[9].push_back(Point(6,12,12)); int_map[9].push_back(14);
			pos_map[9].push_back(Point(6,13,12)); int_map[9].push_back(14);
			pos_map[9].push_back(Point(6,14,12)); int_map[9].push_back(14);

			pos_map[9].push_back(Point(6,11,11)); int_map[9].push_back(14);
			pos_map[9].push_back(Point(6,12,11)); int_map[9].push_back(28);
			pos_map[9].push_back(Point(6,13,11)); int_map[9].push_back(28);
			pos_map[9].push_back(Point(6,14,11)); int_map[9].push_back(28);
			pos_map[9].push_back(Point(6,15,11)); int_map[9].push_back(14);

			pos_map[9].push_back(Point(6,11,10)); int_map[9].push_back(14);
			pos_map[9].push_back(Point(6,12,10)); int_map[9].push_back(14);
			pos_map[9].push_back(Point(6,13,10)); int_map[9].push_back(28);
			pos_map[9].push_back(Point(6,14,10)); int_map[9].push_back(28);
			pos_map[9].push_back(Point(6,15,10)); int_map[9].push_back(14);

			pos_map[9].push_back(Point(6,13,9)); int_map[9].push_back(14);
			pos_map[9].push_back(Point(6,14,9)); int_map[9].push_back(14);



			//t=7
			pos_map[10].push_back(Point(7,10,11)); int_map[10].push_back(14);
			pos_map[10].push_back(Point(7,11,11)); int_map[10].push_back(14);
			pos_map[10].push_back(Point(7,12,11)); int_map[10].push_back(14);
			pos_map[10].push_back(Point(7,13,11)); int_map[10].push_back(28);
			pos_map[10].push_back(Point(7,14,11)); int_map[10].push_back(28);

			pos_map[10].push_back(Point(7,10,10)); int_map[10].push_back(14);
			pos_map[10].push_back(Point(7,11,10)); int_map[10].push_back(14);
			pos_map[10].push_back(Point(7,12,10)); int_map[10].push_back(14);
			pos_map[10].push_back(Point(7,13,10)); int_map[10].push_back(28);
			pos_map[10].push_back(Point(7,14,10)); int_map[10].push_back(28);

			pos_map[10].push_back(Point(7,13,9)); int_map[10].push_back(14);
			pos_map[10].push_back(Point(7,14,9)); int_map[10].push_back(14);

			pos_map[10].push_back(Point(7,13,8)); int_map[10].push_back(14);
			pos_map[10].push_back(Point(7,14,8)); int_map[10].push_back(14);

			pos_map[10].push_back(Point(7,13,7)); int_map[10].push_back(14);
			pos_map[10].push_back(Point(7,14,7)); int_map[10].push_back(14);

			//t=8
			for(int k=0;k<5;k++){
				pos_map[11].push_back(Point(8,13,5+k)); int_map[11].push_back(14);
				pos_map[11].push_back(Point(8,14,5+k)); int_map[11].push_back(14);
			}

			for(int k=0;k<5;k++){
				pos_map[11].push_back(Point(8,9+k,10)); int_map[11].push_back(14);
				pos_map[11].push_back(Point(8,9+k,11)); int_map[11].push_back(14);
			}

			/*
			 * NO MORE OVERLAP
			 */

			for(int k=0;k<5;k++){
				pos_map[12].push_back(Point(9,13,4+k));
				pos_map[12].push_back(Point(9,14,4+k));
				int_map[12].push_back(14);
				int_map[12].push_back(14);
			}

			//COMPONENT 2
			for(int j=1; j<6; j+=2){
				for(int k=0;k<5;k++){
					pos_map[j].push_back(Point(j/2,17+k-j/2,10));
					int_map[j].push_back(14);
					pos_map[j].push_back(Point(j/2,17+k-j/2,11));
					int_map[j].push_back(14);
				}
			}
			for(int k=0;k<5;k++){
				pos_map[13].push_back(Point(9,8+k,10));
				pos_map[13].push_back(Point(9,8+k,11));
				int_map[13].push_back(14);
				int_map[13].push_back(14);
			}

			std::cerr<<"COMPONENT 1"<<std::endl;


			for(int k=0;k<pos_map[1].size();k++){
				std::cerr<< pos_map[1].at(k).toString() << "; ";
			}
			std::cerr<<std::endl;


		}

		int sum_pos=0;
		int sum_int=0;
		for(int k=0; k<14; k++){
			sum_int+=int_map[k].size();
			sum_pos+=pos_map[k].size();
		}

		std::cerr<<"SIZES: "<<sum_pos<<" & "<<sum_int<<std::endl;


	}

	/*
	 * Constructs new graph from parsed one
	 */
	void constructGraphPixel(){
		int N=g.maxNodeId();
		int counter=0;

		for (int i=0;i<=N;i++) {

			int comp_time = g.times[g.nodeFromId(i)];

			ListDigraph::Node x = graph_pixel.addNode();
			counter++;
			cc_map[x] = true;
			meta_map[x]=false;
			color_map[x]=0;
			is_color_map[x]=false;
			ListDigraph::Node n=g.nodeFromId(i);
			true_sizes_map[x] = floor(g.sizes[n]/AVG_SIZE);
			float comp_total_int = 0;
			vector<float> & temp_intensity = int_map[i];

			if(true_sizes_map[x] != 2)
				point2d_map[x]=dim2::Point<int>(600*(i%2),comp_time*100);
			else
				point2d_map[x]=dim2::Point<int>(300,comp_time*100);

			for (int j = 0; j < temp_intensity.size(); ++j) {

				comp_total_int += temp_intensity.at(j);

			}

			comp_total_intensity_map[x] = comp_total_int;

		}
		std::cerr<<counter<<" COMPONENTS CREATED"<<std::endl;

		//now get the outgoing arcs and create arcs between the components
		for (ListDigraph::NodeIt n(g); n != INVALID; ++n){
			for (ListDigraph::OutArcIt e(g, n); e != INVALID; ++e) {
				int id1= g.id(n);
				int id2= g.id(g.target(e));


				ListDigraph::Arc arc = graph_pixel.addArc(graph_pixel.nodeFromId(id1),graph_pixel.nodeFromId(id2));
				is_f_var[arc] = false;
			}
		}


		/// Print out
		for (ListDigraph::NodeIt n(graph_pixel); n != INVALID; ++n){
			std::cerr << "Node True Size "<< true_sizes_map[n] << ". Connection: " << graph_pixel.id(n) << " -> ";
			for (ListDigraph::OutArcIt e(graph_pixel, n); e != INVALID; ++e) {

				std::cerr << graph_pixel.id(graph_pixel.target(e)) << " , ";
			}
			std::cerr << std::endl;
		}

		int meta_counter=0;
		int color_counter=0;

		//go through the components and create structure with meta and color nodes
		for (int i=0; i<=N; ++i) {



			const vector<float> & temp_intensity = int_map[i];
			const vector<Point> & temp_pos = pos_map[i];

			ListDigraph::Node comp= graph_pixel.nodeFromId(i);

			for (int j = 0; j < temp_pos.size(); ++j) {

				ListDigraph::Node meta = graph_pixel.addNode();

				//add meta node and save position in points_map
				meta_counter++;
				meta_map[meta] = true;
				points_map[meta] = temp_pos.at(j);
				point2d_map[meta]=lemon::dim2::Point<int>(point2d_map[comp].x,point2d_map[comp].y+50);

				//FIXME removed: +1e-7 in (comp_total... )
				intensity_map[meta] = temp_intensity.at(j)/(comp_total_intensity_map[comp])*true_sizes_map[comp];

				cc_map[meta]=false;	color_map[meta]=0;	is_color_map[meta]=false;

				ListDigraph::Arc arc = graph_pixel.addArc(comp,meta);
				cost_map[arc]=0; is_f_var[arc] = false;

				//add two color nodes
				ListDigraph::Node red = graph_pixel.addNode();
				ListDigraph::Node blue = graph_pixel.addNode();
				color_counter+=2;
				color_map[red] = 1;
				color_map[blue] = 2;
				is_color_map[blue]=true; is_color_map[red]=true;
				meta_map[red]=false; meta_map[blue]=false;
				cc_map[red]=false; cc_map[blue]=false;
				m_map[red] = IloNumVar(env,0,1,IloNumVar::Float);
				m_map[blue] = IloNumVar(env,0,1,IloNumVar::Float);
				points_map[red]=temp_pos.at(j);
				points_map[blue]=temp_pos.at(j);
				point2d_map[red]=lemon::dim2::Point<int>(point2d_map[comp].x-6*temp_pos.size()+3*j,point2d_map[comp].y+100);
				point2d_map[blue]=lemon::dim2::Point<int>(point2d_map[comp].x+6*temp_pos.size()-3*j,point2d_map[comp].y+100);

				//add arcs from meta to the color nodes and sets 0 cost
				ListDigraph::Arc arc1 = graph_pixel.addArc(meta, red);
				ListDigraph::Arc arc2 = graph_pixel.addArc(meta, blue);
				cost_map[arc1]=0; cost_map[arc2]=0;
				is_f_var[arc1]=false; is_f_var[arc2]=false;
			}


		}


		std::cerr<<"META NODES: "<<meta_counter<<" COLOR NODES: "<<color_counter<<std::endl;

	}

	/*
	 * Creates the flow arcs in the new graph and their according cplex variables
	 */
	void constructConnections(FilterNodes<ListDigraph> &comp_graph, FilterNodes<ListDigraph> &meta_graph,FilterNodes<ListDigraph> &color_graph){
		//go through the graph and connect pixels if in neighborhood
		//first through the components
		int i = 0;
		for (FilterNodes<ListDigraph>::NodeIt comp(comp_graph); comp != INVALID; ++comp) {

			//this finds a component's neighbor components
			//FIXME: workaround cannot get a subgraph of a directed graph???
			vector<ListDigraph::Node> neighbors;
			for (ListDigraph::OutArcIt e(graph_pixel, comp); e != INVALID; ++e) {
				ListDigraph::Node curr_node = graph_pixel.target(e);
				if (cc_map[curr_node] == true){
					neighbors.push_back(curr_node);
				}
			}

			//here we find a component's meta nodes
			for (ListDigraph::OutArcIt e(graph_pixel, comp); e != INVALID; ++e) {
				ListDigraph::Node curr_meta_node = graph_pixel.target(e);
				if(meta_map[curr_meta_node]==true){
					ListDigraph::Node curr_neighbor;

					//now find the meta nodes of neighboring components
					//go through all neighbors of the component related to curr_node(=meta node)
					for (std::vector<ListDigraph::Node>::size_type i = 0; i!= neighbors.size(); i++) {
						curr_neighbor = neighbors[i];

						//get neighbor's meta nodes
						for (ListDigraph::OutArcIt b(graph_pixel, curr_neighbor); b != INVALID; ++b) {

							ListDigraph::Node curr_neighbor_meta_node = graph_pixel.target(b);
							if (cc_map[curr_neighbor_meta_node] == false) { //c_n_o is meta node!


								if (closeEnough(points_map[curr_meta_node],points_map[curr_neighbor_meta_node])) {
									if(distance(points_map[curr_meta_node],points_map[curr_neighbor_meta_node])<0){
										cerr<<"AAAAAAAAH"; return;
									}
									//now loop over all possible colors
									for (ListDigraph::OutArcIt color(graph_pixel, curr_meta_node); color != INVALID; ++color) {

										for (ListDigraph::OutArcIt d(graph_pixel, curr_neighbor_meta_node); d != INVALID; ++d) {
											if(color_map[graph_pixel.target(color)]==color_map[graph_pixel.target(d)]){

												//create arc and set cost
												ListDigraph::Arc f = graph_pixel.addArc(graph_pixel.target(color),graph_pixel.target(d));
												cost_map[f]=cost(points_map[curr_meta_node],points_map[curr_neighbor_meta_node]);
												is_f_var[f] = true;
												f_map[f] = IloNumVar(env,0,1,IloNumVar::Float);

											}

										}
									}
								}
							}
						}
					}
				}
			}


		}




	}


	void outputGraph(FilterNodes<ListDigraph> &comp_graph, FilterNodes<ListDigraph> &meta_graph,FilterNodes<ListDigraph> &color_graph){

		for (FilterNodes<ListDigraph>::NodeIt comp(comp_graph); comp != INVALID; ++comp) {

			cerr<<"##########################################"<<endl;
			cerr<<"NODE "<<graph_pixel.id(comp)<<endl;

			//metas
			for (ListDigraph::OutArcIt e(graph_pixel, comp); e != INVALID; ++e) {

				if(cc_map[graph_pixel.target(e)]) continue;

				//colors
				for (ListDigraph::OutArcIt b(graph_pixel, graph_pixel.target(e)); b != INVALID; ++b) {

					ListDigraph::Node color = graph_pixel.target(b);

					cerr<<"color node #"<<graph_pixel.id(color)<<", color: "<<color_map[color]<<", info: "<<points_map[color].toString()<<endl;
					cerr<<"........connected to "<<endl;

					for (ListDigraph::OutArcIt f(graph_pixel, color); f != INVALID; ++f) {

						ListDigraph::Node color2 = graph_pixel.target(f);

						cerr<<"........color node #"<<graph_pixel.id(color2)<<", color: "<<color_map[color2]<<", info: "<<points_map[color2].toString()<<" (distance of flow: "<<distance(points_map[color2],points_map[color])<<")"<<endl;
						if(distance(points_map[color2],points_map[color]) > DISTANCE){
							throw "FUUUUUUUUUUCK";
						}


					}

					cerr<<endl;

				}


			}

		}


	}

	/*
	 * prints graph to an eps file in the build folder
	 */
	void printGraphToEPS(){

		cerr<<"PRINT GRAPH!"<<endl;

		Palette palette;
		  Palette paletteW(true);

		  ListDigraph::NodeMap<int> sizeNode(graph_pixel,1);

		  graphToEps(graph_pixel, "graph.eps").nodeSizes(sizeNode).coords(point2d_map).nodeScale(.005).arcWidthScale(.01).nodeColors(composeMap(palette, color_map)).title("BLABLABLA").run();
//		    .coords(coords)
//		    .title("Sample EPS figure")
//		    .copyright("(c) 2003-2010 LEMON Project")
//		    .absoluteNodeSizes().absoluteArcWidths()
//		    .nodeScale(2).nodeSizes(sizes)
//		    .nodeShapes(shapes)
//		    .nodeColors(composeMap(palette, colors))
//		    .arcColors(composeMap(palette, acolors))
//		    .arcWidthScale(.4).arcWidths(widths)
//		    .nodeTexts(id).nodeTextSize(3)
//		    .run();
	}

	/*
	 * sets the objective function to the cplex program
	 */
	void setObjective(FilterArcs<ListDigraph> &var_graph, FilterNodes<ListDigraph> &color_graph){
		//traverse graph and add up all f's with their according cost
		IloExpr myExpr(env);
		int varcount=0;
		for (FilterArcs<ListDigraph>::ArcIt a(var_graph); a != INVALID; ++a) {
			myExpr += f_map[a] * cost_map[a];
			varcount++;
		}
		std::cerr << "VARCOUNT: " << varcount << std::endl;

		model.add(IloMinimize(env,myExpr));
	}

	/*
	 * checks graph for consistency
	 */
	bool checkGraph(FilterNodes<ListDigraph> &comp_graph, FilterNodes<ListDigraph> &meta_graph,FilterNodes<ListDigraph> &color_graph){


		//check if meta nodes are only connected to color nodes
		for (FilterNodes<ListDigraph>::NodeIt m(meta_graph); m != INVALID; ++m) {

			for (ListDigraph::OutArcIt a(graph_pixel, m); a != INVALID; ++a) {

				ListDigraph::Node color = graph_pixel.target(a);

				if(meta_map[color] != 0 || color_map[color] == 0 || cc_map[color] != 0){
					return false;
				}

				for (ListDigraph::OutArcIt e(graph_pixel, color); e != INVALID; ++e) {

					ListDigraph::Node color2 = graph_pixel.target(e);

					if(meta_map[color2] != 0 || color_map[color2] == 0 || cc_map[color2] != 0){
						return false;
					}
				}


			}


		}

		//check if color nodes are only connected to color nodes
		for (FilterNodes<ListDigraph>::NodeIt i(color_graph); i != INVALID; ++i) {

			for (ListDigraph::OutArcIt a(graph_pixel, i); a != INVALID; ++a) {

				ListDigraph::Node color = graph_pixel.target(a);

				if(meta_map[color] != 0 || color_map[color] == 0 || cc_map[color] != 0){
					return false;
				}

			}

		}




		return true;


	}


	/*
	 * sets the four constraints
	 */
	void setConstraints(FilterNodes<ListDigraph> &comp_graph, FilterNodes<ListDigraph> &meta_graph,FilterNodes<ListDigraph> &color_graph){



		/*
		 * CONSTRAINT 1
		 */
		int num_out=0;
		int num_in=0;
		int constraint_count = 0;
		for (FilterNodes<ListDigraph>::NodeIt i(color_graph); i != INVALID; ++i) {


//			if(points_map[i].t!=0) continue;

			IloExpr constr1(env);
			for (ListDigraph::OutArcIt a(graph_pixel, i); a != INVALID; ++a) {
				num_out++;
				constr1 += f_map[a];
			}

			//only add constraint, if there is actually a variable!! (example: last timestep, there is no flow going out!)
			if (num_out >0){
				model.add(constr1 == m_map[i]);
				constraint_count++;
			}

			num_out=0;
		}


		std::cerr << constraint_count << " equations of constraint 1" << std::endl;

		/*
		 * CONSTRAINT 2
		 */
		constraint_count=0;
		num_in=0;
		int var_constr =0;
		//go over all meta nodes (=pixels)
		for (FilterNodes<ListDigraph>::NodeIt m(meta_graph); m != INVALID; ++m) {
			IloExpr constr2(env);

			//get everything that goes into children of m!
			//for each child of m (which are color nodes)
			for(ListDigraph::OutArcIt a(graph_pixel,m); a != INVALID; ++a){

				ListDigraph::Node child = graph_pixel.target(a);

				//for every flow that goes into that child
				for(ListDigraph::InArcIt e(graph_pixel,child); e != INVALID; ++e){

					//only look at colors pointing to this child
					if(is_color_map[graph_pixel.source(e)] == true){
						constr2 += f_map[e];
						var_constr++;
					}

				}

			}

			//only add constraint, if there is actually a variable!! (example: last timestep, there is no flow going out!)
			if(var_constr>0 ){ //TODO is that needed still? && intensity_map[m]<1){
				model.add(constr2 == intensity_map[m]);
				constraint_count++;
			}

			var_constr=0;
		}

		std::cerr << constraint_count << " equations of constraint 2" << std::endl;
		/*
		 * CONSTRAINT 3
		 */
		//sum over all meta nodes
		constraint_count=0;
		num_out = 0;
		for (FilterNodes<ListDigraph>::NodeIt i(meta_graph); i != INVALID; ++i) {
			IloExpr constr3(env);



			//sum over their outgoing edges (=colors)
			for (ListDigraph::OutArcIt a(graph_pixel, i); a != INVALID; ++a) {
				num_out++;
				constr3 += m_map[graph_pixel.target(a)];
			}

			//only add constraint, if there is actually a variable!! (example: last timestep, there is no flow going out!)
			if (num_out >0 ){
				model.add(constr3 == intensity_map[i]);
				constraint_count++;
			}

			num_out=0;
		}

		std::cerr << constraint_count << " equations of constraint 3" << std::endl;


		/*
		 * CONSTRAINT 4
		 */
		constraint_count=0;
		num_in=0;



		for (FilterNodes<ListDigraph>::NodeIt j(color_graph); j != INVALID; ++j) {

			IloExpr constr4(env);
			for (ListDigraph::InArcIt a(graph_pixel, j); a != INVALID; ++a) {

				//only arcs that come from colors
				if(is_color_map[graph_pixel.source(a)]==true){
					num_in++;
					constr4 += f_map[a];
				}

			}

			//only add constraint, if there is actually a variable!! (example: last timestep, there is no flow going out!)
			if(num_in >0 ){
				model.add(constr4 == m_map[j]);
				constraint_count++;
			}

			num_in =0;

		}

		std::cerr << constraint_count << " equations of constraint 4" << std::endl;
	}

	/*
	 * sets the hypothesis depending on the global variable HYPO
	 */


	void setHypothesis(FilterNodes<ListDigraph> &comp_graph, FilterNodes<ListDigraph> &meta_graph,FilterNodes<ListDigraph> &color_graph){
		/*
		 * STARTING VALUES
		 */
		int n_in;
		int n_color=0;
		for (FilterNodes<ListDigraph>::NodeIt comp(comp_graph); comp != INVALID; ++comp) {

			//find component without incoming edges
			n_in=0;
			for (ListDigraph::InArcIt a(graph_pixel, comp); a != INVALID; ++a) {
				n_in++;
			}

			if(n_in ==0){

				n_color++;
				//get meta nodes
				for (ListDigraph::OutArcIt a(graph_pixel, comp); a != INVALID; ++a) {

					ListDigraph::Node meta = graph_pixel.target(a);
					//get its children
					for (ListDigraph::OutArcIt e(graph_pixel, meta); e != INVALID; ++e) {
						ListDigraph::Node child = graph_pixel.target(e);

						if(color_map[child]==n_color){
							//cerr<<"HYPO SET "<<
							model.add(m_map[child]==intensity_map[meta]);
						}
					}
				}
			}
		}

		/*
		 * HYPOTHESIS VALUES
		 */

		if(HYPO == 0){

			int n_out;
			n_color=0;
			for (FilterNodes<ListDigraph>::NodeIt comp(comp_graph); comp != INVALID; ++comp) {

				//find component without outgoing edges (except to meta nodes)
				n_out=0;
				for (ListDigraph::OutArcIt a(graph_pixel, comp); a != INVALID; ++a) {
					if(cc_map[graph_pixel.target(a)] == true)
						n_out++;
				}

				if(n_out ==0){
					n_color++;
					//get meta nodes
					for (ListDigraph::OutArcIt a(graph_pixel, comp); a != INVALID; ++a) {

						ListDigraph::Node meta = graph_pixel.target(a);
						//get its children
						for (ListDigraph::OutArcIt e(graph_pixel, meta); e != INVALID; ++e) {
							ListDigraph::Node child = graph_pixel.target(e);
							if(color_map[child]==n_color){

								model.add(m_map[child]==intensity_map[meta]);
							}
						}
					}
				}
			}

		} else if (HYPO == 1){

			int n_out;
			n_color=3;
			for (FilterNodes<ListDigraph>::NodeIt comp(comp_graph); comp != INVALID; ++comp) {

				//find component without outgoing edges (except to meta nodes)
				n_out=0;
				for (ListDigraph::OutArcIt a(graph_pixel, comp); a != INVALID; ++a) {
					if(cc_map[graph_pixel.target(a)] == true)
						n_out++;
				}

				if(n_out ==0){
					n_color--;
					//get meta nodes
					for (ListDigraph::OutArcIt a(graph_pixel, comp); a != INVALID; ++a) {

						ListDigraph::Node meta = graph_pixel.target(a);
						//get its children
						for (ListDigraph::OutArcIt e(graph_pixel, meta); e != INVALID; ++e) {
							ListDigraph::Node child = graph_pixel.target(e);
							if(color_map[child]==n_color ){//&& intensity_map[meta]<1){
								//std::cerr<<"hypo added: component nr " << graph_pixel.id(comp)<< ":  color : "<<n_color<< " with int. "<< intensity_map[meta]<<std::endl;

								model.add(m_map[child]==intensity_map[meta]);
							}
						}
					}
				}
			}

		}
	}

	/*
	 * gets the results and creates a graphical output depending on the global variable CREATE_IMAGE
	 */
	void getResultsAndSerialize(IloCplex &cplex, FilterNodes<ListDigraph> &comp_graph, FilterNodes<ListDigraph> &meta_graph,FilterNodes<ListDigraph> &color_graph){

		cplex.out() << "CPLEX RESULT: " << cplex.getCplexStatus() << " Solution value = " << cplex.getObjValue() << endl;



		/*
		 * TEST IF INTENSITY IN META NODE IS ACTUALLY INTENSITY OF THE COMPONENTS SUMMED UP!
		 */
		bool testbool = false;
		for (FilterNodes<ListDigraph>::NodeIt meta(meta_graph); meta != INVALID; ++meta) {
			float intensity = intensity_map[meta];
			float sum =0;
			for(ListDigraph::OutArcIt a(graph_pixel, meta); a != INVALID; ++a){

				ListDigraph::Node color_node = graph_pixel.target(a);
				sum += cplex.getValue(m_map[color_node]);

			}

			if(abs(sum-intensity)>1e-7){
				testbool =true;
			}

		}

		std::cerr<<"TEST: INTENSITY DIFFERS FROM SUM OF COMPONENTS INTENSITY: "<<testbool<<std::endl;

		/*
		 * test if sums of component's intensities equal pixel intensity.
		 */
		for (FilterNodes<ListDigraph>::NodeIt meta(meta_graph); meta != INVALID; ++meta) {

			float pixelint = intensity_map[meta];

			float colorint =0;

			for (ListDigraph::OutArcIt a(graph_pixel, meta); a != INVALID; ++a) {
				colorint += cplex.getValue(m_map[graph_pixel.target(a)]);
			}

			if(abs(colorint - pixelint)>1e-6){

				cerr << "INTENSITIES DIFFER!!!"<<endl;
				cerr << "colorint: " << colorint << ", pixelint: " <<pixelint<<endl;

				return;

			}


		}

//		/*
//		 * test if there's always mass 1 red and mass 1 green
//		 */
//		for (FilterNodes<ListDigraph>::NodeIt comp(comp_graph); comp != INVALID; ++comp) {
//
//			float total_int_red =0;
//			float total_int_green =0;
//
//			for (ListDigraph::OutArcIt a(graph_pixel, comp); a != INVALID; ++a) {
//
//				if(cc_map[graph_pixel.target(a)] == true) continue;
//
//				ListDigraph::Node meta = graph_pixel.target(a);
//
//				for (ListDigraph::OutArcIt e(graph_pixel, meta); e != INVALID; ++e) {
//
//					ListDigraph::Node color = graph_pixel.target(e);
//
//					if(color_map[color]==1){
//						total_int_green += cplex.getValue(m_map[color]);
//					}
//					if(color_map[color]==2){
//						total_int_red += cplex.getValue(m_map[color]);
//					}
//
//				}
//
//			}
//
//			cerr<<total_int_green<<" + "<<total_int_red<<endl;

//			if(abs(total_int_green-1)>1e-8 && total_int_green != 0){
//				cerr<<"green mass not equal to 1: "<<total_int_green<<endl;
//				return;
//			}
//			if(abs(total_int_red-1)>1e-8 && total_int_red != 0){
//				cerr<<"red mass not equal to 1: "<<total_int_red<<endl;
//			}
//
//
//		}

		testbool=false;
		if(CREATE_IMAGE){

			vigra::MultiArrayShape<4>::type shape(3,X,Y,T);
			vigra::MultiArray<4, float> volume(shape,0.0);


			vigra::MultiArray<4, float> volume_check(shape,0.0);


			//get variable values
			float max=0.0;
			float min=100000.0;
			for (FilterNodes<ListDigraph>::NodeIt comp(comp_graph); comp != INVALID; ++comp) {

				//find its meta nodes
				for (ListDigraph::OutArcIt a(graph_pixel, comp); a != INVALID; ++a) {

					if(cc_map[graph_pixel.target(a)]==true) continue;
					ListDigraph::Node meta = graph_pixel.target(a);

					int value = 0;


					int t,x,y;

					float value_old;

					//go over colors
					for (ListDigraph::OutArcIt a(graph_pixel, meta); a != INVALID; ++a) {
						ListDigraph::Node child = graph_pixel.target(a);
						t = points_map[meta].t;
						x = points_map[meta].x;
						y = points_map[meta].y;

						float var_val = cplex.getValue(m_map[child]);

						if(var_val>0 && color_map[child]==1 )
						{
							//volume(0,x,y,t) = 255*var_val*(comp_total_intensity_map[comp])/true_sizes_map[comp];
							volume(0,x,y,t) = 255*var_val;
							volume_check(0,x,y,t) = var_val;

						}
							if(var_val>0 && color_map[child]==2)
							{
							//volume(1,x,y,t) = 255*var_val*(comp_total_intensity_map[comp])/true_sizes_map[comp];
								volume(1,x,y,t) = 255*var_val;
							//	std::cerr<< comp_total_intensity_map[comp] << " , " ;
								volume_check(1,x,y,t) = var_val;
							}

						if(var_val<0) {
							testbool=true;
						}
						if(var_val >max) max=var_val;
						if(var_val<min) min=var_val;

					}

				}

			}


			std::cerr<<"FOURTH TEST: THERE ARE NEGATIVE INTENSITIES: "<<testbool<<std::endl;
			std::cerr<<"maximal intensity: "<<max<<std::endl;
			std::cerr<<"minimal intensity: "<<min<<std::endl;



			for(int k3=0;k3<volume_check.shape(3);k3++)
			{float sumg=0.0;
			 float sumr=0.0;

			 for(int k1=0;k1<volume_check.shape(1);k1++)
				 {
				 for(int k2=0;k2<volume_check.shape(2);k2++)
					 {sumg+=volume(1,k1,k2,k3);
						 sumr+=volume(0,k1,k2,k3);
					 }
				 }
				 std::cerr << "sum  green = "<< sumg<< " , ";
				 std::cerr << "  red "<< sumr<< ", time = " << k3 << std::endl;




			}


			for(int k=0;k<volume.shape(3);k++)
			{
				std::stringstream filename;
				filename << "img/res/result" << k;

				vigra::MultiArrayView<3, float,vigra::StridedArrayTag> image=volume.bindAt(3,k);
				vigra::MultiArray<3, float> image2=image;
				image2(0,0,0)=255;
				float s=0.0;
				 for(int k1=0;k1<image2.shape(1);k1++)
					 for(int k2=0;k2<image2.shape(2);k2++)
						 s+=image2(0,k1,k2);

				 std::cerr << " here red "<< s << std::endl;


				vigra::ImageExportInfo info((filename.str()+".png").c_str());
				vigra::exportImage(vigra::srcImageRange(vigra::makeRGBImageView(image2)),info);
			}
		}
	}


};


/*
 * starts the program
 */
int main(int argc, char *argv[]) {


	if(argc<3){
		cerr<<"Usage: call this function with:"<<endl;
		cerr<<"1) filename and relative path"<<endl;
		cerr<<"2) number of hypothesis"<<endl;
		cerr<<"3) average worm size (~85-100)"<<endl;
		cerr<<"The files are located in kgregor/workspace/wormscpp/testdata/encountersnew/<number of file>/encounter<number of file>"<<endl;
		cerr<<"where number is 1,3,5, or 7. A possible function call is:"<<endl;
		cerr<<"./test ../testdata/encountersnew/3/encounter3"<<endl;
		cerr<<"hypothesis";
		return 0;
	}

	std::string hypo=argv[argc-2];
	std::string avgsize=argv[argc-1];
	std::string file=argv[argc-3];

	  stringstream hy(hypo);
	  stringstream avg(avgsize);

	  int hypothesis;
	  int average;
	  hy >> hypothesis;
	  avg >> average;

	  AVG_SIZE=average;
	  HYPO=hypothesis;


	if(file=="ex"){
		cout<<"test on small example"<<endl;
		mode=1;
	}

	Solver s;
	s.execute(file);

	return 0;
}

