"""
This file gets a dictionary of type 'component -> larva -> positions' and creates an output movie
"""


import sys
import pickle
import numpy as np
from cythonfiles.connectivity import deserializeGraphFast2
import h5py
from tools import readH5,showCC, writeH5
import matplotlib.pyplot as plt
import vigra
from tools import logme
import cPickle 

import numpy as np
import colorsys

ALPHA = 0.5

class ColorResult(object):
    
    
    def __init__(self,data,outputfolder,component_dictionary):
        print "color result created!"
        self.data = data
        self.component_dictionary = component_dictionary
        self.outputfolder = outputfolder


    def _get_colors(self,num_colors,seed=41):
        #random colors between 0,1
        golden_ratio_conjugate = 0.618033988749895
        np.random.seed(seed)
        colors=[]
        hue=np.random.rand()*360
        for i in np.arange(0., 360., 360. / num_colors):
            hue += golden_ratio_conjugate
            lightness = (50 + 1 * 10)/100.
            saturation = (90 + 1 * 10)/100.
            
            colors.append(colorsys.hsv_to_rgb(hue, 0.99,0.99))
        return colors
    
    
    def remap(self,dict):
        
        tmp=[]
        for k,v in dict.items():
            for k2 in v.iterkeys():
                tmp.append(k2)
        
        uniquevals=np.unique(tmp)
        remapping={}
        for k,el in enumerate(uniquevals):
            remapping[el]=k
        
        return remapping

    def serialize(self):

        palette=np.vstack(self._get_colors(400))*255
        tmp=sys.path
        tmp=["/home/kgregor/workspace/larvae_counting"]+tmp
        
        print "read original data..."
        #D,Positions,sizes,times=deserializeGraphFast2(file_in_base+".graph_fast")
        #data=readH5(self.datafile, onlyhandle=True)
        #data = data[:,:,:,:,:].squeeze().astype(np.uint8)
        data = self.data
        print "done, shape:" , data.shape
        
        
        #print "read labeled output..."
        #dict=cPickle.load(open("/home/kgregor/workspace/worms_ilp/comp_dict.pkl",'rb'))
        #print "done"
        
        dict = self.component_dictionary
                
        remapping=self.remap(dict)
        
        print "concatenation"
        data.shape=data.shape+(1,)
        data=np.concatenate([data,data,data],axis=-1)
        print "concatenation done"
        
                
        for k,comp in enumerate(dict):
            
            logme("creating images %.2f"%(k/float(len(dict))))
            parts = dict[comp]
            for larva in parts:
                larva_pos = parts[larva]
                larva_pos = np.vstack(larva_pos)
                #for the case that larva have a high number
                larva=remapping[larva]
                
                if larva >= 400:
                    print "Larva id too high! larvaid=", larva
                    larva = 1
                
                data[larva_pos[:,0],larva_pos[:,1],larva_pos[:,2],0]= ALPHA * palette[larva][0] + (1-ALPHA)*data[larva_pos[:,0],larva_pos[:,1],larva_pos[:,2],0]
                data[larva_pos[:,0],larva_pos[:,1],larva_pos[:,2],1]= ALPHA * palette[larva][1] + (1-ALPHA)*data[larva_pos[:,0],larva_pos[:,1],larva_pos[:,2],1]
                data[larva_pos[:,0],larva_pos[:,1],larva_pos[:,2],2]= ALPHA * palette[larva][2] + (1-ALPHA)*data[larva_pos[:,0],larva_pos[:,1],larva_pos[:,2],2]
        print "done with creating"
        
        for k,img in enumerate(data):
            
            logme("write images %.2f"%(k/float(data.shape[0])))
            
            print "shapeimg", img.shape
            print "k", k
            print "ouputfolder", self.outputfolder
            print type(img)
            
            assert np.min(img) >= 0 and np.max(img) <= 255
            
            img = np.require(img, np.uint8, "C")
            
            vigra.impex.writeImage(img,self.outputfolder+"/%.4d.png"%k)
        
