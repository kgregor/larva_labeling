#Class that labels a given encounter
class labeler(object):
    
    
    def __init__(self):
        pass
    
    def setup(self,data,lvol,segmentation, info, cc_start, cc_end, times, positions, K):
        pass
    
    
    """
    function that takes an encounter and solves it.
    needs to return larvae_map, positions_map
    
    where:
    
    larvae_map: maps from a component to a list of larvae it contains
    
    positions_map: maps from a component to many dictionaries, where each dictionary
        stands for a larva the component contains and its positions. Like:
        
        CC 15000 -> Larva 4 -> list of positions
                -> Larva 9 -> list of positions
    
    """
    def label_sequence(self):
        pass
