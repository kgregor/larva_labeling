# -*- coding: utf-8 -*-
from tools import readH5,showCC, writeH5
import matplotlib.pyplot as plt
import h5py
import numpy as np


file_in_base = "/home/kgregor/workspace/worms_ilp/final_output/"
fileoutbase = "/home/kgregor/workspace/worms_ilp/final_output/merged/res"


fh1 = readH5(file_in_base+"trails1.h5","labeling/data",onlyhandle=True)
#lvol1 = np.copyfh1[:,:,:]
print "file1 read"
fh2 = readH5(file_in_base+"trails2.h5","labeling/data",onlyhandle=True)
#lvol2 = fh2[:,:,:]
print "file2 read"
#fh3 = readH5(file_in_base+"trails3.h5","labeling/data",onlyhandle=True)

T = 100

larva_map = {}
relabeled = {}

output = np.zeros((1000,1400,1400),np.uint16)

for t in range(0,T):
  
  lvol1 = fh1[t,:,:]
  lvol2 = fh2[t,:,:]
  
  #find overlaps -> parts need to be relabeled!
  seg1 = np.where(lvol1>0,1,0)
  seg2 = np.where(lvol2>0,1,0)
  
  minlarva = np.zeros((1400,1400),np.uint8)
  minlarva = np.where(lvol1<lvol2,lvol1,lvol2)
  
  #when there's no overlap, add up the two
  output[t,:,:] += np.where(seg1-seg2<0,lvol2,lvol1)
  
  #where they are overlapping, add the smaller larva of the two
  output[t,:,:] = np.where(seg1-seg2==0,minlarva,output[t,:,:])
  
  print "t=",t,"------------------------------"
  print "LVOL1 UNIQUE:", np.unique(lvol1)
  print "LVOL2 UNIQUE:", np.unique(lvol2)
  print "OUTPUT UNIQUE:", np.unique(output[t,:,:])
  
  plt.imshow(output[t,:,:],interpolation=None,cmap=None,vmin=0,vmax=20)
  plt.savefig(fileoutbase+"_"+str(t).zfill(4)+".png")