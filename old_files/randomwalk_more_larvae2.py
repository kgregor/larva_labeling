from tools import readH5,writeH5
from skimage.segmentation import random_walker#,random_walker_with_prior
import matplotlib.pyplot as plt
from matplotlib import colors
import pylab
import numpy as np
import time
import pickle
import vigra
import os
import matplotlib
import h5py
np.set_printoptions(threshold=np.nan)

#for testing
kk=1

#for numerical issues when working with the threshold vector
tol = 0.1

#for plotting: needs to be as high as the maximal amount of different worms (or higher,
#but then the plotting doesn't look as nice anymore)
N_MAX = 6

start_sizes = {}
all_labels = []	    
K = 3
fileoutbase = "randomwalker_encounters/cut_and_label/combined%d_"%K
lvol=None
segmentation=None
data = None
up = 0.5
testrun = 0
imgflag = 0

#threshold for when a new region enters the image to be considered a larva
larva_size_thresh = 130

region_min_x = 0
region_min_y = 0

def cc_to_larvae(output,lvol):

	cc_ll_connections = {}
	
	for t in range (0,output.shape[-1]):
		components = np.unique(lvol[...,t])[1:]  #we dont want the 0
		labelarr = np.zeros_like(output[...,t])
		labelarr = np.where(output[...,t]!=-1,output[...,t],0)
		for cc in components:

			#for the component, find the corresponding larvae and their positions
			ccarr = np.zeros_like(lvol[...,t])
			ccarr = np.where(lvol[...,t]==cc,1,ccarr)	
			cc_ll = ccarr * labelarr
			cc_ll_list = np.unique(cc_ll)[1:]
			print "ZEITPUNKT", t , "CC", cc , "HAT LARVEN", cc_ll_list

			#this finds the positions of the larvae belonging to the component
			#positions={}
			#for ll in cc_ll_list:
			#	positions[ll] = []
			#	pixels = np.where(labelarr==ll)
			#	for i in range(len(pixels[0])):
			#		positions[ll].append([t,pixels[0][i]+region_min_x,pixels[1][i]+region_min_y])


			cc_ll_connections[cc] = cc_ll_list

	return cc_ll_connections
	#pickle.dump(cc_ll_connections, open("cc_ll_connections.pkl","wb"))
				


#write the output to images
def saveimgs(output_labels):
	lmin = sorted(np.unique(output_labels))[1]
	for t in range (0,output_labels.shape[-1]):
		a=np.copy(np.where(output_labels[...,t]==-1,0,output_labels[...,t]))
		plt.imshow(a,interpolation=None,cmap=None,vmin=lmin-2,vmax=lmin+4)
		plt.savefig(fileoutbase+str(kk)+"_"+str(t).zfill(2)+".png")

#write the output to h5 file
def serialize(output):
	
	print fileoutbase + "output_test" + str(kk) + ".h5"

	fh4=h5py.File(fileoutbase + "output_test" + str(kk) + ".h5","a")
	g=fh4.require_group("volume")
	g.create_dataset(name="data",data=output)
	fh4.close()

#finds most common entry other than '-1' in an array
def find_most_common(arr):
	print "find most common of", np.unique(arr)
	countermap = {}
	for i in range(len(arr)):
		for j in range(len(arr[0])):
			item = arr[i][j]
			if item == 0:
				continue				
			if item in countermap:
				countermap[item]+=1
			else:
				countermap[item]=1
	most_common = sorted(countermap, key=countermap.get, reverse=True)
	print "FOUND", most_common
	
	if most_common[0] == -1:
		return most_common[1]
	else:
		return most_common[0]

#decides whether a threshold is still okay of if it has reached
#its maximum (in this case [0.5, 0.5, 0.5, ...] )
def allowed(th):
	if len(th)==1 and abs(th[0]-0.5) < tol:
		return 0

	maxi = [0.5] * (len(th)-1)
	diff = np.asarray(maxi) - np.asarray(th[0:len(th)-1])
	for k in diff:
		if k<-tol:
			return 0
	return 1


#increases a threshold vector
def increase(th,up):

	if len(th) == 1:
		th[0] += up

	if len(th) == 2:
		th[0] += up
		th[1] -= up
	else:
		k=len(th)-2
		while k>=0:
			if abs(th[k]-0.5) > tol:
				th[k] += up
				break
			else:
				th[k] = -0.5
				th[k-1] += up
				break
			k-=1
	
		summe = sum(th[0:len(th)-1])
		th[-1] = - summe

#computes the difference between the larvae's sizes
def compute_energy(labels_unique, start_sizes, current_sizes, Nlarvae):
	energy = 0
	for k in labels_unique:
		if k == -1:
			continue		
		energy += abs(current_sizes[k] - start_sizes[k])
	return energy

def get_biggest(seg):
    #Clean out small cc components from a binary segmentation
    lseg=vigra.analysis.labelImageWithBackground(seg.astype(np.float32)).view(np.ndarray)
    sizes=np.bincount(lseg.flatten().astype(np.int32))
    sizes=sizes[1:]
    
    ii=np.argmax(sizes)
    lseg=np.where(lseg==ii+1,1,0)
    
    return lseg



def handle_disappearing_and_appearing(labels,t):
	#delete a worm if necessary

	labels_unique = np.unique(labels)

	lvol_pos = np.where(lvol[...,t]>0,1,0)
	labels_pos = np.where(labels>0, 1, 0)
	remainder = lvol_pos - labels_pos
	remainder_pos = np.where(remainder>0, 1, 0)
	remainder_neg = np.where(remainder<0, 1, 0)
	remainder_pos_size = sum(sum(remainder_pos))
	remainder_neg_size = sum(sum(remainder_neg))
	print "REMAINDER_SIZE POS:",remainder_pos_size, "REMAINDER SIZE NEG:", remainder_neg_size
	

	#ncomp_before = len(np.unique(lvol[...,t-1]))-1	
	#ncomp = len(np.unique(lvol[...,t]))-1
	#print "LABELED VOL UNIQUE: " , np.unique(lvol[...,t]), "COMPONENTS:" , ncomp

	#REMARK: The try-except is necessary, because it could happen due to the threshold, that worms
	#are deleted.
	#a worm as appeared
	#find if worm disappeared
	if remainder_neg_size>larva_size_thresh:
		print "WORM GONE"
		#plt.imshow(remainder,interpolation=None,cmap=None,vmin=0,vmax=N_MAX)
		#plt.show()
		#find which worm has disappeared
		remainder = abs(remainder) * labels
		id_worm_gone = find_most_common(np.asarray(remainder))
		labels = np.where(labels==id_worm_gone,-1,labels)
		start_sizes[id_worm_gone] = 0
		print "KILLED WORM" , id_worm_gone

	#REMARK: The try-except is necessary, because it could happen due to the threshold, that worms
	#are deleted.
	#a worm has appeared
	if remainder_pos_size>larva_size_thresh:
		print "START SIZES" , start_sizes, "---------------------------------"
		print labels_unique
		new_worm_label = max(labels_unique)+1
		try:
			start_sizes[new_worm_label] = remainder_pos_size
			print "START SIZES SET!",start_sizes,"-------------------------------------"
		except:
			print "couldnt set start sizes for", new_worm_label
			sys
		print "NEW WORM" , new_worm_label
		labelsnew = get_biggest(np.where(remainder==1,1,0))
		labels[labelsnew==1]=new_worm_label
		#plt.imshow(labels,interpolation=None,cmap=None,vmin=0,vmax=N_MAX)
		#plt.show()
	
	return labels


def create_labels_from_rwresult(res,labels_unique,th,t):
	#assign new labels
	current = np.zeros_like(segmentation[...,t])
	numrows=len(current)
	numcols=len(current[0])
	for i in range(numrows):
		for j in range(numcols):
			#at each place, assign current the maximum!
			maxprob = -10
			maxind = -1
			larva = -2
			for k in labels_unique:
				larva += 1
				if k == -1:
					continue
				#even if the larvae are labeled 2 and 4, they must be addressed as 0 and 1
				if res[larva,i,j,1] + th[larva] > maxprob:
					maxprob = res[larva,i,j,1]
					maxind = k-1
			current[i,j] = maxind + 1
	return np.where(segmentation[...,t]==0,0,current)

#main function
def label_sequence():    

    print "RANDOM WALK INPUT SIZE: " , segmentation.shape,lvol.shape
    try:
	
	labels0=lvol[...,0]
	labels0=np.where(labels0==0,-1,labels0)
	labels=labels0
	labels_start = np.unique(labels)
	print "LABELS START:" , labels_start

	output_labels = np.zeros_like(lvol)
	
	#save sizes of components from first time step
	for l in labels_start:
		if l < 1:
			continue   
		start_sizes[l] = sum(sum(k==l for k in labels))

	print "START"

	output_labels[...,0] = labels

	print "TIME STEPS", lvol.shape[-1]

	for t in range(1,lvol.shape[-1]):

	    	#take care of worms that have appeared or disappeared
		labels = handle_disappearing_and_appearing(labels,t)


		labels_unique = np.unique(labels)
		lvol_unique = np.unique(lvol[...,t])

		Nlarvae = len(labels_unique) - 1
		th = [-0.5]*Nlarvae
		th[len(th)-1] = 0.5*(Nlarvae-1)

		print "#########################################################"
		print "          TIME STEP ", t, "Larvae:" , labels_unique, "lvol:", lvol_unique
		print "#########################################################"

		#set labels of next time step to 0
		nextlabels=np.where(segmentation[...,t]==0,-1,0).astype(np.int32)
		labels3d=np.dstack([labels,nextlabels])
		img3d=data[...,t-1:t+1].astype(np.float32) 
		    
		min_energy = 10000

		print "thresholds go"
		#go through all the thresholds
		while allowed(th):
			
			#get the result of the random walker
			res=random_walker(img3d, labels3d,500,mode="bf",return_full_prob=True)

			#use the result and the threshold to assign labels for the next time step
			current = create_labels_from_rwresult(res,labels_unique,th,t)
			curr_labels = np.unique(current)

			print "CURR LABELS:" , curr_labels

			#get the sizes of the worms from the new labeling
			current_sizes = {}  
			for l in start_sizes:   
				current_sizes[l] = sum(sum(k==l for k in current))             
			
			print "USE THRESHOLD" , th
			#plt.imshow(current,interpolation=None,cmap=None,vmin=0,vmax=N_MAX)
			#plt.show()

			#calculate the difference between the worms' size and their size from the beginning
			energy = compute_energy(labels_unique, start_sizes, current_sizes, Nlarvae)


			print "ENERGY", energy
			if energy >= min_energy:
			    increase(th,up)
			    continue

			#print "FOUND BETTER THRESHOLD!", th, "Current sizes:", current_sizes, "start sizes", start_sizes
			min_energy = energy
			
			labelsnew = {}
	
			try:		
				for l in curr_labels:
					if l == 0:
						continue
					labelsnew[l] = np.where(current==l,1,0)
					labelsnew[l] = get_biggest(labelsnew[l])
			except:
				print "SCHEISSE"
				print current
	
		
			for k in curr_labels:
				if k == 0:
						continue
				labelsnew[k] = get_biggest(np.where(current==k,1,0))
	

			#put the mask for the two larvae together again
			current2 = np.zeros_like(segmentation[...,t])
			for k in curr_labels:
				if k == 0:
						continue
				current2 += get_biggest(np.where(current==k,1,0))*k
			labels=np.where(segmentation[...,t]==0,-1,current2)
	
			#remove little fragments
			for k in curr_labels:
				if k == 0:
					continue
				labelsnew[k] = get_biggest(np.where(labels==k,1,0))
		       
	
			#reset the labels array which will be worked on in the next iteration
			labels=-np.ones_like(labels)
			for k in curr_labels:
				if k == 0:
					continue
				labels[labelsnew[k]==1]=k	


			#update threshold
			increase(th,up)
	
		output_labels[...,t] = labels

		
	
	

	if imgflag == 1:
		print "DRAW IMAGES!!!"
		saveimgs(output_labels)
    	if testrun==0:
		cc_ll_conn = cc_to_larvae(output_labels,lvol)
		print "serialize to h5"
		#serialize(output_labels)

	return output_labels, cc_ll_conn

	print "done"
    except Exception,e:
	print e
	pass

    


def test_run(filename):
	global testrun
	testrun = 1
	global data
	data=readH5(filename).squeeze().swapaxes(0,-1).astype(np.float32)/255
	global segmentation	
	segmentation=readH5(filename,"volume/segmentation").squeeze().swapaxes(0,-1).astype(np.int32)
	global lvol	
	lvol=readH5(filename,"volume/labeledvol").squeeze().swapaxes(0,-1).astype(np.int32)
	segmentation=segmentation*(lvol>0)

	out = label_sequence()

	return out
	
#info: min_t, max_t, min_x, max_x, min_y, max_y

def start(_data,_lvol,_segmentation, _kk, info, _K):
	
	global region_min_x
	region_min_x = info[2]
	global region_min_y
	region_min_y = info[4]

	global fileoutbase
	fileoutbase = "randomwalker_encounters/cut_and_label/combined%d_"%K

	global kk
	kk = _kk
	global lvol
	lvol = _lvol
	global segmentation
	segmentation = _segmentation
	segmentation=segmentation*(lvol>0)
	global data
	data = _data

	return label_sequence()  



if __name__ == "__main__":


	filename="randomwalker_encounters/cut_and_label/encounter_with_3/test%d.h5"%kk
	#filename="testdata/encounters/%d/encounter%d.h5"%(kk,kk)
	#filename="/home/kgregor/Desktop/new_cutouts/3/test1.h5"
	#filename="randomwalker_encounters/test%d.h5"%kk    


	f,e=os.path.split(filename)
	n,e=os.path.splitext(e)
		
		
	resfolder="14_12_2012/noniterativenoprior/"

	print resfolder+e
	if not os.path.exists(resfolder+n):
	    os.mkdir(resfolder+n)

	resfolder=resfolder+n+"/"

	global data
	data=readH5(filename).squeeze().swapaxes(0,-1).astype(np.float32)/255
	global segmentation	
	segmentation=readH5(filename,"volume/segmentation").squeeze().swapaxes(0,-1).astype(np.int32)
	global lvol	
	lvol=readH5(filename,"volume/labeledvol").squeeze().swapaxes(0,-1).astype(np.int32)
	segmentation=segmentation*(lvol>0)

	label_sequence()

	  

