# -*- coding: utf-8 -*-
from tools import readH5,showCC, writeH5
import numpy as np
import pickle
import time
import cythonfiles
from cythonfiles.connectivity import getPositionsOfComponent, serializeGraphFast2, deserializeGraphFast2, deserializeGraphFast
import sys
import pylab
import h5py
import math
import randomwalk_more_larvae2
import matplotlib.pyplot as plt

timesteppp = []

counter = 1
#number of examples to be cut out
n_max_encounters = 1


larva_map = {}

K = 2

file_in_base = "/home/kgregor/Desktop/2012_10_18_1712/2012_10_18_1712"
#file_in_base = "/home/kgregor/data/1000"
#file_in_base = "/home/kgregor/Desktop/1000frames_newseg/encounters/test"
file_out_base = "/home/kgregor/workspace/worms_ilp/randomwalker_encounters/all_2/"


print "create final seg array"
print "start"


def do_labeling(info, D, counter,final_segmentation):
                
    print "ACHTUNG SERIALIZE" , info

    fh = readH5(file_in_base+".h5","volume/data",onlyhandle=True)
    data=fh[info[0]:info[1]+1,:,info[2]:info[3]+1,info[4]:info[5]+1,:]
    
    fh2=readH5(file_in_base+".h5","volume/labeledvol",onlyhandle=True)
    lvol=fh2[info[0]:info[1]+1,:,info[2]:info[3]+1,info[4]:info[5]+1,:]
    
    fh3=readH5(file_in_base+".h5","volume/segmentation",onlyhandle=True)
    seg=fh3[info[0]:info[1]+1,:,info[2]:info[3]+1,info[4]:info[5]+1,:]
    
    lvol2=np.zeros_like(lvol)
    for oldkey, newkey in comp_map.items():
        lvol2 += np.where(lvol == oldkey, oldkey, 0)
    
    
    result,cc_ll_conn = randomwalk_more_larvae2.start(data.squeeze().swapaxes(0,-1).astype(np.float32)/255,lvol2.squeeze().swapaxes(0,-1).astype(np.int32),seg.squeeze().swapaxes(0,-1).astype(np.int32), counter, info, K)

    print "plug in", info
    rr = result.swapaxes(0,-1).astype(np.float32)

    print "result shape", rr.shape

    print "result put in"
    tt=0
    for t in range(info[0],info[1]+1):
        rrr = rr[tt,...]
        for cc in np.unique(rrr):
            if cc == -1:
                continue
            if cc in larva_map:
                print "translate component",cc,"to larva",larva_map[int(cc)]
                rrr = np.where(rrr==int(cc),larva_map[int(cc)],rrr)


        final_segmentation[t,info[2]:info[3]+1,info[4]:info[5]+1] += rrr
        print "ok"
        tt+=1

    #translate cc_ll_conn
    #component --> [larva1, larva2, ...]
    cc_ll_conn_new = {}
    for key in cc_ll_conn:
      cc_ll_conn_new[key] = []
      for v in cc_ll_conn[key]:
          cc_ll_conn_new[key].append(larva_map[int(v)])


def get_info_to_serialize(total_series):
    x_max =0
    y_max =0
    x_min =1000000
    y_min =1000000
    
    for i in range(len(total_series)):
        pixel = positions[total_series[i]]
        if pixel[1] > x_max:
            x_max = pixel[1]
        if pixel[1] < x_min:
            x_min = pixel[1]
        if pixel[2] > y_max:
            y_max = pixel[2]
        if pixel[2] < y_min:
            y_min = pixel[2]
    
    print "MIN_X " , x_min , ", MAX_X " , x_max , ", MIN_Y " , y_min , " MAX_Y " , y_max

    #now get time steps!
    t_max=positions[total_series[-1]][0]
    t_min=positions[total_series[0]][0]
    print "MIN TIME STEP " , t_min
    print "MAX TIME STEP " , t_max
    left = math.floor(max(x_min-10, 0))
    right = math.ceil(min(x_max+10, 1600))
    bottom = math.floor(max(y_min-10, 0))
    top = math.ceil(min(y_max+10, 1590))
        
    return [int(t_min), int(t_max), int(left), int(right), int(bottom), int(top)]


LIMIT = 100000

if 0:
    
    connections,Positions,sizes,times=deserializeGraphFast2(file_in_base+".graph_fast")
    
    connections_reduced={}
    positions_reduced={}
    sizes_reduced={}
    times_reduced={}
    
    print "here"
 
    # SAVE PART OF THE DATA
    for cc,other in connections.items():
        connections_reduced[cc]=other
        sizes_reduced[cc]=sizes[cc]
        times_reduced[cc]=times[cc]
        #if cc>=LIMIT:
        #    break

        
    # GET POSITIONS
    for cc,other in connections.items():
        positions_reduced[cc] = Positions[cc]
        
    print "read true sizes"
    true_sizes=readH5(file_in_base + ".res","graph_fast/entire/true_sizes")
    print "true sizes successfully read"
    
    print "write to pickle file"
    pickle.dump(connections_reduced, open("con.pkl","wb"))
    pickle.dump(positions_reduced, open("pos.pkl","wb"))
    pickle.dump(times_reduced, open("tim.pkl","wb"))
    pickle.dump(sizes_reduced, open("siz.pkl","wb"))
    pickle.dump(true_sizes, open("tru.pkl","wb"))
    
else:
    connections=pickle.load(open("con.pkl","r"))
    positions=pickle.load(open("pos.pkl","r"))
    times=pickle.load(open("tim.pkl","r"))
    sizes=pickle.load(open("siz.pkl","r"))
    true_sizes = pickle.load(open("tru.pkl","r"))
    
    
    
######################################################################
#                       CREATE BACKWARD ARCS                         #
######################################################################


    #dictionary whether a node was already checked completely
    done = {}
    looked_at = {}
    is_child={}

    #create list of lists: each component gets a list with its "backneighbors"
    back_connections = [[] for i in range(LIMIT+400)] #make up for components in T+1
    for cc,neighbors in connections.items():
        #print "COMPONENT " , cc
        back_connections[cc]=[]
    
    print "create backarcs"
    
    #fill in backneighbors
    for cc,neighbors in connections.items():
        for neigh_cc in neighbors:
            back_connections[neigh_cc].append(cc)
    
    print "backarcs created"
    
    #test correct construction
    #for i in range(1,100):
    #    print i, "connected to " , connections[i]
    #    for neigh in connections[i]:
    #        print neigh , " connected to " , back_connections[neigh]
    #    print "     "
        
    ######################################################################
    #                       FIND RELEVANT SERIES                         #
    ######################################################################
    
    
    #A node is called relevant, if it consists of K components and it doesnt
    #connect with more components
    def is_relevant(cc, series):
        
        if cc > LIMIT:
            return 0
        
        #if K-component is connected to another relevant K-component
        if true_sizes[cc] == K and len(connections[cc]) == 1 :
            if is_relevant(connections[cc][0], series):
                series.append(cc)
                return 1
        #it should not merge with other components in the next time step
        if true_sizes[cc] == K and len(connections[cc]) > 1:
            sum_neigh = 0
            for neigh in connections[cc]:
                sum_neigh += true_sizes[neigh]
                #if true_sizes[connections[cc][0]]==1 and true_sizes[connections[cc][1]]==1 :
            if sum_neigh <= K:
                series.append(cc)
                return 1
        else:
            return 0
    
    #a node is considered a start node, if it has size K, comes from components with smaller size
    def is_start_node(cc):
        if true_sizes[cc]!=K:
            return 0
        
        if len(back_connections[cc])==1:
            return 0
        
        sum_back_neigh = 0
        for back_neigh in back_connections[cc]:
            sum_back_neigh += true_sizes[back_neigh]

        if sum_back_neigh > K:
            return 0
        
        return 1

    def find_heads(cc, series, depth):
        
        #we don't want to find clusters containing components bigger than our parameter K
        if depth==0 or true_sizes[cc] > K:
            return 0
        
        
        #if we find an isolated larva, stop there, and store information that it is a child
        #because then we don't want to print its neighbors later on!
        if true_sizes[cc] == 1:
            if not cc in done:
                series.append(cc)
                done[cc]=1
            return 1
        
            
    def find_connections(cc, series, depth, from_top):
        
        
        #we don't want to find clusters containing components bigger than our parameter K
        if depth==0 or true_sizes[cc] > K:
            return 0
        
        looked_at[cc] = 1
        series.append(cc)
        
        #if we find an isolated larva, stop there, and store information that it is a child
        #because then we don't want to print its neighbors later on!
        if true_sizes[cc] == 1:
            if not cc in looked_at:
                series.append(cc)
                done[cc]=1
            if from_top ==1 :
                is_child[cc] = 1
            return 1
        
        #check for children
        if len(connections[cc])==0:
            return 0
        for child in connections[cc]:
            if child not in looked_at:
                if not find_connections(child, series, depth-1, 1):
                    return 0
        
        #print "PARENTS: " , back_connections[cc]
        if len(back_connections[cc])==0:
            return 0
        for parent in back_connections[cc]:
            if parent not in looked_at:
                if not find_connections(parent, series, depth-1, 0):
                    return 0
        
        if not cc in done:
	    done[cc] = 1
    
        return 1


    def is_start_of_single_trail(cc):
      if true_sizes[cc] != 1:
          return 0
      if len(back_connections[cc])>0:
          if true_sizes[back_connections[cc][0]]==1:
              return 0
      if len(connections[cc]) > 1:
          return 0
      return 1


    def get_single_trail(cc, series):
      #print "cc in single trail:", cc
      
      if true_sizes[cc] != 1:
          return

      series.append(cc)

      if len(connections[cc]) > 1 or len(connections[cc]) < 1 or connections[cc][0] == -1:
          return

      get_single_trail(connections[cc][0],series)
      
    #########################################################################
    
    #########################################################################
    



    final_segmentation=np.zeros(shape=(1400,1400,1000),dtype=np.int32)


    
    if 1:

        
        #fs = readH5("/home/kgregor/workspace/worms_ilp/final_output/single_trails.h5","labeling/data",onlyhandle=True)
        

        print "final seg read"

        global larva_map
        larva_map = pickle.load(open("larva_component_map.pkl","r"))

        #extract relevant sequences
        for cc,neighbors in connections.items():

            looked_at = {}
            tails= []
            heads= []
            series = []
            D = {} #connections
            P = {} #positions
            T = [] #times
            S = [] #sizes
            comp_map = {}
            done = {}
            is_child_new = {}
            
            if counter>n_max_encounters : 
              print "FOUND ENOUGH"
              break
            
            if is_start_node(cc):

              print 'found start node' , cc, "back_conn:", back_connections[cc]
            if find_connections(cc,series, 100, 0):
            
                series.sort(cmp=None, key=None, reverse=False)
            
                print "TOTAL SERIES: " , series
                
            
            else:
                
                print "ERROR"
                continue
            
            
            #get infos of all components in this series!
            S.append(-1) #such that the zero-spot is filled
            T.append(-1)
            
            #map components to 1...n
            comp = 1
            for cc in series:
                comp_map[cc] = comp
                comp +=1
                
            if cc in is_child:
                is_child_new[comp_map[cc]] = 1
              
            
            
            info = get_info_to_serialize(series)
            
            left = info[2]
            bottom = info[4]
            t_min = info[0]
            t_max = info[1]
            
              
            
            do_labeling(info, D, counter,final_segmentation)

            timesteppp.append(info[0])


            print "Got the information, put it in the original file!"
            
            counter = counter + 1

    print "write to H5 file"
    fh4.close()
    print timesteppp
    print "DONE"
    
