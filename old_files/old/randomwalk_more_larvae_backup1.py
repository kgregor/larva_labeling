from tools import readH5,writeH5
from skimage.segmentation import random_walker#,random_walker_with_prior
import matplotlib.pyplot as plt
from matplotlib import colors
import pylab
import numpy as np
import time
import vigra
import os
import matplotlib

np.set_printoptions(threshold=np.nan)
cmap = matplotlib.colors.ListedColormap ( np.concatenate([np.zeros((1,3)),np.random.rand ( 4,3)],axis=0))

tol = 0.001
N_MAX = 3

for kk in range(1,2):    
    try:
        #filename="testdata/encounters/%d/encounter%d.h5"%(kk,kk)
        filename="randomwalker_encounters/test%d.h5"%kk
        
        f,e=os.path.split(filename)
        n,e=os.path.splitext(e)
        
        
        
        resfolder="14_12_2012/noniterativenoprior/"
        
        print resfolder+e
        if not os.path.exists(resfolder+n):
            os.mkdir(resfolder+n)
        
        resfolder=resfolder+n+"/"
        
        data=readH5(filename).squeeze().swapaxes(0,-1).astype(np.float32)/255
        segmentation=readH5(filename,"volume/segmentation").squeeze().swapaxes(0,-1).astype(np.int32)
        lvol=readH5(filename,"volume/labeledvol").squeeze().swapaxes(0,-1).astype(np.int32)
        segmentation=segmentation*(lvol>0)
        
        print segmentation.shape,lvol.shape
        
        labels0=lvol[...,0]
        labels0=np.where(labels0==0,-1,labels0)
        labels=labels0


	def allowed(th):
		return not (abs(th[0]-0.5) < tol and abs(th[1]-0.5) < tol)

	#TODO: Only very raw
	def increase(th,up):
	    if abs(th[0]-0.5) > tol:
		if abs(th[1]-0.5) > tol:
			th[1] = th[1] + up
		else:
			th[0] = th[0] + up
			th[1] = -0.5
	    else:
		if abs(th[1]-0.5) > tol:
			th[1] = th[1]+up
	
	    th[2] = - th[0] - th[1]
        
        def get_biggest(seg):
            #Clean out small cc components from a binary segmentation
            lseg=vigra.analysis.labelImageWithBackground(seg.astype(np.float32)).view(np.ndarray)
            #print lseg.shape,"inside"
            sizes=np.bincount(lseg.flatten().astype(np.int32))
            #print "sizes", sizes.shape
            sizes=sizes[1:]
            
            ii=np.argmax(sizes)
            lseg=np.where(lseg==ii+1,1,0)
            
            return lseg
        
        
        #save sizes of components from first time step
	start_sizes = [0 for k in range(N_MAX+1)]
	for l in range(1,N_MAX+1):   
		start_sizes[l] = sum(sum(k==l for k in labels))

        for t in range(1,lvol.shape[-1]):
          


	    Nlarvae = len(np.unique(labels)) - 1
            print "#########################################################"
            print "          TIME STEP ", t, "Larvae:" , Nlarvae
            print "#########################################################"
          

            #set labels of next time step to 0
            nextlabels=np.where(segmentation[...,t]==0,-1,0).astype(np.int32)
            labels3d=np.dstack([labels,nextlabels])
            img3d=data[...,t-1:t+1].astype(np.float32) 
from tools import readH5,writeH5
from skimage.segmentation import random_walker#,random_walker_with_prior
import matplotlib.pyplot as plt
from matplotlib import colors
import pylab
import numpy as np
import time
import vigra
import os
import matplotlib

np.set_printoptions(threshold=np.nan)

tol = 0.1
N_MAX = 3
#cmap = matplotlib.colors.ListedColormap ( np.concatenate([np.zeros((1,3)),np.random.rand ( 4,3)],axis=0))

for kk in range(1,5):    
    try:
        #filename="testdata/encounters/%d/encounter%d.h5"%(kk,kk)
        #filename="/home/kgregor/Desktop/new_cutouts/3/test1.h5"
	#filename="randomwalker_encounters/test%d.h5"%kk        
	filename="randomwalker_encounters/all_3/test%d.h5"%kk


        f,e=os.path.split(filename)
        n,e=os.path.splitext(e)
        
        
        
        resfolder="14_12_2012/noniterativenoprior/"
        
        print resfolder+e
        if not os.path.exists(resfolder+n):
            os.mkdir(resfolder+n)
        
        resfolder=resfolder+n+"/"
        
        data=readH5(filename).squeeze().swapaxes(0,-1).astype(np.float32)/255
        segmentation=readH5(filename,"volume/segmentation").squeeze().swapaxes(0,-1).astype(np.int32)
        lvol=readH5(filename,"volume/labeledvol").squeeze().swapaxes(0,-1).astype(np.int32)
        segmentation=segmentation*(lvol>0)
        
        print segmentation.shape,lvol.shape
        
        labels0=lvol[...,0]
        labels0=np.where(labels0==0,-1,labels0)
        labels=labels0

	print "LABELS START:" , np.unique(labels)

	def find_most_common(arr):
		print "find most common of", np.unique(arr)
		countermap = {}
		for i in range(len(arr)):
			for j in range(len(arr[0])):
				item = arr[i][j]
				if item == 0:
					continue				
				if item in countermap:
					countermap[item]+=1
				else:
					countermap[item]=1
		most_common = sorted(countermap, key=countermap.get, reverse=True)
		print "FOUND", most_common
		return most_common[0]


	def allowed(th):
		maxi = [0.5] * (N_MAX-1)
		diff = np.asarray(maxi) - np.asarray(th[0:len(th)-1])
		for k in diff:
			if k<-tol:
				return 0
		return 1


	#TODO: Only very raw
	def increase(th,up):
		k=N_MAX-2
		while k>=0:
			if abs(th[k]-0.5) > tol:
				th[k] += up
				break
			else:
				th[k] = -0.5
				th[k-1] += up
				break
			k-=1
		
		summe = sum(th[0:len(th)-1])
		th[N_MAX-1] = - summe


	def compute_energy(start_sizes, current_sizes, Nlarvae):
		energy = 0
		for k in range(1,Nlarvae+1):		
			energy += abs(current_sizes[k] - start_sizes[k])
		return energy
        
        def get_biggest(seg):
            #Clean out small cc components from a binary segmentation
            lseg=vigra.analysis.labelImageWithBackground(seg.astype(np.float32)).view(np.ndarray)
            sizes=np.bincount(lseg.flatten().astype(np.int32))
            sizes=sizes[1:]
            
            ii=np.argmax(sizes)
            lseg=np.where(lseg==ii+1,1,0)
            
            return lseg
	
	def handle_disappearing_and_appearing(labels):
		#delete a worm if necessary

		labels_unique = np.unique(labels)

		lvol_pos = np.where(lvol[...,t]>0,1,0)
		labels_pos = np.where(labels>0, 1, 0)
		remainder = lvol_pos - labels_pos
		remainder_size = sum(sum(remainder))
		print "REMAINDER_SIZE",remainder_size
		
		#REMARK: The try-except is necessary, because it could happen due to the threshold, that worms
		#are deleted.
		#a worm as appeared
		#find if worm disappeared
		if remainder_size<-70:
			print "WORM GONE"
			#find which worm has disappeared
			remainder = abs(remainder) * labels
			id_worm_gone = find_most_common(np.asarray(remainder))
			labels = np.where(labels==id_worm_gone,-1,labels)
			print "KILLED WORM" , id_worm_gone

		#REMARK: The try-except is necessary, because it could happen due to the threshold, that worms
		#are deleted.
		#a worm has appeared
		if remainder_size>70:
			print labels_unique
			new_worm_label = max(labels_unique)+1
			try:
				start_sizes[new_worm_label] = remainder_size
			except:
				pass
			print "NEW WORM" , new_worm_label
			labelsnew = get_biggest(np.where(remainder==1,1,0))
			labels[labelsnew==1]=new_worm_label
		
		return labels


	def create_labels_from_rwresult(res,labels_unique,th):
		#assign new labels
		current = np.zeros_like(segmentation[...,t])
		numrows=len(current)
		numcols=len(current[0])
		for i in range(numrows):
			for j in range(numcols):
				#at each place, assign current the maximum!
				maxprob = -10
				maxind = -1
				larva = -2
				for k in labels_unique:
					larva += 1
					if k == -1:
						continue
					#even if the larvae are labeled 2 and 4, they must be addressed as 0 and 1
					if res[larva,i,j,1] + th[k-1] > maxprob:
						maxprob = res[larva,i,j,1]
						maxind = k-1
				current[i,j] = maxind + 1


	        return np.where(segmentation[...,t]==0,0,current)

        
        
        #save sizes of components from first time step
	start_sizes = [0 for k in range(N_MAX+1)]
	for l in range(1,N_MAX+1):   
		start_sizes[l] = sum(sum(k==l for k in labels))

	print "START"

	a=np.copy(np.where(labels==-1,0,labels))
	plt.imshow(a,interpolation=None,cmap=None,vmin=0,vmax=N_MAX)
	#plt.show()
	plt.savefig("randomwalker_encounters/all_3/rw_result/"+str(kk)+"_"+str(0).zfill(2)+".png")


        for t in range(1,lvol.shape[-1]):


	    	#take care of worms that have appeared or disappeared
		labels = handle_disappearing_and_appearing(labels) 

		         	
		Nlarvae = len(np.unique(labels)) - 1
		labels_unique = np.unique(labels)
		print "#########################################################"
		print "          TIME STEP ", t, "Larvae:" , labels_unique
		print "#########################################################"

		#set labels of next time step to 0
		nextlabels=np.where(segmentation[...,t]==0,-1,0).astype(np.int32)
		labels3d=np.dstack([labels,nextlabels])
		img3d=data[...,t-1:t+1].astype(np.float32) 
		    
		min_energy = 10000

		#threshold vector
		th = [-0.5]*N_MAX
		last = 0.5*(N_MAX-1)
		th[len(th)-1] = last
		up = 0.5
		    

		#go through all the thresholds
		while allowed(th):
			print "THRESHOLD USED:", th
		        
			#get the result of the random walker
			res=random_walker(img3d, labels3d,500,mode="bf",return_full_prob=True)

			print "1"

			#use the result and the threshold to assign labels for the next time step
			current = create_labels_from_rwresult(res,labels_unique,th)

			print "2"

		        #get the sizes of the worms from the new labeling
			current_sizes = [0 for k in range(N_MAX+1)]   
			for l in range(1,N_MAX+1):   
				current_sizes[l] = sum(sum(k==l for k in current))             
		        
			print "3"

		        #calculate the difference between the worms' size and their size from the beginning
			energy = compute_energy(start_sizes, current_sizes, Nlarvae)
		        if energy >= min_energy:
			    increase(th,up)
		            continue
		        
		        print "FOUND BETTER THRESHOLD!"
		        min_energy = energy
		        
			curr_labels = np.unique(current)
			labelsnew = {}
		
			try:		
				for l in curr_labels:
					if l == 0:
						continue
					labelsnew[l] = np.where(current==l,1,0)
					labelsnew[l] = get_biggest(labelsnew[l])
			except:
				print "SCHEISSE"
				print current
		
			
			
			for k in curr_labels:
				if k == 0:
						continue
				labelsnew[k] = get_biggest(np.where(current==k,1,0))
		

		        #put the mask for the two larvae together again
			current2 = np.zeros_like(segmentation[...,t])
			for k in curr_labels:
				if k == 0:
						continue
				current2 += get_biggest(np.where(current==k,1,0))*k
		        labels=np.where(segmentation[...,t]==0,-1,current2)
		
			#remove little fragments
			for k in curr_labels:
				if k == 0:
					continue
				labelsnew[k] = get_biggest(np.where(labels==k,1,0))
		       
		
			#reset the labels array which will be worked on in the next iteration
			labels=-np.ones_like(labels)
			for k in curr_labels:
				if k == 0:
					continue
				labels[labelsnew[k]==1]=k	


			#update threshold
			increase(th,up)
		

		a=np.copy(np.where(labels==-1,0,labels))
		plt.imshow(a,interpolation=None,cmap=None,vmin=0,vmax=N_MAX)
		#plt.show()
		plt.savefig("randomwalker_encounters/all_3/rw_result/"+str(kk)+"_"+str(t).zfill(2)+".png")

	a=np.copy(np.where(labels==-1,0,labels))
    
    
        plt.imshow(a,interpolation=None,cmap=None,vmin=0,vmax=N_MAX)
        #plt.show()
	plt.savefig("randomwalker_encounters/all_3/rw_result/"+str(kk)+"_"+str(t).zfill(2)+".png")

        print "done"
    except Exception,e:
        print e
        pass
    
    
    
    
#    x,y=np.where(labels==1)
#    ii=np.random.randint(0,x.shape[0],x.shape[0]-x.shape[0]/100)
#    labels[x[ii],y[ii]]=0
#    
#    x,y=np.where(labels==2)
#    ii=np.random.randint(0,x.shape[0],x.shape[0]-x.shape[0]/100)
#    labels[x[ii],y[ii]]=0
    
    
    
    
    
    #pylab.imshow(labels)
    #pylab.show()
	    a=np.copy(np.where(labels==-1,0,labels))
	    
	    
            #pylab.imshow(labels3d[:,:,0],interpolation=None,cmap=cmap)
	    print "here -> ",np.unique(a)
	    plt.imshow(a,interpolation=None,cmap=cmap,vmin=0,vmax=3)
            plt.show()

	    #threshold vector
	    th = [-0.5,-0.5,-0.5]
	    up = 1
            
            #go through all the thresholds
            while allowed(th):
                
                res=random_walker(img3d, labels3d,500,mode="bf",return_full_prob=True)

		#print "Threshold", th

		#assign new labels
		current = np.zeros_like(segmentation[...,t])
		numrows=len(current)
		numcols=len(current[0])
		for i in range(numrows):
			for j in range(numcols):
				#at each place, assign current the maximum!
				maxprob = -10
				maxind = -1
				for k in range(Nlarvae):
					if res[k,i,j,1] + th[k] > maxprob:
						maxprob = res[k,i,j,1]
						maxind = k
				current[i,j] = maxind + 1


                current=np.where(segmentation[...,t]==0,0,current)


                #current is the labeled image of the current time step
		current_sizes = [0 for k in range(N_MAX+1)]   
		for l in range(1,N_MAX+1):   
			current_sizes[l] = sum(sum(k==l for k in current))             
                
                #product of differences of size of the components
		#compute_energy(start_sizes, current_sizes)

		energy = abs(current_sizes[1] - start_sizes[1])+abs(current_sizes[2] - start_sizes[2])
		if Nlarvae > 2:
			energy += abs(current_sizes[3]-start_sizes[3])	
                
                #print "ENERGY:" , energy, "min energy of this time step", min_energy
                
                if energy >= min_energy:
		    increase(th,up)
                    continue
                
                #print "FOUND BETTER THRESHOLD!"
                min_energy = energy
                
                labels1=np.where(current==1,1,0)
                labels1=get_biggest(labels1)
                labels2=np.where(current==2,1,0)
                labels2=get_biggest(labels2)
                labels3 = np.zeros_like(current)
		if Nlarvae > 2:
			labels3=np.where(current==3,1,0)
        	        labels3=get_biggest(labels3)

                #put the mask for the two larvae together again
                current=labels1+labels2*2+labels3*3
                
                labels=np.where(segmentation[...,t]==0,-1,current)
		
		#If there is a larva in lvol that is not in labels, find it!
		lvol_pos = np.where(lvol[...,t]>0,1,0)
		labels_pos = np.where(labels>0, 1, 0)
			
		remainder = lvol_pos - labels_pos
		remainder_size = sum(sum(remainder))
		
		#check if a new worm appeared		

		if remainder_size>50:
			start_sizes[3] = remainder_size
			print "A NEW WORM HAS APPEARED!"
		

                """
                pylab.subplot(1,3,1)
                pylab.imshow(img3d[...,1]*255,cmap="gray",interpolation=None)
                pylab.axis("off")
                pylab.subplot(1,3,2)
                pylab.imshow(current,interpolation=None)
                pylab.axis("off")
                pylab.subplot(1,3,3)
                pylab.imshow(res[1,...,1],interpolation=None)
                pylab.axis("off")
                
                pylab.subplots_adjust(hspace=0.01, wspace=0.01, top=1, bottom=0, left=0,
                                right=1)
                
                pylab.savefig(resfolder+"%.3d.png"%t)
                """
                print "there"
                
                labels1=np.where(labels==1,1,0)
                labels1=get_biggest(labels1)
                #labels1=vigra.filters.discErosion(labels1.astype(np.uint8),1)
                labels2=np.where(labels==2,1,0)
                labels2=get_biggest(labels2)
                #labels2=vigra.filters.discErosion(labels2.astype(np.uint8),1)
		if Nlarvae > 2:                
			labels3=np.where(labels==3,1,0)
        	        labels3=get_biggest(labels3)

                
                labels=-np.ones_like(labels)
                labels[labels1==1]=1
                labels[labels2==1]=2
		labels[labels3==1]=3
		if remainder_size>50:
			labels[remainder==1]=3

		#update threshold
		increase(th,up)
				
		


        print "done"
    except Exception,e:
        print e
        pass
    
    
    
    
#    x,y=np.where(labels==1)
#    ii=np.random.randint(0,x.shape[0],x.shape[0]-x.shape[0]/100)
#    labels[x[ii],y[ii]]=0
#    
#    x,y=np.where(labels==2)
#    ii=np.random.randint(0,x.shape[0],x.shape[0]-x.shape[0]/100)
#    labels[x[ii],y[ii]]=0
    
    
    
    
    
    #pylab.imshow(labels)
    #pylab.show()
