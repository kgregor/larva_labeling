from tools import readH5,writeH5
from skimage.segmentation import random_walker#,random_walker_with_prior
import pylab
import numpy as np
import time
import vigra
import os


np.set_printoptions(threshold=np.nan)

for kk in range(1,2):    
    try:
        #filename="testdata/encounters/%d/encounter%d.h5"%(kk,kk)
        filename="encounters_test/test%d.h5"%kk
        
        f,e=os.path.split(filename)
        n,e=os.path.splitext(e)
        
        
        
        resfolder="14_12_2012/noniterativenoprior/"
        
        print resfolder+e
        if not os.path.exists(resfolder+n):
            os.mkdir(resfolder+n)
        
        resfolder=resfolder+n+"/"
        
        data=readH5(filename).squeeze().swapaxes(0,-1).astype(np.float32)/255
        segmentation=readH5(filename,"volume/segmentation").squeeze().swapaxes(0,-1).astype(np.int32)
        lvol=readH5(filename,"volume/labeledvol").squeeze().swapaxes(0,-1).astype(np.int32)
        segmentation=segmentation*(lvol>0)
        
        print segmentation.shape,lvol.shape
        
        labels0=lvol[...,0]
        labels0=np.where(labels0==0,-1,labels0)
	#give only one larva a label, other is not considered	
	labels0=np.where(labels0==2,-1,labels0)
        labels=labels0

        def get_biggest(seg):
            #Clean out small cc components from a binary segmentation
            lseg=vigra.analysis.labelImageWithBackground(seg.astype(np.float32)).view(np.ndarray)
            #print lseg.shape,"inside"
            sizes=np.bincount(lseg.flatten().astype(np.int32))
            #print "sizes", sizes.shape
            sizes=sizes[1:]
            
            ii=np.argmax(sizes)
            lseg=np.where(lseg==ii+1,1,0)
            
            return lseg
        
        
        #save sizes of components from first time step
        start_size1 = sum(sum(k==1 for k in labels))
        #start_size2 = sum(sum(k==2 for k in labels))
        
	thresholds = [0.5, 0.6, 0.7, 0.8, 0.9]

        for t in range(1,3):#lvol.shape[-1]):
          
            print "#########################################################"
            print "                     TIME STEP ", t
            print "#########################################################"
          
            #set labels of next time step to 0
            nextlabels=np.where(segmentation[...,t]==0,-1,0).astype(np.int32)
            labels3d=np.dstack([labels,nextlabels])
            img3d=data[...,t-1:t+1].astype(np.float32) 
            
            min_energy = 10000
            
            pylab.imshow(labels3d[:,:,0],interpolation=None)
            pylab.show()
            
            
            #go through all the thresholds
            for th in range(len(thresholds)):
                
                thresh = thresholds[th]
                
		#probabilities of a pixel belonging to larva 1
                res=random_walker(img3d, labels3d,0.2,mode="bf",return_full_prob=True)
		print "MAXIMUM ENTRY IN RES:", res
                

                current=np.where(res[0,:,:,1]>thresh,1,-1)
                current=np.where(segmentation[...,t]==0,0,current)
                
                
                #current is the labeled image of the current time step
                curr_size1 = sum(sum(k==1 for k in current))
                print "THRESHOLD" , thresh, " CURR SIZE 1:" , curr_size1, " START SIZE:" , start_size1
                
                #product of differences of size of the components
                energy = abs(curr_size1 - start_size1)
                
                print "ENERGY:" , energy, "min energy of this time step", min_energy
                
                if energy > min_energy:
                    continue
                
                print "FOUND BETTER THRESHOLD!"
                min_energy = energy
                
                labels1=np.where(current==1,1,0)
                print labels1.shape,"labels1"
                labels1=get_biggest(labels1)
                
                
                #The function need a binary mask 
                
                #put the mask for the two larvae together again
                labels=np.where(segmentation[...,t]==0,-1,current)
                """
                pylab.subplot(1,3,1)
                pylab.imshow(img3d[...,1]*255,cmap="gray",interpolation=None)
                pylab.axis("off")
                pylab.subplot(1,3,2)
                pylab.imshow(current,interpolation=None)
                pylab.axis("off")
                pylab.subplot(1,3,3)
                pylab.imshow(res[1,...,1],interpolation=None)
                pylab.axis("off")
                
                pylab.subplots_adjust(hspace=0.01, wspace=0.01, top=1, bottom=0, left=0,
                                right=1)
                
                pylab.savefig(resfolder+"%.3d.png"%t)
                """
                print "there"
                
                labels1=np.where(labels==1,1,0)
                labels1=get_biggest(labels1)
                #labels1=vigra.filters.discErosion(labels1.astype(np.uint8),1)
                
                
                
                labels=-np.ones_like(labels)
                labels[labels1==1]=1
            
        print "done"
    except Exception,e:
        print e
        pass
    
    
    
    
#    x,y=np.where(labels==1)
#    ii=np.random.randint(0,x.shape[0],x.shape[0]-x.shape[0]/100)
#    labels[x[ii],y[ii]]=0
#    
#    x,y=np.where(labels==2)
#    ii=np.random.randint(0,x.shape[0],x.shape[0]-x.shape[0]/100)
#    labels[x[ii],y[ii]]=0
    
    
    
    
    
    #pylab.imshow(labels)
    #pylab.show()
