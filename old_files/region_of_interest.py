import numpy as np
import randomwalker
import h5py
import sys
import cPickle
import color_tracking_result
import os
import logging



logging.basicConfig(level=logging.DEBUG)


DEBUG=0
NMAX_ENCOUNTER=1
LIMIT = 10000000
MAX_RECURSION_DEPTH=1000


class GraphRegion(object):
    
    def __init__(self,graph,positions,info,cc_subregion,cc_end,cc_start,K):
        self.graph=graph
        self.info=info
        self.cc_subregion=cc_subregion
        self.cc_end=cc_end
        self.cc_start=cc_start
        self.positions=positions
        self.new_positions=None #positions in the new coordinate system
        self.K = K
        
        self.startt=info[0]
        self.stopt=info[1]
        self.startx=info[2]
        self.stopx=info[3]
        self.starty=info[4]
        self.stopy=info[5]
        
        print "INFO: start time", self.startt, "end time" , self.stopt, "len x:", self.stopx-self.startx+1, "len y:", self.stopy-self.starty+1
        
        self.shape=(info[1]-info[0]+1,info[3]-info[2]+1,info[5]-info[4]+1) #spatial region shape out of the volume
        
        self.slicing=(slice(info[0],info[1]+1,None),slice(info[2],info[3]+1,None),slice(info[4],info[5]+1,None))
        
        self._get_new_coordinates()
        

    def _get_new_coordinates(self):
        self.new_positions={}
        info =self.info
        for cc_ind in self.cc_subregion:
            
            pos_new=np.array(self.positions[cc_ind])
            pos_new[:,0]-=info[0]
            pos_new[:,1]-=info[2]
            pos_new[:,2]-=info[4]
            
            self.new_positions[cc_ind]=pos_new
    
    def __str__(self):
        
        out="This is %s \n"%self.info
        out+="starting stuff %s"%self.cc_start
        out+="ending stuff %s"%self.cc_end
        
        return out
    
        
            

def visualize_region(data,region):
    
    print "data.shape",data.shape
    print "region", region.shape
    vol=np.zeros((region.shape),np.uint8)
    print vol.shape,"here",region.slicing
    print region.info
    vol[:,:,:]=data[region.slicing]
    vol.shape=vol.shape+(1,)
    vol=np.concatenate([vol,vol,vol],axis=-1)
    print "there"
    
    alpha = 0.6
    
    for cc in region.cc_subregion:
        p=region.new_positions[cc]
        
        print "PPPP", p
        
        color=[0,0,255]
        if cc in region.cc_start:
            color=[255,0,0]
        else:
            if cc in region.cc_end:
                color=[0,255,0]
            
        vol[p[:,0],p[:,1],p[:,2],0]=alpha*vol[p[:,0],p[:,1],p[:,2],0] + (1-alpha)*color[0]
        vol[p[:,0],p[:,1],p[:,2],1]=alpha*vol[p[:,0],p[:,1],p[:,2],1] + (1-alpha)*color[1]
        vol[p[:,0],p[:,1],p[:,2],2]=alpha*vol[p[:,0],p[:,1],p[:,2],2] + (1-alpha)*color[2]
        
    
    return vol
    

def get_info_to_serialize(total_series,positions):
    ##FIXME: rewrite clearly
    
    x_max =0
    y_max =0
    x_min =1000000
    y_min =1000000
    
#    print total_series
    print "LEN TOTAL SERIES", len(total_series)
    
    for i in range(len(total_series)):
        pixel = positions[total_series[i]]
        pixel=np.array(pixel)
        cmax=np.max(pixel,axis=0)
        cmin=np.min(pixel,axis=0)
        
        if cmax[1] > x_max:
            x_max = cmax[1]
        if cmin[1] < x_min:
            x_min = cmin[1]
        if cmax[2] > y_max:
            y_max = cmax[2]
        if cmin[2] < y_min:
            y_min = cmin[2]
    if DEBUG: print "MIN_X " , x_min , ", MAX_X " , x_max , ", MIN_Y " , y_min , " MAX_Y " , y_max
    
    #now get time steps!
    t_max=positions[total_series[-1]][0][0]
    t_min=positions[total_series[0]][0][0]
    
    if DEBUG: 
        print "MIN TIME STEP " , t_min
        print "MAX TIME STEP " , t_max
    
    assert x_min>=0 and y_min>=0

        
    left = x_min-5
    right = x_max+5    
    bottom = y_min-5
    top = y_max+5
    
    return [int(t_min), int(t_max), int(left), int(right), int(bottom), int(top)]


def write_dict_to_file(complete_positions):
    
    
    f = open("/home/kgregor/Desktop/dictionary2.txt","w")
    
    for cc in complete_positions:
        #print complete_positions[cc]
        f.write(str(cc)+"\n")
        for larva in complete_positions[cc]:
            #print "larva", larva
            f.write(str(larva)+":"+str(complete_positions[cc][larva])+"\n")
        f.write("==============================================\n")
   
    f.close()
    



def is_relevant(cc, series,true_sizes,connections,K):
    #checks if a node is a candidate node start for an encounter
    
    
    if cc > LIMIT and DEBUG:
        return 0
    
    #if K-component is connected to another relevant K-component
    if true_sizes[cc] == K and len(connections[cc]) == 1 :
        if is_relevant(connections[cc][0], series,true_sizes,connections,K):
            series.append(cc)
            return 1
        
    #it should not merge with other components in the next time step
    if true_sizes[cc] == K and len(connections[cc]) > 1:
        sum_neigh = 0
        for neigh in connections[cc]:
            sum_neigh += true_sizes[neigh]
            #if true_sizes[connections[cc][0]]==1 and true_sizes[connections[cc][1]]==1 :
        if sum_neigh <= K:
            series.append(cc)
            return 1
    else:
        return 0

#a node is considered a start node, if it has size K, comes from components with smaller size
def is_start_node(cc,true_sizes,back_connections,used_K_nodes,K):
    if true_sizes[cc]!=K:
        return 0
    
    if cc in used_K_nodes:
        return 0
    
    if len(back_connections[cc])==1:
        return 0
    
    sum_back_neigh = 0
    for back_neigh in back_connections[cc]:
        sum_back_neigh += true_sizes[back_neigh]

    if sum_back_neigh > K:
        return 0
    
    return 1


#def find_heads(cc, series, depth,true_sizes,K):  
#    #we don't want to find clusters containing components bigger than our parameter K
#    if depth==0 or true_sizes[cc] > K:
#        return 0
#    
#    #if we find an isolated larva, stop there, and store information that it is a child
#    #because then we don't want to print its neighbors later on!
#    if true_sizes[cc] == 1:
#        if not cc in done:
#            series.append(cc)
#            done[cc]=1
#        return 1
    
        
def find_connections(cc, series, depth, from_top, done,true_sizes,looked_at,connections,back_connections,K,starting_cc,ending_cc):       
    #we don't want to find clusters containing components bigger than our parameter K
    #looked at check that things are not looked twice
    if depth==0 or true_sizes[cc] > K:
        return 0
    
    looked_at[cc] = 1
    series.append(cc)
    
    #if we find an isolated larva, stop there, and store information that it is a child
    #because then we don't want to print its neighbors later on!
    if true_sizes[cc] == 1:
        if not cc in looked_at:
            series.append(cc)
            done[cc]=1
#         if from_top ==1 :
#             #TODO is_child[cc] = 1
#             ending_cc.append(cc) #stores the ccs at the end of the subgraph
#         if from_top ==0:
#             starting_cc.append(cc) #stored the ccs at the start of the subgraph
        return 1
    
    #check for children
    if len(connections[cc])==0:
        return 0
    for child in connections[cc]:
        if child not in looked_at:
            if not find_connections(child, series, depth-1, 1,done,true_sizes,looked_at,connections,back_connections,K,starting_cc,ending_cc):
                return 0
    
    #print "PARENTS: " , back_connections[cc]
    if len(back_connections[cc])==0:
        return 0
    for parent in back_connections[cc]:
        if parent not in looked_at:
            if not find_connections(parent, series, depth-1, 0,done,true_sizes,looked_at,connections,back_connections,K,starting_cc,ending_cc):
                return 0
    
    if not cc in done:
        done[cc] = 1

    return 1

def find_starting_and_ending(series, starting_cc, ending_cc, connections, back_connections):
    for cc in series:
        if true_sizes[cc]==1:
            if len(connections[cc])==0:
                ending_cc.append(cc)
                continue
            if len(back_connections[cc])==0:
                starting_cc.append(cc)
                continue
            if connections[cc][0] not in series:
                ending_cc.append(cc)
            if back_connections[cc][0] not in series:
                starting_cc.append(cc)


def is_start_of_single_trail(cc,true_sizes,connections,back_connections):
    if true_sizes[cc] != 1:
        return 0
    
    if len(back_connections[cc])>0:
        if true_sizes[back_connections[cc][0]]==1:
            return 0
        if len(connections[cc]) > 1:
            return 0
    
    return 1
      
  
def get_single_trail(cc, series,connnections):
    while True:
        
        if true_sizes[cc] != 1:
            return
        series.append(cc)
        if len(connections[cc]) > 1 or len(connections[cc]) < 1 or connections[cc][0] == -1:
            return
        cc = connections[cc][0]
        


class InfoProducer(object):
    def __init__(self,connections,Positions,times,pixel_sizes,true_sizes):
        
        self.cc_graph=connections
        self.cc_positions=Positions
        self.cc_pixel_sizes=pixel_sizes
        self.cc_times=times
        self.cc_true_sizes=true_sizes
        
        ########
        self.back_connections=None
        self.K=None
        
        self._create_back_map_cc()
        
    def __iter__(self):
        return self
    
    
    def _create_back_map_cc(self):
        #Creates a cc component graph from child to parant
        #dictionary whether a node was already checked completely

        
        #create list of lists: each component gets a list with its "backneighbors"
        back_connections = [[] for i in range(LIMIT+400)] #make up for components in T+1
        for cc,neighbors in self.cc_graph.items():
            #print "COMPONENT " , cc
            back_connections[cc]=[]
        
        #fill in backneighbors
        for cc,neighbors in self.cc_graph.items():
            for neigh_cc in neighbors:
                back_connections[neigh_cc].append(cc)
        
        self.back_connections=back_connections
        
    
    
    
    def extract_K1(self, component_dictionary, complete_positions):
        #final_segmentation = np.zeros((1000,1400,1400),np.uint16)
        positions=self.cc_positions
        true_sizes=self.cc_true_sizes
        back_connections=self.back_connections
        connections=self.cc_graph
        
        
        counter=0
        
        all_infos=[]
        for cc, neighbors in connections.items():
            
            if counter > NMAX_ENCOUNTER and DEBUG>1:
                print "FOUDN ENOUGH"
                break
            series = [] ##Will contain the current trail
            if not is_start_of_single_trail(cc,true_sizes,connections,back_connections):
                continue
            
            if DEBUG>0: print "start of single trail:" , cc, "at time", times[cc]
            
            get_single_trail(cc, series,connections)
            
            if len(series) <= 1: ##if it is a isolated larva for one timestep between two bigger connected components
                continue
        
            info = get_info_to_serialize(series,positions)
            
            starting_cc=[series[0]]
            ending_cc=[series[-1]]
            
            #yield info,series,starting_cc,ending_cc
            
            #yield GraphRegion(connections,positions,info,series,ending_cc,starting_cc,1)
            
            
            #print "found single trail:", series
#            fh2=readH5(file_in_base+".h5","volume/labeledvol",onlyhandle=True)
              
        
            for cc in series:
                component_dictionary[cc]=starting_cc
                complete_positions[cc] = {}
                complete_positions[cc][cc] = positions[cc]
                
            print "SINGLE TRAIL LABELED"
            counter+=1
            
            all_infos.append(info)
        
        if DEBUG: print len(all_infos), "single trails have been labeled"
        #return all_infos
        
        
#        print "write to H5 file"
#        fh4=h5py.File("/home/kgregor/workspace/worms_ilp/final_output/" + "trails1.h5","w")
#        g=fh4.require_group("labeling")
#        g.create_dataset(name="data",data=final_segmentation, compression=1)
#        fh4.close()
        
#        print "final seg dumped"
#        
#        pickle.dump(larva_map, open("larva_component_map.pkl","wb"))
        
        
        
    def disable_K_nodes(self,series,used_K_nodes,K):
        for cc in series:
            if self.cc_true_sizes[cc] == K:
                used_K_nodes.append(cc)
    
    def _extract_K(self):
        #final_segmentation = np.zeros((1000,1400,1400),np.uint16)
        positions=self.cc_positions
        true_sizes=self.cc_true_sizes
        back_connections=self.back_connections
        connections=self.cc_graph
        K=self.K
        
        
        print "KKKKKKKKKKKKKK", K
        
        done = {}
        #save all the nodes containing K larvae to prevent them from being selected as start node later
        #if we didn't do that, the same series could be cropped out multiple times
        used_K_nodes = [] 
        looked_at = {}
#        is_child={}
        
        counter=0
    
        larva_map = {}
        #extract relevant sequence
        for cc,neighbors in connections.items():
            looked_at = {} #tells if in the current series the componets are already visited
            tails= []
            heads= []
            series = []
            D = {} #connections
            P = {} #positions
            
            starting_cc=[] #connections at the top of the subgraph
            ending_cc=[] #connections at the end of the subgraph
            
            comp_map = {}
            done = {}
            
            if counter>NMAX_ENCOUNTER and DEBUG : 
              print "FOUND ENOUGH"
              break
            
            
            if is_start_node(cc,true_sizes,back_connections,used_K_nodes,K):
                
#                print "USED K NODES: ", used_K_nodes
                
                if DEBUG>0: print 'found start node' , cc, "back_conn:", back_connections[cc]
                
                if find_connections(cc, series, MAX_RECURSION_DEPTH,False,done,true_sizes,looked_at,connections,back_connections,K,starting_cc,ending_cc):              
                    series.sort(cmp=None, key=None, reverse=False)
                    find_starting_and_ending(series, starting_cc, ending_cc,connections,back_connections)
                    starting_cc.sort(reverse=False)
                    ending_cc.sort(reverse=False)
                    info = get_info_to_serialize(series,positions)
                    
                    self.disable_K_nodes(series,used_K_nodes,K)
                 
                    assert len(starting_cc) == len(ending_cc) and len(starting_cc)>0
                 
                    yield GraphRegion(self.cc_graph,positions,info,series,ending_cc,starting_cc,K)
                    
                    
                    counter = counter + 1
                    
                    if DEBUG>1: print "TOTAL SERIES: " , series
                else:
                    if DEBUG>1: print "not a K =%d larvae  encounter"%K
                    continue
                #get infos of all components in this series!
#                S.append(-1) #such that the zero-spot is filled
#                T.append(-1)
                
                #map components to 1...n
                
#                if cc in is_child:
#                  is_child_new[comp_map[cc]] = 1
                
                
                
                
                
#                do_labeling(info, D, counter, final_segmentation)
          
          
          
    
    
    def extract(self,K):
        self.K=K
        return self._extract_K()
            
    #puts the result from the random walker into the dictionary and relabels
    #the sequence as well as components connected to it, if that is necessary
    def relabel_rw_result(self,component_dictionary,comp_map,pos_map,el):
        #relabel region if start components of region have already been considered before!
        relabel = False
        #aux are the indices in the start of a region that have been relabeled before.
        #thus in the whole region those larvae need to be relabeled as well
        aux = {}
        relabel_end = False
        #this is the same as aux only for the indices in the end of a region
        #the two maps are needed to create the real mapping later on (which is called mapping)
        aux_end = {}
        aux_old = {}
    
        for cc in el.cc_start:
            #we need to relabel the fresh result!
            if cc in component_dictionary:
                relabel = True
                aux[cc] = component_dictionary[cc][0]

        for cc in el.cc_end:
            if cc in component_dictionary:
                relabel_end = True
                aux_end[cc]=comp_map[cc]
                aux_old[cc] = component_dictionary[cc][0]
                
        for cc in comp_map:
            #print "cc", cc, "with larvae", comp_map[cc]
            if relabel==True:
#                print "STUFF NEEDS TO BE RELABELED"
#                print "aux", aux
#                print "cc start", el.cc_start
                
                if cc in el.cc_start:
                    #the components are in cc_start, and relabel==True,
                    #that means, that these components have been relabeled and their
                    #entry in component_dictionary stays the same.
                    #we need to take care of the rest of the sequence now though. They need to be relabeled
                    #with the label that the start components have!
                    print "cc",cc,"already labeled!"
                else:
                    if not cc in component_dictionary:
                        component_dictionary[cc] = []  #TODO might not be necessary
                        
                    #for all the larvae in the region, assign it the new label, if necessary. it need not be necessary
                    #for all larvae, because not all starting indices have been labeled before
                    for larva in comp_map[cc]:
                        
                        #only if a larva was in an already relabeled starting_cc, it needs to be relabeled.
                        #otherwise, it is just copied into the dictionary
                        if larva in aux:
                            if not aux[larva] in component_dictionary[cc]:
                                component_dictionary[cc].append(aux[larva])
                        else:
                            if not larva in component_dictionary[cc]:
                                component_dictionary[cc].append(larva)
                        
                    #cc was in the end of the sequence and was relabeled
                    if cc in el.cc_end:
#                        print "aux end", aux_end
#                        print "cc end", el.cc_end
#                        print "cc", cc ," is end cc and was relabeled"
                        aux_end[cc]=component_dictionary[cc]
                        
                        
            #just normal copying            
            else:
                component_dictionary[cc] = []
                for larva in comp_map[cc]:
                    if not larva in component_dictionary[cc]:
                        component_dictionary[cc].append(larva)
        

        #create new mapping from aux_end and aux_old
        #this will be the mapping for when the last components of a region overlap with already labeled components
        #those already labeled components need to be relabeled then.
        mapping = {}
        for cc in aux_old:
            mapping[aux_old[cc]] = aux_end[cc]
            
        print "mapping done"
            
        if relabel_end == True:
            for cc,ll in component_dictionary.items():
                for oldkey, newkey in mapping.items():
                    if oldkey in component_dictionary[cc]:
                        component_dictionary[cc].remove(oldkey)
                        
                        #TODO HIER WAR EIN FEHLER!!!
                        
                        if len(newkey)==0:
                            print "newkey len ist null"
                            component_dictionary.append(oldkey)
                        else:
                            if not newkey[0] in component_dictionary[cc]:
                                component_dictionary[cc].append(newkey[0])
                        #print "check for", oldkey, "to replace with", newkey[0]
                        #print component_dictionary[cc]
                        #print "-------"
        
        #fill positions map
        for cc in pos_map:
            
            #these are the isolated larva at the beginning and end
            #their positions are already stored.
            if cc in complete_positions:
                continue
            
            aux_pos_map = {}
            for larva in pos_map[cc]:
                
                pos = pos_map[cc][larva]
                
                #int "POSMAP CC",pos_map[cc]
                #print "POS MAP CC LARVA", pos_map[cc][larva]
                if larva in component_dictionary:
                    #print "COMPONENTN DOICT LARVA", component_dictionary[larva]
                    aux_pos_map[component_dictionary[larva][0]]=pos
            complete_positions[cc] = aux_pos_map
            #print complete_positions[cc]
        
        print "===================================================================="   




if __name__=="__main__":
    
    args = sys.argv
    
    #decides whether random walker should be applied or only encounters should be cut out
    RW=int(args[1])
    file_in_base=str(args[2])
    filename=str(args[3])
    file_out_base=str(args[4])
    
    import pickle
    import mailme
    from tools import readH5
    import glob
    import os
    
    try:
    
        print "read data", filename
        from cythonfiles.connectivity import getPositionsOfComponent, deserializeGraphFast2
        connections,positions,sizes,times=deserializeGraphFast2(file_in_base+filename+".graph_fast")
        data=readH5(file_in_base+filename+".h5").astype(np.uint8).squeeze()
        true_sizes=readH5(file_in_base + filename+ ".res","graph_fast/entire/true_sizes")
        
        import pylab
        print "start"
        IF=InfoProducer(connections,positions,times,sizes,true_sizes)
        print "created infoproducer"
        
        for i in range(1,len(positions)):
            for j in range(len(positions[i])):
                assert positions[i][j][0]>=0 and positions[i][j][1]>=0 and positions[i][j][2]>=0
        
        
        #this maps each component to the larvae it contains
        component_dictionary = {}
        complete_positions = {}
        
        print "start extracting"
        
        if RW>0:
            IF.extract_K1(component_dictionary, complete_positions)
        
        for K in range(2,11):
            
            debug_counter = 0
            counter= 0
            
            for el in IF.extract(K):
                counter+=1
                
                if DEBUG>0 and debug_counter > NMAX_ENCOUNTER:
                    if K==3:
                        write_dict_to_file(component_dictionary)
                        sys.exit()
                    else:
                        break
                
                print "Crop out Region!"
                data_cropped=data[el.slicing].astype(np.int32).swapaxes(0,-1)
                data_cropped = data_cropped
                print "data cropped"
                lvol_cropped=readH5(file_in_base+filename+".h5","volume/labeledvol",onlyhandle=True)[el.slicing[0],0,el.slicing[1],el.slicing[2],0].swapaxes(0,-1).astype(np.int32)
                segmentation=readH5(file_in_base+filename+".h5","volume/segmentation",onlyhandle=True)[el.slicing[0],0,el.slicing[1],el.slicing[2],0].astype(np.int32).swapaxes(0,-1)
                segmentation_cropped=segmentation*(lvol_cropped>0)
                
                
                lvol2_cropped=np.zeros_like(lvol_cropped)
                for comp in el.cc_subregion:
                    lvol2_cropped += np.where(lvol_cropped == comp, comp, 0)
                segmentation_cropped=segmentation*(lvol2_cropped>0)
                
                if RW>0:
                    print "start random walker with K=",K
                    RW = randomwalker.RandomWalker(data_cropped, lvol2_cropped, segmentation_cropped, el.info,el.cc_start,el.cc_end,IF.cc_times,IF.cc_positions, K)
                    labeling, comp_map, pos_map = RW.label_sequence()
                    print "random walker done"
                    IF.relabel_rw_result(component_dictionary,comp_map,pos_map,el) 
                else:
                    
                    if not os.path.exists(file_out_base+filename):
                        os.mkdir(file_out_base+filename)
                    
                    fh=h5py.File(file_out_base+filename+"/encounter_"+str(K)+"_"+str(counter).zfill(2)+".h5","w")
                    g=fh.require_group("volume")
                    tmpshape = data_cropped.shape
                    data_out = data_cropped[None,None,:,:,:].swapaxes(0,-1).swapaxes(2,3)
                    lvol_out = lvol2_cropped[None,None,:,:,:].swapaxes(0,-1).swapaxes(2,3)
                    seg_out = segmentation_cropped[None,None,:,:,:].swapaxes(0,-1).swapaxes(2,3)
                    g.create_dataset(name="data",data=data_out, compression=1)
                    g.create_dataset(name="labeledvol", data=lvol_out,  compression=1)
                    g.create_dataset(name="segmentation", data=seg_out, compression=1)
                    fh.close()
                    del el.positions
                    del el.graph
                    cPickle.dump(el, open(file_out_base+filename+"/encounter_"+str(K)+"_"+str(counter).zfill(2)+".pkl","wb"))       
                    
        if RW>0:    
            #general new mapping
            for cc in complete_positions:
                
                aux_dict = complete_positions[cc]
                complete_positions[cc]={}
                for larva in aux_dict:
                    #TODO HIER WAR EIN FEHLER IN t8_2012_10_23_1542_data5, larva=4244
                    if larva not in component_dictionary:
                        complete_positions[cc][larva] = aux_dict[larva]
                        continue
                    if len(component_dictionary[larva]) == 1:
                        complete_positions[cc][component_dictionary[larva][0]] = aux_dict[larva]
                    
                    
                    
            if not os.path.exists(file_out_base+"rw"):
                print "CERATE PATH"
                os.mkdir(file_out_base+"rw")
                
        
            print "write to pickle file"
            cPickle.dump(complete_positions, open(file_out_base+"rw/"+filename+"_comp_dict.pkl","wb"))
            print "dumped pickle file"
            print "Create color result!"
            
                #datafile = file_in_base+filename+"/"+filename+".h5"
                #cres = color_tracking_result.ColorResult(data,file_out_base+"rw/",complete_positions)
                #cres.serialize()
           
            
    except Exception, e:
        print "didnt work"
        logging.exception("Exception in Region of Interest!")
        f=open("log_rw.txt","w")
        f.write(filename+"\n")
        f.write(str(e)+"\n")
        f.close()
    
    #mailme.mailme_finished()
    
#    
#    
#######################################################################
##                       CREATE BACKWARD ARCS                         #
#######################################################################
#    done = {}
#    looked_at = {}
#    is_child={}
#
#
#    
#    
#    ######################################################################
#    #                       FIND RELEVANT SERIES                         #
#    ######################################################################
#    
#    
#    #A node is called relevant, if it consists of K components and it doesnt
#    #connect with more components
#
#    
#    #########################################################################
#    
#    #########################################################################
#    
#
#
#    if K == 1:
#
#
#
#
#    else:
#
#      final_segmentation = np.zeros((1000,1400,1400),np.uint16)
#
#      print "final seg read"
#
#      global larva_map
#      larva_map = pickle.load(open("larva_component_map.pkl","r"))
#
#      #extract relevant sequences
#      for cc,neighbors in connections.items():
#
#        looked_at = {}
#        tails= []
#        heads= []
#        series = []
#        D = {} #connections
#        P = {} #positions
#        T = [] #times
#        S = [] #sizes
#        comp_map = {}
#        done = {}
#        is_child_new = {}
#        
#        if counter>n_max_encounters : 
#          print "FOUND ENOUGH"
#          break
#        
#        if is_start_node(cc):
#
#          print 'found start node' , cc, "back_conn:", back_connections[cc]
#          if find_connections(cc,series, 100, 0):
#          
#            series.sort(cmp=None, key=None, reverse=False)
#        
#            print "TOTAL SERIES: " , series
#                
#            
#          else:
#                
#            print "ERROR"
#            continue
#            
#            
#          #get infos of all components in this series!
#          S.append(-1) #such that the zero-spot is filled
#          T.append(-1)
#          
#          #map components to 1...n
#          comp = 1
#          for cc in series:
#            comp_map[cc] = comp
#            comp +=1
#            
#            if cc in is_child:
#              is_child_new[comp_map[cc]] = 1
#          
#          info = get_info_to_serialize(series)
#          
#          left = info[2]
#          bottom = info[4]
#          t_min = info[0]
#          t_max = info[1]
#                  
#            
#          do_labeling(info, D, counter, final_segmentation)
#                  
#          print "Got the information, put it in the original file!"
#        
#          counter = counter + 1