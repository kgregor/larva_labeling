# README #

This tries to track indistinguishable objects (e.g. larvae) over time using a random walker algorithm.
This means, in the first frame, each larva is colored with a particular color, the larvae is projected onto the next frame for starting points of the random walker algorithm which then does the segmentation in that frame etc.

**Note:** Please note that this was created under time pressure so the code is a little ugly and there are no tests