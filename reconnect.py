from tools import readH5
import cPickle
from region_of_interest2 import GraphRegion
import numpy as np
from copy import copy



def find_reconnections(inlarvae, outlarvae, connections):
    
    print "in", inlarvae
    print "out", outlarvae
    
    maxcc = np.max(outlarvae)
    maxrecursion=100000
    
    identities=[]
    paths=[]
    for cc in outlarvae:
        if connections[cc] > maxcc:
            break
        found=-1
        
        ccinner=copy(cc)
        
        counter=0
        inbetween=[]
        while(True):
            counter+=1
            if counter>maxrecursion: 
                print "1"
                break
            
            if len(connections[ccinner])!=1 or true_sizes[connections[ccinner][0]]!=1:
                print "2"
                break
            else:
                inbetween+=[ccinner]
                ccinner=connections[ccinner][0]
                if ccinner in inlarvae:
                    found=ccinner
                    print "3, found=", found
                    break
            
        if found!=-1:
            identities+=[(cc,found)]
            paths.append(inbetween+[found])
    
    print identities
    print paths

if __name__ == "__main__":
    file_in_base = "/home/kgregor/data/newencounters/t8_2012_10_23_1646_data3/"
    filenamedict = "encounter_4_01.pkl"
    filenamegraph = "/home/kgregor/data/interesting_movies/t8_2012_10_23_1646_data3/t8_2012_10_23_1646_data3"
    
    
    
    from cythonfiles.connectivity import getPositionsOfComponent, deserializeGraphFast2
    connections,positions,sizes,times=deserializeGraphFast2(filenamegraph+".graph_fast")
    
    true_sizes=readH5(filenamegraph+".res","graph_fast/entire/true_sizes",onlyhandle=True)
    
    region = cPickle.load(open(file_in_base+filenamedict,"rb"))
    inlarvae =region.cc_start
    outlarvae = region.cc_end
    
    find_reconnections(inlarvae, outlarvae, connections)
    
    